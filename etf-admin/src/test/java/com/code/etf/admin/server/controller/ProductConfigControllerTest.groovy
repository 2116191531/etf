package com.code.etf.admin.server.controller

import com.alibaba.fastjson.JSON
import com.aliyun.oss.OSSClient
import com.aliyun.oss.model.BucketStat
import com.code.etf.admin.api.dto.*
import com.code.etf.admin.api.feign.ProductConfigFeign
import com.code.etf.admin.api.feign.RedemptionOrderFeign
import com.code.etf.admin.server.EtfAdminApplication
import com.code.etf.admin.server.service.OssService
import com.code.etf.admin.server.service.UserCenterManager
import com.code.etf.admin.server.service.XtApiService
import com.code.user.center.api.vo.AdminDetailVO
import com.code.user.center.api.vo.UserDetailVO
import com.code.user.center.api.vo.UserTokenRes
import com.code.user.center.starter.service.UserCenterService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootContextLoader
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import spock.lang.Specification

@WebAppConfiguration
@ContextConfiguration(classes = [EtfAdminApplication.class], loader = SpringBootContextLoader.class)
@ActiveProfiles("dev")
class ProductConfigControllerTest extends Specification {

    @Autowired
    ProductConfigController productConfigController
    @Autowired
    private OssService ossService

    def "GetProductConfig"() {
        given:
        ProductConfigQueryCommand productConfigQueryRequest = new ProductConfigQueryCommand()
        productConfigQueryRequest.setDealPair(dealPair)
        productConfigQueryRequest.setIsRelease(isRelease)
        productConfigQueryRequest.setMultiple(multiple)
        ProductConfigDTO productConfigDTO = productConfigController.getProductConfig(productConfigQueryRequest)
        expect:
        println "result:=>" + JSON.toJSONString(productConfigDTO)
        where:
        dealPair     | isRelease | multiple
        "BTC3L/USDT" | null      | null
    }

    def "GetProductConfigList"() {


    }

    def "SetProductConfig"() {
    }

    def "UpdateProductConfig"() {
    }

    def "FileUpload"() {
    }

    def "testOss"() {
        given:
        OSSClient client = ossService.getOssClient();
        BucketStat bucketStat = client.getBucketStat("cdbkoss");
        expect:
        println "result:=>" + JSON.toJSONString(bucketStat)
    }

    @Autowired
    RedemptionOrderFeign redemptionOrderFeign

    def "testFeign"() {
        given:
        RedemptionOrderStatisticsDTO redemptionOrderStatisticsDTO = redemptionOrderFeign.getRedemptionOrderStatistics(coinUnit)
        expect:
        println "result:=>" + JSON.toJSONString(redemptionOrderStatisticsDTO.toString())
        where:
        coinUnit | test
        "btc3l"  | null
    }


    @Autowired
    ProductConfigFeign productConfigFeign

    def "testProductConfigFeign"() {
        given:
        List<ProductConfigDTO> productConfigDTOList = productConfigFeign.findAll()
        expect:
        println "result:=>" + JSON.toJSONString(productConfigDTOList.toString())
    }

    def "testNetWorth"() {
        given:
        BigDecimal trackPrice = userCenterApiService.getNewPrice("btc3l_usdt")
        expect:
        println "result:=>" + JSON.toJSONString(trackPrice)
    }

    @Autowired
    XtApiService xtApiService

    def "testGetTicker"() {
        given:
        Map<String, XtGetTickersResponse> result = xtApiService.getTicker("btc3l_usdt")
        expect:
        println "result:=>" + JSON.toJSONString(result)
    }

    @Autowired
    OpenController openController

    def "testApi"() {
        given:
        RebalanceRecordDTO rebalanceRecordDTO = openController.findOneRebalanceRecordByNow(dealPair)
        expect:
        println "result:=>" + JSON.toJSONString(rebalanceRecordDTO)
        where:
        dealPair     | test
        "btc3l_usdt" | null
    }

    @Autowired
    UserCenterManager userCenterManager

    def "testGetUserInfoByToken" (){
        given:
        com.code.user.center.api.mojo.UserCenterResult<UserDetailVO> userCenterResult = UserCenterService.userInfo("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0aGlzIGlzIGZyZWVseSB0b2tlbiIsImF1ZCI6IkFQUCIsImNvbS5jb2RlLnVzZXIuY2VudGVyLmFwaS52by5Vc2VyVG9rZW5SZXMiOiJ7XCJzeXN0ZW1JZFwiOjMsXCJ1c2VySWRcIjozOH0iLCJpc3MiOiJGUkVFTFkiLCJleHAiOjE2MjkwODMwMjAsImlhdCI6MTYyODQ3ODIyMH0.1-Kyz9PVVJKreGLmFnrWMo5v727_HOF4Azn2E_S2ANM")
        expect:
        println "result:=>" + JSON.toJSONString(userCenterResult)
    }


}
