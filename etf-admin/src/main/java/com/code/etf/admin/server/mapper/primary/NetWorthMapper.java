package com.code.etf.admin.server.mapper.primary;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code.etf.admin.server.entity.NetWorth;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 七楼
 * @since 2021-08-11
 */
@Mapper
public interface NetWorthMapper extends BaseMapper<NetWorth> {
}
