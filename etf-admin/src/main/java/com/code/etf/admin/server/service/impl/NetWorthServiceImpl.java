package com.code.etf.admin.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.code.etf.admin.server.entity.NetWorth;
import com.code.etf.admin.server.mapper.primary.NetWorthMapper;
import com.code.etf.admin.server.service.NetWorthService;
import org.springframework.stereotype.Service;

@Service
public class NetWorthServiceImpl extends ServiceImpl<NetWorthMapper, NetWorth> implements NetWorthService {

}
