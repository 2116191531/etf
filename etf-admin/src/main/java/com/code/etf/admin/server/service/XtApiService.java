package com.code.etf.admin.server.service;

import com.alibaba.fastjson.JSONObject;
import com.code.etf.admin.api.dto.MarketConfigDTO;
import com.code.etf.admin.api.dto.XtGetTickersResponse;
import com.code.etf.admin.server.config.properties.XtProcessProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class XtApiService {

    @Autowired
    private XtProcessProperties xtProcessProperties;

    @Autowired
    private RestTemplate restTemplate;

    /**
     * @return Map<标的交易对, XtGetTickersResponse>
     */
    public Map<String, XtGetTickersResponse> getTickers() {
        String url = xtProcessProperties.getUrl() + "/data/api/v1/getTickers";
        JSONObject forObject = restTemplate.getForObject(url, JSONObject.class);
        if (forObject == null || forObject.size() <= 0) {
            return Collections.emptyMap();
        }
        return forObject.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> JSONObject.parseObject(JSONObject.toJSONString(e.getValue()), XtGetTickersResponse.class)));
    }


    /**
     * @return XtGetTickersResponse
     */
    public XtGetTickersResponse getTicker(String market) {
        String url = xtProcessProperties.getUrl() + "/data/api/v1/getTicker?market={market}";
        JSONObject jsonObject = restTemplate.getForObject(url, JSONObject.class, market);
        if (null == jsonObject) {
            return new XtGetTickersResponse();
        }
        XtGetTickersResponse xtGetTickersResponse = jsonObject.toJavaObject(XtGetTickersResponse.class);
        return xtGetTickersResponse;
    }


    public HashMap<String, MarketConfigDTO> getMarketConfig() {
        String url = xtProcessProperties.getUrl() + "/data/api/v1/getMarketConfig";
        HashMap<String, MarketConfigDTO> forObject = restTemplate.getForObject(url, HashMap.class);
        return forObject;
    }

}
