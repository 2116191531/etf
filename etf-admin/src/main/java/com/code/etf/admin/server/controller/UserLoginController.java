package com.code.etf.admin.server.controller;


import com.code.commons.mojo.result.ResultData;
import com.code.springboot.web.exception.BusinessException;
import com.code.springboot.web.exception.UnauthorizedException;
import com.code.user.center.api.mojo.ResultStatus;
import com.code.user.center.api.mojo.UserCenterResult;
import com.code.user.center.api.qo.LoginQo;
import com.code.user.center.api.vo.LoginTokenRes;
import com.code.user.center.api.vo.UserPrefixVO;
import com.code.user.center.starter.service.UserCenterService;
import com.code.util.xx.RsaHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/admin")
@Api(value = "用户登录", tags = "用户登录")
public class UserLoginController {

    @ApiOperation(value = "登录验证第一步", notes = "登录验证第一步")
    @PostMapping("/token/prefix")
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @ApiImplicitParams({
            @ApiImplicitParam(value = "账号", name = "account", required = true),
            @ApiImplicitParam(value = "密码", name = "password", required = true)
    })
    public UserPrefixVO tokenPrefix(@ApiIgnore @RequestBody LoginQo qo, HttpServletRequest request) {
        UserCenterResult<UserPrefixVO> result = UserCenterService.loginTokenPre(qo);
        if (result == null) {
            throw new BusinessException("用户中心访问失败");
        }

        if (!ResultStatus.SUCCESS.getCode().equals(result.getCode())
                || result.getData() == null) {
            throw new BusinessException(result.getMessage() + ":" + result.getData());
        }
        return result.getData();
    }

    @ApiOperation(value = "登录验证第二步", notes = "登录验证第二步")
    @PostMapping("/token/suffix")
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    @ApiImplicitParams({
            @ApiImplicitParam(value = "账号", name = "account", required = true),
            @ApiImplicitParam(value = "密码", name = "verifyCode", required = true)
    })
    public LoginTokenRes tokenSuffix(@ApiIgnore @RequestBody LoginQo qo, HttpServletRequest request) {

        UserCenterResult<LoginTokenRes> result = UserCenterService.loginTokenPost(qo);
        if (result == null) {
            throw new BusinessException("用户中心访问失败");
        }

        if (!ResultStatus.SUCCESS.getCode().equals(result.getCode())
                || result.getData() == null) {
            throw new BusinessException(result.getMessage() + ":" + result.getData());
        }
        return result.getData();
    }

    @PostMapping("/logout")
    @ApiOperation(value = "退出登录", notes = "退出登录")
    public void userLogout(@RequestHeader("Authorization") String authorization, HttpServletRequest request) {
        UserCenterService.logout(authorization);
        request.getSession().invalidate();
    }

    @GetMapping("/info")
    @ApiOperation(value = "获取用户信息", notes = "获取用户信息")
    @ResponseBody
    public UserCenterResult userInfo(@RequestHeader("Authorization") String authorization, HttpServletResponse response) {


        UserCenterResult result = UserCenterService.userInfo(authorization);
        if (result == null || ResultStatus.UNAUTHORIZED.getCode().equals(result.getCode())) {
            response.setStatus(ResultStatus.UNAUTHORIZED.getCode());
            throw new UnauthorizedException();
        }

        if (!ResultStatus.SUCCESS.getCode().equals(result.getCode())
                || result.getData() == null) {
            response.setStatus(ResultStatus.FORBIDDEN.getCode());
            throw new BusinessException(result.getMessage() + ":" + result.getData());
        }
        return result;
    }

    @ApiOperation(value = "获取公钥", notes = "获取公钥")
    @GetMapping("pub-key")
    @ResponseBody
    public ResultData<String> getPubKey() {
        return ResultData.success(RsaHelper.publicKeyString);
    }
}