package com.code.etf.admin.server.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 申赎记录表
 * </p>
 *
 * @author wyf
 * @since 2021-07-08
 */
@TableName("etf_redemption_order")
@ApiModel(value = "RedemptionOrder对象", description = "申赎记录表")
public class RedemptionOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "订单id")
    private Long id;

    @ApiModelProperty(value = "交易对")
    private String dealPair;

    @ApiModelProperty(value = "用户id")
    private Long uId;

    @ApiModelProperty(value = "用户类型")
    private String uType;

    @ApiModelProperty(value = "交易类型：1 申购 2 赎回")
    private String dealType;

    @ApiModelProperty(value = "订单数量 (交易货币)")
    private BigDecimal count;

    @ApiModelProperty(value = "交易货币单位：例 btc3l")
    private String countUnit;

    @ApiModelProperty(value = "订单状态：1 未交易 2 交易成功 3 交易失败 4 未知")
    private String status;

    @ApiModelProperty(value = "交易净值")
    private BigDecimal netWorth;

    @ApiModelProperty(value = "手续费率")
    private BigDecimal feeRate;

    @ApiModelProperty(value = "手续费")
    private BigDecimal fee;

    @ApiModelProperty(value = "手续费单位")
    private String feeUnit;

    @ApiModelProperty(value = "订单金额（计价货币）")
    private BigDecimal price;

    private Long createTime;

    @ApiModelProperty(value = "订单金额单位")
    private String priceUnit;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDealPair() {
        return dealPair;
    }

    public void setDealPair(String dealPair) {
        this.dealPair = dealPair;
    }

    public Long getuId() {
        return uId;
    }

    public void setuId(Long uId) {
        this.uId = uId;
    }

    public String getuType() {
        return uType;
    }

    public void setuType(String uType) {
        this.uType = uType;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public BigDecimal getCount() {
        return count;
    }

    public void setCount(BigDecimal count) {
        this.count = count;
    }

    public String getCountUnit() {
        return countUnit;
    }

    public void setCountUnit(String countUnit) {
        this.countUnit = countUnit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getNetWorth() {
        return netWorth;
    }

    public void setNetWorth(BigDecimal netWorth) {
        this.netWorth = netWorth;
    }

    public BigDecimal getFeeRate() {
        return feeRate;
    }

    public void setFeeRate(BigDecimal feeRate) {
        this.feeRate = feeRate;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public String getFeeUnit() {
        return feeUnit;
    }

    public void setFeeUnit(String feeUnit) {
        this.feeUnit = feeUnit;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getPriceUnit() {
        return priceUnit;
    }

    public void setPriceUnit(String priceUnit) {
        this.priceUnit = priceUnit;
    }

    @Override
    public String toString() {
        return "RedemptionOrder{" +
                "id=" + id +
                ", dealPair=" + dealPair +
                ", uId=" + uId +
                ", uType=" + uType +
                ", dealType=" + dealType +
                ", count=" + count +
                ", countUnit=" + countUnit +
                ", status=" + status +
                ", netWorth=" + netWorth +
                ", feeRate=" + feeRate +
                ", fee=" + fee +
                ", feeUnit=" + feeUnit +
                ", price=" + price +
                ", createTime=" + createTime +
                ", priceUnit=" + priceUnit +
                "}";
    }
}
