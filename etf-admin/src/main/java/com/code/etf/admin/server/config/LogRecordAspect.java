package com.code.etf.admin.server.config;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.MDC;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * aop记录日志
 */
@Slf4j
@Aspect
@Configuration
public class LogRecordAspect {

    /**
     * 定义切点Pointcut
     */
    @Pointcut("execution(* com.code.etf.admin.server.controller.*Controller.*(..))")
    public void execService() {
    }

    /**
     * 环绕通知
     *
     * @param pjp pjp
     * @return Object
     */
    @Around("execService()")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
        MDC.put("tradeId", java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase());
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        assert sra != null;
        HttpServletRequest request = sra.getRequest();
        long start = System.currentTimeMillis();
        if (!ServletFileUpload.isMultipartContent(request)) {
            try {
                Object[] args = pjp.getArgs();
                List<Object> argsList = new ArrayList<>();
                if (args != null && args.length > 0) {
                    for (Object arg : args) {
                        // 如果参数类型是请求和响应的http，则不需要拼接【这两个参数，使用JSON.toJSONString()转换会抛异常】
                        if (arg instanceof HttpServletRequest || arg instanceof HttpServletResponse) {
                            continue;
                        }
                        argsList.add(arg);
                    }
                }
                log.info("请求地址:{}，请求参数:{}", request.getRequestURI(), JSON.toJSONString(argsList));
            } catch (JSONException e) {
                log.debug("AOP用FastJson解析请求参数错误=>", e);
            }
        }

        Object result = pjp.proceed();

        try {
            log.info("请求响应:{}", JSON.toJSONString(result));
        } catch (JSONException e) {
            log.debug("AOP用FastJson解析响应参数错误=>", e);
        }
        log.info("请求时间=>{}毫秒{}", System.currentTimeMillis() - start, System.lineSeparator());
        return result;
    }
}
