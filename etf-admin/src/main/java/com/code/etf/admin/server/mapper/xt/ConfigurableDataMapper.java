package com.code.etf.admin.server.mapper.xt;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code.etf.admin.server.entity.ConfigurableData;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 配置同步数据表 Mapper 接口
 * </p>
 *
 * @author wyf
 * @since 2021-07-12
 */
@Mapper
public interface ConfigurableDataMapper extends BaseMapper<ConfigurableData> {

}
