package com.code.etf.admin.server.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 配置同步数据表
 * </p>
 *
 * @author wyf
 * @since 2021-07-12
 */
@TableName("etf_configurable_data")
@Builder
@Data
public class ConfigurableData implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    //配置名称
    private String name;

    //说明
    private String descr;

    //配置详情
    private String dataDetail;

    private Long createTime;

    private Long updateTime;

}
