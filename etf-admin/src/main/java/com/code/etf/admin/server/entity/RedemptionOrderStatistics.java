package com.code.etf.admin.server.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@TableName("etf_redemption_order_statistics")
@ApiModel(value = "申赎统计", description = "申赎统计")
public class RedemptionOrderStatistics {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "币种id")
    private Long id;

    @ApiModelProperty(value = "币种")
    private String countUnit;

    @ApiModelProperty(value = "市场id")
    private Long marketId;

    @ApiModelProperty(value = "总发行量")
    private BigDecimal totalIssueAmount;

    @ApiModelProperty(value = "账户份额余额")
    private BigDecimal totalBalance;

    @ApiModelProperty(value = "做市总持仓")
    private BigDecimal marketMakeTotalHoldAmount;

    @ApiModelProperty(value = "手续费总持仓")
    private BigDecimal feeTotalAmount;

    private Long createTime;
    private Long updateTime;

}
