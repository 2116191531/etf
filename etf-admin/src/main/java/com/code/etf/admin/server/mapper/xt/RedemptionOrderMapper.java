package com.code.etf.admin.server.mapper.xt;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code.etf.admin.server.entity.RedemptionOrder;

/**
 * <p>
 * 申赎记录表 Mapper 接口
 * </p>
 *
 * @author wyf
 * @since 2021-07-07
 */
public interface RedemptionOrderMapper extends BaseMapper<RedemptionOrder> {

}
