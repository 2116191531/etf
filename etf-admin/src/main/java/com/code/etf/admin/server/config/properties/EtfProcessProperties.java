package com.code.etf.admin.server.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "com.code.etf")
public class EtfProcessProperties {
    private String dictMulCode;
}
