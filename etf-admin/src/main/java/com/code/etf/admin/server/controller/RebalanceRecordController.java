package com.code.etf.admin.server.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.code.etf.admin.api.command.RebalanceRecordPageCommand;
import com.code.etf.admin.api.dto.RebalanceRecordDTO;
import com.code.etf.admin.server.service.RebalanceRecordService;
import com.code.user.center.api.annotation.JwtIgnore;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 调仓记录 前端控制器
 * </p>
 *
 * @author 七楼
 * @since 2021-06-29
 */
@RestController
@RequestMapping("/rebalance-record")
@Api(value = "调仓记录-控制器", tags = "调仓记录-控制器")
public class RebalanceRecordController {
    @Autowired
    private RebalanceRecordService rebalanceRecordService;

    @PostMapping
    @ApiOperation(value = "添加单个", notes = "添加单个")
    @ResponseBody
    @JwtIgnore
    public RebalanceRecordDTO create(@RequestBody @Validated RebalanceRecordDTO dto) {
        return rebalanceRecordService.insert(dto);
    }

    @GetMapping("{id}")
    @ApiOperation(value = "查询单个", notes = "查询单个")
    @ResponseBody
    public RebalanceRecordDTO getOne(@PathVariable("id") Long id) {
        return rebalanceRecordService.findOne(id);
    }


    @GetMapping("list")
    @ApiOperation(value = "分页条件查询列表", notes = "分页条件查询列表")
    public IPage<RebalanceRecordDTO> findAll(@Validated RebalanceRecordPageCommand command) {
        return rebalanceRecordService.findAll(command);
    }

    @GetMapping("find-one-by-type-timing")
    @ApiOperation(value = "查询今天定时再平衡", notes = "查询今天定时再平衡")
    public RebalanceRecordDTO findOneByNow(@RequestParam String dealPair) {
        return rebalanceRecordService.findOneByTypeTiming(dealPair);
    }
}
