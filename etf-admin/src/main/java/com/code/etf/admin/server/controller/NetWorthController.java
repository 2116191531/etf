package com.code.etf.admin.server.controller;

import com.code.etf.admin.api.dto.NetWorthDTO;
import com.code.etf.admin.server.entity.NetWorth;
import com.code.etf.admin.server.service.NetWorthService;
import com.code.user.center.api.annotation.JwtIgnore;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/net-worth")
public class NetWorthController {
    @Autowired
    private NetWorthService netWorthService;

    @PostMapping
    @ApiOperation(value = "添加单个", notes = "添加单个")
    public void create(@RequestBody @Validated NetWorthDTO dto) {
        netWorthService.save(NetWorth.builder().dealPair(dto.getDealPair()).val(dto.getVal()).time(dto.getTime()).build());
    }


}
