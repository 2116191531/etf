package com.code.etf.admin.server.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.code.etf.admin.api.dto.RedemptionOrderCommand;
import com.code.etf.admin.api.dto.RedemptionOrderDTO;
import com.code.etf.admin.api.dto.RedemptionOrderStatisticsDTO;
import com.code.etf.admin.api.dto.RedemptionOrderTotalDTO;

import java.util.List;

public interface RedemptionOrderService {
    IPage<RedemptionOrderDTO> getRedemptionOrderList(RedemptionOrderCommand redemptionOrderCommand);

    RedemptionOrderStatisticsDTO getRedemptionOrderStatistics(String countUnit);

    List<RedemptionOrderStatisticsDTO> getAllRedemptionOrderStatistics();

    RedemptionOrderTotalDTO getRedemptionOrderTotalInfo(RedemptionOrderCommand redemptionOrderCommand);
}
