package com.code.etf.admin.server.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@TableName("etf_net_worth")
@ApiModel(value = "NetWorth对象", description = "净值")
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class NetWorth {
    private String dealPair;
    private Long time;
    private BigDecimal val;
}
