package com.code.etf.admin.server.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.code.etf.admin.api.command.RebalanceRecordPageCommand;
import com.code.etf.admin.api.dto.RebalanceRecordDTO;
import com.code.etf.admin.api.menu.RebalanceTypeEnum;
import com.code.etf.admin.api.menu.RedisKeysEnum;
import com.code.etf.admin.server.entity.RebalanceRecord;
import com.code.etf.admin.server.mapper.primary.RebalanceRecordMapper;
import com.code.etf.admin.server.service.RebalanceRecordService;
import com.code.springboot.redis.server.RedisManager;
import com.code.springboot.web.exception.BusinessException;
import com.code.util.env.EnvUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.RoundingMode;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;

/**
 * <p>
 * 调仓记录 服务实现类
 * </p>
 *
 * @author 七楼
 * @since 2021-06-29
 */
@Slf4j
@Service
public class RebalanceRecordServiceImpl extends ServiceImpl<RebalanceRecordMapper, RebalanceRecord> implements RebalanceRecordService {
    @Autowired
    private RedisManager redisManager;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public RebalanceRecordDTO insert(RebalanceRecordDTO dto) {
        RebalanceRecord po = toPo(dto);
        // 每天只可以添加一个定时调仓
        if (!EnvUtil.isTestOrQa()) {
            if (RebalanceTypeEnum.TIMING_RE_BALANCE.getKey().equals(dto.getType())) {
                final RebalanceRecordDTO oneByNow = findOneByTypeTiming(dto.getDealPair());
                if (oneByNow != null) {
                    log.warn("当前{}交易对，今日已进行了调仓，故此次调仓拒绝！", dto.getDealPair());
                    return null;
                }
            }
        }

        boolean isSuc = this.save(po);

        if (isSuc) {
            RebalanceRecordDTO result = findOne(po.getId());
            redisManager.set(String.format(RedisKeysEnum.ETF_RE_BALANCE_RECORD.getKey(), po.getDealPair()), JSONObject.toJSONString(result));
            return result;
        }
        throw new BusinessException("资源保存失败");
    }

    @Override
    public RebalanceRecordDTO findOne(Long id) {
        return this.toDto(this.getById(id));
    }

    @Override
    public IPage<RebalanceRecordDTO> findAll(RebalanceRecordPageCommand command) {
        Page<RebalanceRecord> pg = new Page<>(command.getCurrent(), command.getSize());
        QueryWrapper<RebalanceRecord> qw = new QueryWrapper<>();
        qw.eq(command.getMultiple() != null, RebalanceRecord.Field.MULTIPLE, command.getMultiple());
        qw.eq(StringUtils.isNotBlank(command.getType()), RebalanceRecord.Field.TYPE, command.getType());
        qw.between(command.getStartTime() != null && command.getEndTime() != null, RebalanceRecord.Field.CREATE_TIME, command.getStartTime(), command.getEndTime());
        qw.and(StringUtils.isNotBlank(command.getDealPair()), wrapper -> {
            wrapper.like(RebalanceRecord.Field.DEAL_PAIR, command.getDealPair());
        });
        qw.orderByDesc(RebalanceRecord.Field.CREATE_TIME);
        this.page(pg, qw);
        Page<RebalanceRecordDTO> pgDto = new Page<>();
        BeanUtils.copyProperties(pg, pgDto);
        pgDto.setRecords(pg.getRecords().stream().map(this::toDto).collect(Collectors.toList()));
        return pgDto;
    }

    @Override
    public RebalanceRecordDTO findOneByTypeTiming(String dealPair) {
        long current = System.currentTimeMillis();
        long zero = current / (1000 * 3600 * 24) * (1000 * 3600 * 24) - TimeZone.getDefault().getRawOffset();

        QueryWrapper<RebalanceRecord> qw = new QueryWrapper<>();
        qw.ge(RebalanceRecord.Field.CREATE_TIME, zero);
        qw.eq(RebalanceRecord.Field.DEAL_PAIR, dealPair);
        qw.eq(RebalanceRecord.Field.TYPE, RebalanceTypeEnum.TIMING_RE_BALANCE.getKey());
        return this.toDto(this.getOne(qw));
    }

    @Override
    public RebalanceRecordDTO findOneByNow(String dealPair) {
        QueryWrapper<RebalanceRecord> qw = new QueryWrapper<>();
        qw.eq(RebalanceRecord.Field.DEAL_PAIR, dealPair);
        qw.last(" limit 1");
        List<RebalanceRecord> list = this.list(qw);
        if (list == null || list.size() <= 0) {
            throw new BusinessException("当前" + dealPair + "交易对调仓记录异常，不足1条！");
        }
        return this.toDto(this.list(qw).get(0));
    }

    private RebalanceRecord toPo(RebalanceRecordDTO dto) {
        if (dto == null) {
            return null;
        }
        RebalanceRecord po = new RebalanceRecord();
        BeanUtils.copyProperties(dto, po);

        // 其实净值可以不用砍，计算净值的地方已经切成4位小数了
        po.setNetWorthOld(po.getNetWorthOld().setScale(4,RoundingMode.DOWN));
        po.setNetWorth(po.getNetWorth().setScale(4, RoundingMode.DOWN));

        po.setCoinTradePriceOld(po.getCoinTradePriceOld().setScale(8,RoundingMode.DOWN));
        po.setCoinTradePrice(po.getCoinTradePrice().setScale(8,RoundingMode.DOWN));
        po.setCoinValuationPriceOld(po.getCoinValuationPriceOld().setScale(8,RoundingMode.DOWN));
        po.setCoinValuationPrice(po.getCoinValuationPrice().setScale(8,RoundingMode.DOWN));
        return po;
    }

    private RebalanceRecordDTO toDto(RebalanceRecord po) {
        if (po == null) {
            return null;
        }
        RebalanceRecordDTO dto = new RebalanceRecordDTO();
        BeanUtils.copyProperties(po, dto);
        return dto;
    }
}
