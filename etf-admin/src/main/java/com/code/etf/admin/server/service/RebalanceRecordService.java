package com.code.etf.admin.server.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.code.etf.admin.api.command.RebalanceRecordPageCommand;
import com.code.etf.admin.api.dto.RebalanceRecordDTO;
import com.code.etf.admin.server.entity.RebalanceRecord;

/**
 * <p>
 * 调仓记录 服务类
 * </p>
 *
 * @author 七楼
 * @since 2021-06-29
 */
public interface RebalanceRecordService extends IService<RebalanceRecord> {

    RebalanceRecordDTO insert(RebalanceRecordDTO dto);

    RebalanceRecordDTO findOne(Long id);

    IPage<RebalanceRecordDTO> findAll(RebalanceRecordPageCommand command);

    RebalanceRecordDTO findOneByTypeTiming(String dealPair);

    RebalanceRecordDTO findOneByNow(String dealPair);
}
