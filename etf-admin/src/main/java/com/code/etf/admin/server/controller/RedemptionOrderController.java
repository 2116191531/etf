package com.code.etf.admin.server.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.code.etf.admin.api.dto.RedemptionOrderCommand;
import com.code.etf.admin.api.dto.RedemptionOrderDTO;
import com.code.etf.admin.api.dto.RedemptionOrderStatisticsDTO;
import com.code.etf.admin.api.dto.RedemptionOrderTotalDTO;
import com.code.etf.admin.server.service.RedemptionOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@Api(tags = "申赎记录及资金统计")
@RestController
@RequestMapping("/redemption-order")
public class RedemptionOrderController {


    @Autowired
    private RedemptionOrderService redemptionOrderService;


    @ApiOperation(value = "获取申赎记录列表", notes = "获取申赎记录列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public IPage<RedemptionOrderDTO> getRedemptionOrderList(RedemptionOrderCommand redemptionOrderCommand) {
        return redemptionOrderService.getRedemptionOrderList(redemptionOrderCommand);
    }

    @ApiOperation(value = "获取申赎统计", notes = "获取申赎统计")
    @RequestMapping(value = "/total", method = RequestMethod.GET)
    public RedemptionOrderTotalDTO getRedemptionOrderTotalInfo(@Validated RedemptionOrderCommand redemptionOrderCommand) {
        return redemptionOrderService.getRedemptionOrderTotalInfo(redemptionOrderCommand);
    }

    @ApiOperation(value = "根据币种获取资金统计", notes = "根据币种获取资金统计")
    @RequestMapping(value = "/statistics/{countUnit}", method = RequestMethod.GET)
    public RedemptionOrderStatisticsDTO getRedemptionOrderStatistics(@NotNull @PathVariable("countUnit") String countUnit) {
        return redemptionOrderService.getRedemptionOrderStatistics(countUnit);
    }

}
