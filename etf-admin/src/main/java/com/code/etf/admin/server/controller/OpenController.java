package com.code.etf.admin.server.controller;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.code.etf.admin.api.command.RebalanceRecordPageCommand;
import com.code.etf.admin.api.dto.ProductConfigDTO;
import com.code.etf.admin.api.dto.ProductConfigQueryCommand;
import com.code.etf.admin.api.dto.RebalanceRecordDTO;
import com.code.etf.admin.api.menu.RedisKeysEnum;
import com.code.etf.admin.server.service.ProductConfigService;
import com.code.etf.admin.server.service.RebalanceRecordService;
import com.code.springboot.redis.server.RedisManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * etf 公开接口
 */
@Api(tags = "etf产品公开接口")
@Slf4j
@RestController
@RequestMapping("/open-api")
public class OpenController {

    @Autowired
    RedisManager redisManager;
    @Autowired
    private ProductConfigService productConfigService;
    @Autowired
    private RebalanceRecordService rebalanceRecordService;

    @ApiOperation(value = "根据条件查询产品配置详情", notes = "根据条件查询产品配置详情")
    @RequestMapping(value = "/product-detail", method = RequestMethod.GET)
    public ProductConfigDTO getProductConfig(ProductConfigQueryCommand productConfigQueryCommand) {
        return productConfigService.getProductConfig(productConfigQueryCommand);
    }

    @ApiOperation(value = "查询所有产品配置", notes = "根据条件查询所有产品配置")
    @RequestMapping(value = "/all-product-config-release", method = RequestMethod.GET)
    public List<ProductConfigDTO> getProductConfigAll(ProductConfigQueryCommand productConfigQueryCommand) {
        return productConfigService.getProductConfigListAllByCondition(productConfigQueryCommand);
    }


    @GetMapping("/rebalance-record-list")
    @ApiOperation(value = "分页条件查询列表", notes = "分页条件查询列表")
    public IPage<RebalanceRecordDTO> findAll(@Validated RebalanceRecordPageCommand command) {
        return rebalanceRecordService.findAll(command);
    }

    @GetMapping("/find-one-by-now")
    @ApiOperation(value = "查询最近一条", notes = "查询最近一条")
    public RebalanceRecordDTO findOneRebalanceRecordByNow(String dealPair) {
        String format = String.format(RedisKeysEnum.ETF_RE_BALANCE_RECORD.getKey(), dealPair);
        Object result = redisManager.get(format);
        if (null == result) {
            synchronized (dealPair.intern()) {
                result = redisManager.get(format);
                if (null == result) {
                    RebalanceRecordDTO dto = rebalanceRecordService.findOneByNow(dealPair);
                    redisManager.set(format, dto);
                    return dto;
                }
            }
        }
        return JSON.parseObject(String.valueOf(result), RebalanceRecordDTO.class);
    }

}
