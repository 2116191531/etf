package com.code.etf.admin.server.mapper.xt;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code.etf.admin.server.entity.RedemptionOrderStatistics;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RedemptionOrderStatisticsMapper extends BaseMapper<RedemptionOrderStatistics> {
}
