package com.code.etf.admin.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.code.etf.admin.server.entity.NetWorth;


public interface NetWorthService extends IService<NetWorth> {

}
