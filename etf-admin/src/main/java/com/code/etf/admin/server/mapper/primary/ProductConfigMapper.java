package com.code.etf.admin.server.mapper.primary;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code.etf.admin.server.entity.ProductConfig;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * etf产品配置表 Mapper 接口
 * </p>
 *
 * @author 七楼
 * @since 2021-06-29
 */
@Mapper
public interface ProductConfigMapper extends BaseMapper<ProductConfig> {
    @Select("update etf_product_config set sort=sort+1 where sort>= #{newSort} and sort< #{oldSort}")
    void upSort(Integer newSort, Integer oldSort);

    @Select("update etf_product_config set sort=sort-1 where sort> #{oldSort} and sort<= #{newSort}")
    void downSort(Integer newSort, Integer oldSort);
}
