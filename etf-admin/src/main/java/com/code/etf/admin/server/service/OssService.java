package com.code.etf.admin.server.service;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.PutObjectResult;
import com.code.etf.admin.server.config.OssConfig;
import com.code.springboot.web.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.UUID;

@Component
@Slf4j
public class OssService {


    @Autowired
    private OSSClient ossClient;
    @Autowired
    private OssConfig ossConfig;

    public String uploadFile(String key, byte[] data) {
        try (ByteArrayInputStream i = new ByteArrayInputStream(data)) {
            PutObjectResult tip = ossClient.putObject(ossConfig.getBucketName(), key, i);
            if (tip == null || StringUtils.isEmpty(tip.getETag())) {
                return null;
            }
            log.info("OSSService uploadFile key <{}>", key);
            return ossConfig.getWebServer() + key;
        } catch (IOException e) {
            log.error(e.getMessage());
            return null;
        }
    }

    public String getUrl(String key) {
        // 设置URL过期时间为1小时
        Date expiration = new Date(System.currentTimeMillis() + 3600 * 1000);
        // 生成URL
        URL url = ossClient.generatePresignedUrl(ossConfig.getBucketName(), key, expiration);

        log.info("OSSService getUrl key <{}> expiration<{}> url<{}>", key, expiration, url);
        return url.toString();
    }

    public String uploadFile(MultipartFile file) {
        byte[] bytes = null;
        try {
            bytes = file.getBytes();
            String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
            String uuid = UUID.randomUUID().toString().replace("-", "");
            String fileName = uuid + "-" + System.currentTimeMillis();
            String filePath = "etf/" + fileName + suffix;
            if (null == bytes) {
                log.error("etf:图片上传,文件读取失败");
                throw new BusinessException("etf:图片上传,文件读取失败");
            }
            return this.uploadFile(filePath, bytes);
        } catch (IOException e) {
            e.printStackTrace();
            //TODO 打出堆栈
            log.error("etf图片上传失败:{}", e.getMessage());
            throw new BusinessException("etf图片上传失败");
        }
    }

    public OSSClient getOssClient() {
        return ossClient;
    }

}
