package com.code.etf.admin.server.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.code.etf.admin.api.command.ProductSortCommand;
import com.code.etf.admin.api.dto.*;
import com.code.etf.admin.api.feign.EtfOpenApiFeign;
import com.code.etf.admin.api.menu.EtfMainSwitcher;
import com.code.etf.admin.api.menu.KafkaTopicKeys;
import com.code.etf.admin.api.menu.RebalanceTypeEnum;
import com.code.etf.admin.api.menu.RedisKeysEnum;
import com.code.etf.admin.api.message.NetWorthMessage;
import com.code.etf.admin.server.entity.ConfigurableData;
import com.code.etf.admin.server.entity.ProductConfig;
import com.code.etf.admin.server.entity.RedemptionOrderStatistics;
import com.code.etf.admin.server.mapper.primary.ProductConfigMapper;
import com.code.etf.admin.server.mapper.xt.ConfigurableDataMapper;
import com.code.etf.admin.server.mapper.xt.RedemptionOrderStatisticsMapper;
import com.code.etf.admin.server.service.ProductConfigService;
import com.code.etf.admin.server.service.QuotationManager;
import com.code.etf.admin.server.service.RebalanceRecordService;
import com.code.etf.admin.server.service.UserCenterManager;
import com.code.springboot.redis.server.RedisManager;
import com.code.springboot.web.exception.BusinessException;
import com.code.user.center.api.mojo.UserCenterResult;
import com.code.user.center.api.vo.UserDetailVO;
import com.code.user.center.starter.service.UserCenterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>
 * etf产品配置表 服务实现类
 * </p>
 *
 * @author 七楼
 * @since 2021-06-29
 */
@Slf4j
@Service
public class ProductConfigServiceImpl extends ServiceImpl<ProductConfigMapper, ProductConfig> implements ProductConfigService {


    private static final String dealPairSplitRegex = "_";

    @Autowired
    private RebalanceRecordService rebalanceRecordService;
    @Autowired
    private RedisManager redisManager;
    @Autowired
    private KafkaTemplate kafkaTemplate;
    @Autowired
    private ConfigurableDataMapper configurableDataMapper;
    @Autowired
    private RedemptionOrderStatisticsMapper redemptionOrderStatisticsMapper;
    @Autowired
    private EtfOpenApiFeign etfOpenApiFeign;
    @Autowired
    private QuotationManager quotationManager;
    @Autowired
    private UserCenterManager userCenterManager;

    @Transactional
    @Override
    public ProductConfig saveProductConfig(ProductConfigSaveCommand productConfigSaveCommand, HttpServletRequest request) {
        //获取用户信息
        String token = request.getHeader("Authorization");
        UserCenterResult<UserDetailVO> userDetailVOUserCenterResult = UserCenterService.userInfo(token);
        UserDetailVO data = userDetailVOUserCenterResult.getData();
        if (null == data) {
            throw new BusinessException("用户信息获取失败");
        }

        //校验参数
        this.validProductSaveParam(productConfigSaveCommand);
        //基本数据录入
        ProductConfig productConfig = new ProductConfig();
        BeanUtils.copyProperties(productConfigSaveCommand, productConfig);
        String dealPair = productConfigSaveCommand.getDealPair();
        String[] split = dealPair.split(dealPairSplitRegex);
        productConfig.setCoinTrade(split[0].toLowerCase());
        productConfig.setCoinValuation(split[1].toLowerCase());
        List<Long> marketMakerIds = productConfigSaveCommand.getMarketMakerIds();
        if (!CollectionUtils.isEmpty(marketMakerIds)) {
            marketMakerIds.removeIf(Objects::isNull);
        }
        productConfig.setMarketMakerIds(JSON.toJSONString(marketMakerIds));
        productConfig.setSort(this.getNewSort());
        productConfig.setCreateUser(data.getName());
        productConfig.setCreateTime(System.currentTimeMillis());
        this.save(productConfig);
        try {
            //初始化调仓记录
            this.rebalanceInitProcess(productConfig);
            //发送kafka通知: 净值
            this.sendNetWorthInitKafka(productConfig);

            //缓存交易对杠杆计算因子
            redisManager.set(String.format(RedisKeysEnum.ETF_DEAL_PAIR_NET_WORTH_LOAD_FACTOR.getKey(), productConfig.getDealPair()), 1);
        } catch (Exception e) {
            log.error("etf产品配置:初始化失败:", e);
            throw new BusinessException("etf产品配置:初始化失败:" + e.getMessage());
        }
        // 返回对象
        return productConfig;
    }


    private void validProductSaveParam(ProductConfigSaveCommand productConfigSaveCommand) {
        log.info("etf管理端保存产品配置入参:{}", JSON.toJSONString(productConfigSaveCommand));
        ProductConfig one = this.getOne(new QueryWrapper<ProductConfig>().lambda().eq(ProductConfig::getDealPair, productConfigSaveCommand.getDealPair()));
        if (null != one) {
            throw new BusinessException("交易对已存在! dealPair:" + productConfigSaveCommand.getDealPair());
        }
        //校验:(|产品倍数|*在平衡阈值）<1
        BigDecimal thresholdUpDown = productConfigSaveCommand.getThresholdUpDown();
        Integer multiple = Math.abs(productConfigSaveCommand.getMultiple());
        if (thresholdUpDown.multiply(new BigDecimal(multiple)).compareTo(new BigDecimal(1)) >= 0) {
            throw new BusinessException("配置不符合规范,要求:(|产品倍数|*在平衡阈值）<1" + productConfigSaveCommand.getDealPair());
        }

    }

    private Integer getNewSort() {
        QueryWrapper<ProductConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().orderByDesc(ProductConfig::getSort).last("limit 1");
        ProductConfig productConfigLast = this.getOne(queryWrapper);
        return productConfigLast == null ? 1 : productConfigLast.getSort() + 1;
    }

    @Transactional
    @Override
    public ProductConfig updateProductConfig(ProductConfigUpdateCommand productConfigUpdateCommand, HttpServletRequest request) {
        //获取用户信息
        String token = request.getHeader("Authorization");
        UserCenterResult<UserDetailVO> userDetailVOUserCenterResult = UserCenterService.userInfo(token);
        UserDetailVO data = userDetailVOUserCenterResult.getData();
        if (null == data) {
            throw new BusinessException("用户信息获取失败");
        }
        ProductConfig productConfig = this.getById(productConfigUpdateCommand.getId());
        BeanUtils.copyProperties(productConfigUpdateCommand, productConfig);
        List<Long> marketMakerIds = productConfigUpdateCommand.getMarketMakerIds();
        if (!CollectionUtils.isEmpty(marketMakerIds)) {
            marketMakerIds.removeIf(Objects::isNull);
        }
        productConfig.setMarketMakerIds(JSONObject.toJSONString(marketMakerIds));
        productConfig.setOperator(data.getName());
        productConfig.setUpdateTime(System.currentTimeMillis());

        boolean isSuc = this.updateById(productConfig);

        if (isSuc) {
            return this.getById(productConfig.getId());
        }
        throw new BusinessException("资源保存失败");
    }


    @Override
    public IPage<ProductConfigDTO> getProductConfigList(ProductConfigQueryCommand productConfigQueryCommand) {
        Page<ProductConfig> pg = new Page<>(productConfigQueryCommand.getCurrent(), productConfigQueryCommand.getSize());
        QueryWrapper<ProductConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(productConfigQueryCommand.getMultiple() != null, ProductConfig::getMultiple, productConfigQueryCommand.getMultiple())
                .eq(productConfigQueryCommand.getIsRelease() != null, ProductConfig::getIsRelease, productConfigQueryCommand.getIsRelease())
                .gt(productConfigQueryCommand.getCreateTimeStart() != null, ProductConfig::getCreateTime, productConfigQueryCommand.getCreateTimeStart())
                .lt(productConfigQueryCommand.getCreateTimeEnd() != null, ProductConfig::getCreateTime, productConfigQueryCommand.getCreateTimeEnd())
                .like(productConfigQueryCommand.getDealPair() != null, ProductConfig::getDealPair, productConfigQueryCommand.getDealPair())
                .orderByAsc(ProductConfig::getSort);
        this.page(pg, queryWrapper);
        Page<ProductConfigDTO> pgDto = new Page<>();
        if (!CollectionUtils.isEmpty(pg.getRecords())) {
            BeanUtils.copyProperties(pg, pgDto);
            pgDto.setRecords(toDtos(pg.getRecords()));
        }
        return pgDto;
    }

    @Override
    public ProductConfigDTO getProductConfig(ProductConfigQueryCommand command) {
        ProductConfigDTO productConfigDTO = new ProductConfigDTO();
        QueryWrapper<ProductConfig> queryWrapper = new QueryWrapper();
        queryWrapper.lambda()
                .eq(command.getId() != null, ProductConfig::getId, command.getId())
                .like(command.getDealPair() != null, ProductConfig::getDealPair, command.getDealPair())
                .last("limit 1");
        if (queryWrapper.isEmptyOfWhere()) {
            return null;
        }
        ProductConfig productConfig = this.getOne(queryWrapper);
        if (null == productConfig) {
            return null;
        }
        BeanUtils.copyProperties(productConfig, productConfigDTO);
        productConfigDTO.setMarketMakerIds(JSONArray.parseArray(productConfig.getMarketMakerIds(), Long.class));
        return productConfigDTO;
    }

    @Transactional
    @Override
    public void sortProductConfig(ProductSortCommand productSortCommand) {
        ProductConfig productConfig = this.getById(productSortCommand.getProductId());
        if (null == productConfig) {
            throw new BusinessException("id为:" + productSortCommand.getProductId() + "的产品不存在");
        }
        Integer sort = productSortCommand.getSort();
        Integer oldSort = productConfig.getSort();
        if (sort < oldSort) {
            this.getBaseMapper().upSort(sort, oldSort);
        } else {
            this.getBaseMapper().downSort(sort, oldSort);
        }
        ProductConfig productConfigForUpdate = new ProductConfig();
        productConfigForUpdate.setId(productSortCommand.getProductId());
        productConfigForUpdate.setSort(sort);
        this.updateById(productConfigForUpdate);
    }

    @Override
    public List<ProductConfigDTO> getProductConfigListAll() {
        List<ProductConfig> productConfigs = this.list();
        if (productConfigs == null) {
            return null;
        }
        return productConfigs.stream().map(e -> {
            ProductConfigDTO dto = new ProductConfigDTO();
            BeanUtils.copyProperties(e, dto);
            dto.setMarketMakerIds(JSON.parseArray(e.getMarketMakerIds(), Long.class));
            return dto;
        }).collect(Collectors.toList());
    }

    @Override
    public List<ConfigurableData> getDealPairList() {
        return configurableDataMapper.selectList(Wrappers.emptyWrapper());
    }

    @Override
    public List<String> getCoinNameList() {
        List<ProductConfig> productConfigs = this.list();
        return productConfigs.stream().flatMap(e -> {
            ArrayList<String> stringArrayList = new ArrayList<>();
            stringArrayList.add(e.getCoinTrade());
            stringArrayList.add(e.getCoinValuation());
            return stringArrayList.stream();
        }).distinct().collect(Collectors.toList());
    }

    @Override
    public Integer etfMaintainSwitchOn() {
        redisManager.set(RedisKeysEnum.ETF_DEAL_PAIR_SWITCH.getKey(), EtfMainSwitcher.ON.getCode());
        kafkaTemplate.send(KafkaTopicKeys.ETF_DEAL_PAIR_SWITCH, String.valueOf(EtfMainSwitcher.ON.getCode()));
        log.info("已更新etf维护开关为:{}", redisManager.get(RedisKeysEnum.ETF_DEAL_PAIR_SWITCH.getKey()));
        return EtfMainSwitcher.ON.getCode();
    }

    @Override
    public Integer etfMaintainSwitchOff() {
        redisManager.set(RedisKeysEnum.ETF_DEAL_PAIR_SWITCH.getKey(), EtfMainSwitcher.OFF.getCode());
        kafkaTemplate.send(KafkaTopicKeys.ETF_DEAL_PAIR_SWITCH, String.valueOf(EtfMainSwitcher.OFF.getCode()));
        log.info("已更新etf维护开关为:{}", redisManager.get(RedisKeysEnum.ETF_DEAL_PAIR_SWITCH.getKey()));
        return EtfMainSwitcher.OFF.getCode();
    }

    @Transactional
    @Override
    public Boolean etfReleaseSwitch(Long productId) {
        ProductConfig productConfig = this.getById(productId);
        productConfig.setIsRelease(!productConfig.getIsRelease());
        this.updateById(productConfig);

        if (productConfig.getIsRelease()) {
            etfOpenApiFeign.updateTask(this.toDto(productConfig));
            quotationManager.onTask(productConfig.getDealPairTrack());
        } else {
            etfOpenApiFeign.deleateTask(this.toDto(productConfig));
            quotationManager.subTask(productConfig.getDealPairTrack());
        }

        return productConfig.getIsRelease();
    }

    @Transactional
    @Override
    public Boolean etfbuyswitch(Long productId) {
        ProductConfig productConfig = this.getById(productId);
        productConfig.setSwitchBuyRedemption(!productConfig.getSwitchBuyRedemption());
        this.updateById(productConfig);
        return productConfig.getSwitchBuyRedemption();
    }

    @Override
    public List<ProductConfigDTO> getProductConfigListAllByCondition(ProductConfigQueryCommand productConfigQueryCommand) {
        List<EtfMultipleDTO> etfMutiple = this.getEtfMutiple();
        Map<Integer, String> mutipleMap = etfMutiple.stream().collect(Collectors.toMap(EtfMultipleDTO::getValue, EtfMultipleDTO::getName));
        QueryWrapper<ProductConfig> queryWrapper = new QueryWrapper();
        queryWrapper.lambda()
                .gt(productConfigQueryCommand.getCreateTimeStart() != null, ProductConfig::getCreateTime, productConfigQueryCommand.getCreateTimeStart())
                .lt(productConfigQueryCommand.getCreateTimeEnd() != null, ProductConfig::getCreateTime, productConfigQueryCommand.getCreateTimeEnd())
                .eq(productConfigQueryCommand.getDealPair() != null, ProductConfig::getDealPair, productConfigQueryCommand.getDealPair())
                .eq(productConfigQueryCommand.getIsRelease() != null, ProductConfig::getIsRelease, productConfigQueryCommand.getIsRelease())
                .orderByAsc(ProductConfig::getSort);
        List<ProductConfig> list = this.list(queryWrapper);
        return list.stream().map(e -> {
            ProductConfigDTO dto = new ProductConfigDTO();
            BeanUtils.copyProperties(e, dto);
            dto.setMarketMakerIds(JSON.parseArray(e.getMarketMakerIds(), Long.class));
            dto.setMultipleName(mutipleMap.get(dto.getMultiple()));
            return dto;
        }).collect(Collectors.toList());
    }

    @Override
    public Boolean etfReleaseSwitchOn(Long productId) {
        ProductConfig productConfig = this.getById(productId);
        productConfig.setIsRelease(true);
        this.updateById(productConfig);
        try {
            etfOpenApiFeign.updateTask(this.toDto(productConfig));
            quotationManager.onTask(productConfig.getDealPairTrack());
        }catch (Exception e){
            log.error("开关同步失败:",e);
            throw new BusinessException("开关同步失败,请重试");
        }
        return productConfig.getIsRelease();
    }

    @Override
    public Boolean etfReleaseSwitchOff(Long productId) {
        ProductConfig productConfig = this.getById(productId);
        productConfig.setIsRelease(false);
        this.updateById(productConfig);
        try{
            etfOpenApiFeign.deleateTask(this.toDto(productConfig));
            quotationManager.subTask(productConfig.getDealPairTrack());
        }catch (Exception e){
            log.error("开关同步失败:",e);
            throw new BusinessException("开关同步失败,请重试");
        }
        return productConfig.getIsRelease();
    }

    @Override
    public Boolean etfbuyswitchOn(Long productId) {
        ProductConfig productConfig = this.getById(productId);
        productConfig.setSwitchBuyRedemption(true);
        this.updateById(productConfig);
        return productConfig.getSwitchBuyRedemption();
    }

    @Override
    public Boolean etfbuyswitchOff(Long productId) {
        ProductConfig productConfig = this.getById(productId);
        productConfig.setSwitchBuyRedemption(false);
        this.updateById(productConfig);
        return productConfig.getSwitchBuyRedemption();
    }

    @Override
    public List<EtfMultipleDTO> getEtfMutiple() {
        return userCenterManager.getEtfMutiple();
    }

    @Override
    public List<String> getDealPairListEtf() {
        List<ProductConfig> list = this.list();
        return list.stream().map(ProductConfig::getDealPair).collect(Collectors.toList());
    }


    private List<ProductConfigDTO> toDtos(List<ProductConfig> productConfigs) {
        List<ProductConfigDTO> productConfigDTOS = new ArrayList<>();
        List<Long> productIds = productConfigs.stream().map(ProductConfig::getId).collect(Collectors.toList());
        if (productIds.size() == 0) {
            return new ArrayList<>();
        }
        List<String> coinNames = productConfigs.stream().flatMap(e -> {
            ArrayList<String> stringArrayList = new ArrayList<>();
            stringArrayList.add(e.getCoinTrade());
            stringArrayList.add(e.getCoinValuation());
            return stringArrayList.stream();
        }).collect(Collectors.toList());
        //获取币种余额
        Map<String, RedemptionOrderStatistics> statisticeMap = new HashMap<>();
        try {
            List<RedemptionOrderStatistics> statisticeListByCoinNames = this.getStatisticeListByCoinNames(coinNames);
            if (!CollectionUtils.isEmpty(statisticeListByCoinNames)) {
                statisticeMap = statisticeListByCoinNames.stream().collect(Collectors.toMap(RedemptionOrderStatistics::getCountUnit, Function.identity()));
            }
        } catch (Exception e) {
            log.error("etf:币种:{}余额获取失败:{}", coinNames, e);
        }

        for (ProductConfig productConfig : productConfigs) {
            ProductConfigDTO productConfigDTO = new ProductConfigDTO();
            BeanUtils.copyProperties(productConfig, productConfigDTO);
            productConfigDTO.setMarketMakerIds(JSON.parseArray(productConfig.getMarketMakerIds(), Long.class));
            RedemptionOrderStatistics statisticsTr = statisticeMap.get(productConfig.getCoinTrade());
            RedemptionOrderStatistics statisticsVal = statisticeMap.get(productConfig.getCoinValuation());
            productConfigDTO.setCoinAmountTrade(null == statisticsTr ? null : statisticsTr.getTotalBalance());
            productConfigDTO.setCoinAmountValuation(null == statisticsVal ? null : statisticsVal.getTotalBalance());
            productConfigDTOS.add(productConfigDTO);
        }
        return productConfigDTOS;
    }


    private List<RedemptionOrderStatistics> getStatisticeListByCoinNames(List<String> coinNames) {
        QueryWrapper<RedemptionOrderStatistics> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().in(RedemptionOrderStatistics::getCountUnit, coinNames);
        return redemptionOrderStatisticsMapper.selectList(queryWrapper);
    }


    /**
     * 产品配置后置处理 : 保存净值到redis缓存 ,发送kafka消息,添加调仓记录
     *
     * @param productConfig
     */
    private void rebalanceInitProcess(ProductConfig productConfig) {
        BigDecimal trackPrice = quotationManager.getNewPrice(productConfig.getDealPairTrack());
        RebalanceRecordDTO rebalanceRecord = new RebalanceRecordDTO();
        rebalanceRecord.setCoinTrade(productConfig.getCoinTrade());
        //当期篮子-交易货币
        BigDecimal coinTradePrice = (productConfig.getPriceNetInit().multiply(BigDecimal.valueOf(productConfig.getMultiple().longValue()))).divide(trackPrice, 18, BigDecimal.ROUND_DOWN);
        //当期篮子-计价货币
        BigDecimal coinValuationPrice = productConfig.getPriceNetInit().subtract(coinTradePrice.multiply(trackPrice)).setScale(18, BigDecimal.ROUND_HALF_UP);
        rebalanceRecord.setCoinTradePrice(coinTradePrice);
        rebalanceRecord.setCoinTradePriceOld(coinTradePrice);
        rebalanceRecord.setCoinValuation(productConfig.getCoinValuation());
        rebalanceRecord.setCoinValuationPrice(coinValuationPrice);
        rebalanceRecord.setCoinValuationPriceOld(coinValuationPrice);
        rebalanceRecord.setCreateTime(System.currentTimeMillis());
        rebalanceRecord.setDealPair(productConfig.getDealPair());
        //本次调仓标的价格
        rebalanceRecord.setDealPairTrackPrice(trackPrice);
        //上次调仓标的价格
        rebalanceRecord.setDealPairTrackPriceOld(trackPrice);
        //标的交易对
        rebalanceRecord.setDealPairTrack(productConfig.getDealPairTrack());
        //本次调仓倍数
        rebalanceRecord.setLeverageMultiple(productConfig.getMultiple());
        //上次调仓倍数
        rebalanceRecord.setLeverageMultipleOld(BigDecimal.valueOf(productConfig.getMultiple()).setScale(3, RoundingMode.DOWN));
        //基本杠杆倍数
        rebalanceRecord.setMultiple(productConfig.getMultiple());
        //本次调仓净值
        rebalanceRecord.setNetWorth(productConfig.getPriceNetInit());
        //上次调仓净值
        rebalanceRecord.setNetWorthOld(productConfig.getPriceNetInit());
        rebalanceRecord.setType(RebalanceTypeEnum.THRESHOLD_CHECK_RE_BALANCE.getKey());
        //用户持仓总量
        rebalanceRecord.setUserPositionToal(new BigDecimal(0));
        rebalanceRecordService.insert(rebalanceRecord);
    }

    private void sendNetWorthInitKafka(ProductConfig productConfig) {
        NetWorthMessage netWorth = NetWorthMessage.builder()
                .dealPair(productConfig.getDealPair())
                .time(System.currentTimeMillis())
                .val(productConfig.getPriceNetInit())
                .build();

        //缓存基础净值
        redisManager.set(String.format(RedisKeysEnum.ETF_NET_WORTH_NEW.getKey(), productConfig.getDealPair()), JSONObject.toJSONString(netWorth));

        //发送kafaka消息给XT
        try {
            kafkaTemplate.send(KafkaTopicKeys.ETF_DEAL_PAIR_NET_WORTH, JSONObject.toJSONString(netWorth)).get(10, TimeUnit.SECONDS);
        } catch (Exception e) {
            log.error("etf产品净值同步:kafka消息发送失败:", e);
            throw new BusinessException("etf产品净值同步:kafka消息发送失败");
        }
    }

    private ProductConfigDTO toDto(ProductConfig productConfig) {
        ProductConfigDTO productConfigDTO = new ProductConfigDTO();
        BeanUtils.copyProperties(productConfig, productConfigDTO);
        productConfigDTO.setMarketMakerIds(JSONArray.parseArray(productConfig.getMarketMakerIds(), Long.class));
        return productConfigDTO;
    }
}