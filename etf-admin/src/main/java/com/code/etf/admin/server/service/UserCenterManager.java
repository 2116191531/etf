package com.code.etf.admin.server.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.code.etf.admin.api.dto.EtfMultipleDTO;
import com.code.etf.admin.server.config.properties.EtfProcessProperties;
import com.code.springboot.web.exception.BusinessException;
import com.code.user.center.api.vo.AdminDetailVO;
import com.code.user.center.starter.config.PlatformProperties;
import com.code.user.center.starter.config.UserCenterProperties;
import com.code.user.center.starter.service.RestTemplateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Component
public class UserCenterManager {

    @Autowired
    private UserCenterProperties userCenterProperties;
    @Autowired
    private PlatformProperties platformProperties;
    @Autowired
    private EtfProcessProperties etfProcessProperties;


    @Autowired
    private RestTemplate restTemplate;

    private static EtfMultipleDTO apply(Object e) {
        EtfMultipleDTO etfMultipleDTO = new EtfMultipleDTO();
        Map j = (HashMap) e;
        etfMultipleDTO.setName(j.get("name").toString());
        etfMultipleDTO.setValue(Integer.valueOf(j.get("value").toString()));
        return etfMultipleDTO;
    }

    public List<EtfMultipleDTO> getEtfMutiple() {
        List<EtfMultipleDTO> collect = new ArrayList<>();
        String url = userCenterProperties.getProtocol() + "://" + userCenterProperties.getHost() + ":" + userCenterProperties.getPort() + "/user-center/dictionary/system/valid?systemId={systemId}&code={etfDirCode}";
        JSONObject result = restTemplate.getForObject(url, JSONObject.class, Integer.valueOf(platformProperties.getSystemId()), etfProcessProperties.getDictMulCode());
        JSONObject data = result.getJSONObject("data");
        if (null != data) {
            JSONArray childList = data.getJSONArray("childList");
            collect = childList.stream().map(UserCenterManager::apply).collect(Collectors.toList());
        }
        return collect;
    }
}
