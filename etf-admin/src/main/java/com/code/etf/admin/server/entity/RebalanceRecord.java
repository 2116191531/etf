package com.code.etf.admin.server.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 调仓记录
 * </p>
 *
 * @author 七楼
 * @since 2021-06-29
 */
@TableName("etf_rebalance_record")
@Data
public class RebalanceRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private String dealPair;
    private String type;
    private String coinTrade;
    private String coinValuation;
    private BigDecimal netWorthOld;
    private BigDecimal netWorth;
    private BigDecimal dealPairTrackPriceOld;
    private BigDecimal dealPairTrackPrice;
    private BigDecimal coinTradePriceOld;
    private String dealPairTrack;
    private BigDecimal coinTradePrice;
    private BigDecimal coinValuationPriceOld;
    private BigDecimal coinValuationPrice;
    private BigDecimal leverageMultipleOld;
    private Integer leverageMultiple;
    private Integer multiple;
    private BigDecimal userPositionToal;
    @TableField(fill = FieldFill.INSERT)
    private Long createTime;
    private String createBy;
    @TableField(fill = FieldFill.UPDATE)
    private Long updateTime;
    private String updateBy;

    public static final class Field {
        public static final String CREATE_TIME = "create_time";
        public static final String MULTIPLE = "multiple";
        public static final String TYPE = "type";
        public static final String DEAL_PAIR = "deal_pair";
    }
}
