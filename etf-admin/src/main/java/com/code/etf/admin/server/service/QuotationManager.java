package com.code.etf.admin.server.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.code.commons.mojo.result.ResultStatus;
import com.code.etf.admin.api.dto.SysTaskDTO;
import com.code.etf.admin.api.menu.QuotationTaskTypeDict;
import com.code.etf.admin.server.config.properties.QuotationProcessProperties;
import com.code.springboot.web.exception.BusinessException;
import com.code.user.center.starter.service.RestTemplateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Collections;

@Slf4j
@Component
public class QuotationManager {
    @Autowired
    private QuotationProcessProperties quotationProcessProperties;

    public BigDecimal getNewPrice(String dealPairName) {
        String url = quotationProcessProperties.getUrl() + "/deal-pair-price/" + dealPairName;
        ResponseEntity<BigDecimal> bigDecimalResponseEntity = RestTemplateUtils.get(url, BigDecimal.class);
        log.info("访问行情地址:{}结果{}",url, JSON.toJSONString(bigDecimalResponseEntity));
        if (null != bigDecimalResponseEntity.getBody()) {
            log.info("查询到的最新标的价格为:{}",bigDecimalResponseEntity.getBody());
            if (bigDecimalResponseEntity.getBody().compareTo(BigDecimal.ZERO) <= 0) {
                throw new BusinessException("标的价格获取异常,价格为负数,请检查风控配置");
            }
        }
        return bigDecimalResponseEntity.getBody();
    }

    public void onTask(String dealPair) {
        String url = quotationProcessProperties.getUrl() + "/sys-task/on-task";
        execute(dealPair, url);
    }


    public void subTask(String dealPair) {
        String url = quotationProcessProperties.getUrl() + "/sys-task/sub-task";
        execute(dealPair, url);
    }

    private void execute(String dealPair, String url) {
        SysTaskDTO sysTaskDTO = new SysTaskDTO();
        sysTaskDTO.setDealPair(dealPair);
        sysTaskDTO.setType(QuotationTaskTypeDict.DEAL_PAIR_PRICE);

        try {
            ResponseEntity<JSONObject> post = RestTemplateUtils.post(url, Collections.singleton(sysTaskDTO), JSONObject.class);
            if (post.getBody() == null) {
                throw new BusinessException("quotation行情系统发布任务失败!");
            }
            Integer code = post.getBody().getInteger("code");
            String msg = post.getBody().getString("message");
            if (!ResultStatus.SUCCESS.getCode().equals(code)) {
                throw new BusinessException("quotation行情系统发布任务失败:" + msg);
            }
        } catch (Exception e) {
            throw new BusinessException("quotation行情系统发布任务失败!", e);
        }
    }
}
