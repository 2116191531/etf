package com.code.etf.admin.server;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@SpringBootApplication(scanBasePackages = "com.code.etf.admin.server")
@EnableTransactionManagement
@EnableDiscoveryClient
@MapperScan("com.code.etf.admin.server.mapper")
public class EtfAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(EtfAdminApplication.class, args);
    }
}
