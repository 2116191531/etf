package com.code.etf.admin.server.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.code.etf.admin.api.command.ProductSortCommand;
import com.code.etf.admin.api.dto.*;
import com.code.etf.admin.server.entity.ConfigurableData;
import com.code.etf.admin.server.entity.ProductConfig;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * etf产品配置表 服务类
 * </p>
 *
 * @author 七楼
 * @since 2021-06-29
 */
public interface ProductConfigService extends IService<ProductConfig> {

    ProductConfig saveProductConfig(ProductConfigSaveCommand productConfigSaveCommand, HttpServletRequest request);

    ProductConfig updateProductConfig(ProductConfigUpdateCommand productConfigUpdateCommand, HttpServletRequest request);

    IPage<ProductConfigDTO> getProductConfigList(ProductConfigQueryCommand productConfigQueryCommand);

    ProductConfigDTO getProductConfig(ProductConfigQueryCommand dealPair);

    void sortProductConfig(ProductSortCommand productSortCommand);

    List<ProductConfigDTO> getProductConfigListAll();

    List<ConfigurableData> getDealPairList();

    List<String> getCoinNameList();

    Integer etfMaintainSwitchOn();

    Integer etfMaintainSwitchOff();

    Boolean etfReleaseSwitch(Long productId);

    Boolean etfbuyswitch(Long productId);

    List<ProductConfigDTO> getProductConfigListAllByCondition(ProductConfigQueryCommand productConfigQueryCommand);

    Boolean etfReleaseSwitchOn(Long productId);

    Boolean etfReleaseSwitchOff(Long productId);

    Boolean etfbuyswitchOn(Long productId);

    Boolean etfbuyswitchOff(Long productId);

    List<EtfMultipleDTO> getEtfMutiple();

    List<String> getDealPairListEtf();

}
