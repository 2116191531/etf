package com.code.etf.admin.server.mapper.primary;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code.etf.admin.server.entity.RebalanceRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 调仓记录 Mapper 接口
 * </p>
 *
 * @author 七楼
 * @since 2021-06-29
 */
@Mapper
public interface RebalanceRecordMapper extends BaseMapper<RebalanceRecord> {

}
