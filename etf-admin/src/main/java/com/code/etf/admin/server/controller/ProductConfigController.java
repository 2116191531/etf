package com.code.etf.admin.server.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.code.commons.mojo.result.ResultData;
import com.code.etf.admin.api.command.ProductSortCommand;
import com.code.etf.admin.api.dto.*;
import com.code.etf.admin.api.menu.RedisKeysEnum;
import com.code.etf.admin.server.entity.ConfigurableData;
import com.code.etf.admin.server.service.OssService;
import com.code.etf.admin.server.service.ProductConfigService;
import com.code.etf.admin.server.service.UserCenterManager;
import com.code.springboot.redis.server.RedisManager;
import com.code.user.center.api.annotation.JwtIgnore;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * etf产品配置中心 配置后台接口
 */
@Api(tags = "etf产品配置中心 配置后台接口")
@Slf4j
@RestController
@RequestMapping("/product-config")
public class ProductConfigController {

    @Autowired
    private RedisManager redisManager;
    @Autowired
    private ProductConfigService productConfigService;
    @Autowired
    private UserCenterManager userCenterManager;
    @Autowired
    private OssService ossService;

    @GetMapping("/list")
    @ApiOperation(value = "查询所有已发布的配置列表", notes = "查询已发布的配置列表")
    @ResponseBody
    @JwtIgnore
    public List<ProductConfigDTO> findAll() {
        return productConfigService.getProductConfigListAll();
    }

    @ApiOperation(value = "根据条件查询产品配置详情", notes = "根据条件查询产品配置详情")
    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    public ProductConfigDTO getProductConfig(ProductConfigQueryCommand productConfigQueryCommand) {
        return productConfigService.getProductConfig(productConfigQueryCommand);
    }

    @ApiOperation(value = "根据条件查询产品配置列表", notes = "查询产品配置列表")
    @RequestMapping(value = "/list-by-condition", method = RequestMethod.GET)
    public IPage<ProductConfigDTO> getProductConfigList(ProductConfigQueryCommand productConfigQueryCommand) {
        return productConfigService.getProductConfigList(productConfigQueryCommand);
    }

    @ApiOperation(value = "查询所有产品配置", notes = "根据条件查询所有产品配置")
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<ProductConfigDTO> getProductConfigAll() {
        return productConfigService.getProductConfigListAll();
    }

    @ApiOperation(value = "保存产品配置", notes = "保存产品配置")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ResultData saveProductConfig(@Validated @RequestBody ProductConfigSaveCommand productConfigSaveCommand, HttpServletRequest request) {
        return ResultData.success(productConfigService.saveProductConfig(productConfigSaveCommand,request));
    }

    @ApiOperation(value = "修改产品配置", notes = "修改可修改的产品配置项")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ResultData updateProductConfig(@RequestBody @Validated ProductConfigUpdateCommand productConfigUpdateCommand, HttpServletRequest request) {
        return ResultData.success(productConfigService.updateProductConfig(productConfigUpdateCommand,request));
    }

    @ApiOperation(value = "产品配置列表排序", notes = "产品配置列表排序")
    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    public void sortProductConfig(@RequestBody @Validated ProductSortCommand productSortCommand) {
        productConfigService.sortProductConfig(productSortCommand);
    }


    @ApiOperation(value = "获取产品倍数列表", notes = "获取etf产品杠杆倍数列表")
    @RequestMapping(value = "/etf-multiple", method = RequestMethod.GET)
    public List<EtfMultipleDTO> getEtfMultiple() {
        return productConfigService.getEtfMutiple();
    }


    @ApiOperation(value = "获取xt管理后台已配置的交易对列表", notes = "获取交易对列表")
    @RequestMapping(value = "/deal-pair-list", method = RequestMethod.GET)
    public List<ConfigurableData> getDealPairList() {
        return productConfigService.getDealPairList();
    }


    @ApiOperation(value = "获取etf-admin后台配置的交易对名称列表ETF", notes = "获取交易对名称列表")
    @RequestMapping(value = "/deal-pair-list-etf", method = RequestMethod.GET)
    public List<String> getDealPairListEtf() {
        return productConfigService.getDealPairListEtf();
    }


    @ApiOperation(value = "获取币种列表", notes = "获取币种列表")
    @RequestMapping(value = "/deal-coin-list", method = RequestMethod.GET)
    public List<String> getCoinNameList() {
        return productConfigService.getCoinNameList();
    }


    @ApiOperation(value = "etf产品发布开关", notes = "etf产品发布开关")
    @RequestMapping(value = "/etf-release-switch", method = RequestMethod.GET)
    public Boolean etfReleaseSwitch(@RequestParam Long productId){
        return productConfigService.etfReleaseSwitch(productId);
    }

    //-----------------------------------------------------新开关------
    @ApiOperation(value = "开启etf产品发布开关", notes = "开启etf产品发布开关")
    @RequestMapping(value = "/etf-release-switch-on", method = RequestMethod.GET)
    public Boolean etfReleaseSwitchOn(@RequestParam Long productId){
        return productConfigService.etfReleaseSwitchOn(productId);
    }


    @ApiOperation(value = "关闭etf产品发布开关", notes = "关闭etf产品发布开关")
    @RequestMapping(value = "/etf-release-switch-off", method = RequestMethod.GET)
    public Boolean etfReleaseSwitchOff(@RequestParam Long productId){
        return productConfigService.etfReleaseSwitchOff(productId);
    }

    @ApiOperation(value = "开启etf产品申赎开关", notes = "etf产品申赎开关")
    @RequestMapping(value = "/etf-buy-switch-on", method = RequestMethod.GET)
    public Boolean etfBuySwitchOn(@RequestParam Long productId){
        return productConfigService.etfbuyswitchOn(productId);
    }


    @ApiOperation(value = "关闭etf产品申赎开关", notes = "etf产品申赎开关")
    @RequestMapping(value = "/etf-buy-switch-off", method = RequestMethod.GET)
    public Boolean etfBuySwitchOff(@RequestParam Long productId){
        return productConfigService.etfbuyswitchOff(productId);
    }

    //--------------------------------------------------------------


    @ApiOperation(value = "etf产品申赎开关", notes = "etf产品申赎开关")
    @RequestMapping(value = "/etf-buy-switch", method = RequestMethod.GET)
    public Boolean etfBuySwitch(@RequestParam Long productId){
        return productConfigService.etfbuyswitch(productId);
    }


    @ApiOperation(value = "etf维护开启开关开启", notes = "etf维护开关")
    @RequestMapping(value = "/etf-maintain-switch-on", method = RequestMethod.GET)
    public Integer etfMaintainSwitchOn() {
        return productConfigService.etfMaintainSwitchOn();
    }


    @ApiOperation(value = "etf维护关闭开关关闭", notes = "etf维护开关")
    @RequestMapping(value = "/etf-maintain-switch-off", method = RequestMethod.GET)
    public Integer etfMaintainSwitchOff() {
        return productConfigService.etfMaintainSwitchOff();
    }

    @ApiOperation(value = "获取etf维护开关", notes = "获取etf维护开关 1 开启 0 关闭")
    @RequestMapping(value = "/get-etf-maintain-switch", method = RequestMethod.GET)
    @ResponseBody
    public ResultData<Integer> getEtfMaintainSwitch() {
        Object val = redisManager.get(RedisKeysEnum.ETF_DEAL_PAIR_SWITCH.getKey());
        if (val == null) {
            synchronized ("etf"){
                 val = redisManager.get(RedisKeysEnum.ETF_DEAL_PAIR_SWITCH.getKey());
                if (val == null ){
                    return ResultData.success(productConfigService.etfMaintainSwitchOn());
                }
            }
        }

        return ResultData.success(Integer.valueOf(String.valueOf(val)));
    }

    @ApiOperation(value = "上传产品图片", notes = "上传至oss并返回url")
    @RequestMapping(value = "/upload-icon", method = RequestMethod.POST)
    public ResultData<String> fileUpload(@ApiParam(value = "上传图片", required = true, type = "MultipartFile") MultipartFile file) {
        return ResultData.success(ossService.uploadFile(file));
    }


}
