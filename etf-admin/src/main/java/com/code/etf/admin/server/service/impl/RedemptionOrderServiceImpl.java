package com.code.etf.admin.server.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.code.etf.admin.api.dto.*;
import com.code.etf.admin.api.menu.OrderStatusDict;
import com.code.etf.admin.server.entity.RedemptionOrder;
import com.code.etf.admin.server.entity.RedemptionOrderStatistics;
import com.code.etf.admin.server.mapper.xt.RedemptionOrderMapper;
import com.code.etf.admin.server.mapper.xt.RedemptionOrderStatisticsMapper;
import com.code.etf.admin.server.service.RedemptionOrderService;
import com.code.etf.admin.server.service.XtApiService;
import com.code.springboot.web.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class RedemptionOrderServiceImpl extends ServiceImpl<RedemptionOrderMapper, RedemptionOrder> implements RedemptionOrderService {

    @Autowired
    private RedemptionOrderStatisticsMapper redemptionOrderStatisticsMapper;
    @Autowired
    private XtApiService xtApiService;

    @Override
    public IPage<RedemptionOrderDTO> getRedemptionOrderList(RedemptionOrderCommand redemptionOrderCommand) {
        Page<RedemptionOrder> pg = new Page<>(redemptionOrderCommand.getCurrent(), redemptionOrderCommand.getSize());
        QueryWrapper<RedemptionOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(StringUtils.isNotBlank(redemptionOrderCommand.getStatus()), RedemptionOrder::getStatus, redemptionOrderCommand.getStatus())
                .eq(StringUtils.isNotBlank(redemptionOrderCommand.getDealType()), RedemptionOrder::getDealType, redemptionOrderCommand.getDealType())
                .eq(StringUtils.isNotBlank(redemptionOrderCommand.getUType()), RedemptionOrder::getuType, redemptionOrderCommand.getUType())
                .eq(redemptionOrderCommand.getUId() != null, RedemptionOrder::getuId, redemptionOrderCommand.getUId())
                .ge(redemptionOrderCommand.getCreateTimeStart() != null, RedemptionOrder::getCreateTime, redemptionOrderCommand.getCreateTimeStart())
                .le(redemptionOrderCommand.getGetCreateTimeEnd() != null, RedemptionOrder::getCreateTime, redemptionOrderCommand.getGetCreateTimeEnd())
                .like(redemptionOrderCommand.getCountUnit() != null, RedemptionOrder::getCountUnit, redemptionOrderCommand.getCountUnit())
                .orderByDesc(RedemptionOrder::getCreateTime)
        ;
        this.page(pg, queryWrapper);
        Page<RedemptionOrderDTO> pgDto = new Page<>();
        BeanUtils.copyProperties(pg, pgDto);
        List<RedemptionOrderDTO> resultList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(pg.getRecords())) {
            for (RedemptionOrder record : pg.getRecords()) {
                RedemptionOrderDTO redemptionOrderDTO = new RedemptionOrderDTO();
                BeanUtils.copyProperties(record, redemptionOrderDTO);
                redemptionOrderDTO.setId(String.valueOf(record.getId()));
                resultList.add(redemptionOrderDTO);
            }
        }
        pgDto.setRecords(resultList);
        return pgDto;
    }

    @Override
    public RedemptionOrderStatisticsDTO getRedemptionOrderStatistics(String countUnit) {
        RedemptionOrderStatisticsDTO redemptionOrderStatisticsDTO = new RedemptionOrderStatisticsDTO();
        QueryWrapper<RedemptionOrderStatistics> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(RedemptionOrderStatistics::getCountUnit, countUnit).last("limit 1");
        RedemptionOrderStatistics redemptionOrderStatistics = redemptionOrderStatisticsMapper.selectOne(queryWrapper);
        if (null == redemptionOrderStatistics) {
            return redemptionOrderStatisticsDTO;
        }
        //发行总量
        BigDecimal totalIssueAmount = redemptionOrderStatistics.getTotalIssueAmount();
        totalIssueAmount = totalIssueAmount.setScale(18, BigDecimal.ROUND_HALF_UP);
        //总余额
        BigDecimal totalBalance = redemptionOrderStatistics.getTotalBalance();
        totalBalance = totalBalance.setScale(18, BigDecimal.ROUND_HALF_UP);
        //做市总持仓
        BigDecimal marketMakeTotalHoldAmount = redemptionOrderStatistics.getMarketMakeTotalHoldAmount();
        marketMakeTotalHoldAmount = marketMakeTotalHoldAmount.setScale(18, BigDecimal.ROUND_HALF_UP);
        //手续费总持仓
        BigDecimal feeTotalAmount = redemptionOrderStatistics.getFeeTotalAmount();
        feeTotalAmount = feeTotalAmount.setScale(18, BigDecimal.ROUND_HALF_UP);
        BeanUtils.copyProperties(redemptionOrderStatistics, redemptionOrderStatisticsDTO);

        //已申购总额=发行总量-账户份额余额
        if (BigDecimal.ZERO.compareTo(totalIssueAmount) != 0 && BigDecimal.ZERO.compareTo(totalBalance) != 0) {
            redemptionOrderStatisticsDTO.setUserBuyTotalAmount(redemptionOrderStatistics.getTotalIssueAmount().subtract(redemptionOrderStatistics.getTotalBalance()));
        }
        BigDecimal userBuyTotalAmount = redemptionOrderStatisticsDTO.getUserBuyTotalAmount();
        userBuyTotalAmount = userBuyTotalAmount.setScale(18, BigDecimal.ROUND_HALF_UP);
        //用户总持仓=已购申购总额-做市总持仓-手续费总持仓
        if (BigDecimal.ZERO.compareTo(userBuyTotalAmount) != 0) {
            redemptionOrderStatisticsDTO.setUserTotalHoldAmount(userBuyTotalAmount.subtract(redemptionOrderStatistics.getMarketMakeTotalHoldAmount()).subtract(redemptionOrderStatistics.getFeeTotalAmount()));
        }

        //账户余额百分比
        log.info("参与计算的参数是:{}", JSON.toJSONString(redemptionOrderStatisticsDTO));
        if (BigDecimal.ZERO.compareTo(totalBalance) != 0 && BigDecimal.ZERO.compareTo(totalIssueAmount) != 0) {
            BigDecimal balancePercent = totalBalance.divide(totalIssueAmount, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            redemptionOrderStatisticsDTO.setTotalBalancePercent(null == balancePercent ? BigDecimal.ZERO : balancePercent);
        }
        //已申购总额百分比
        if (BigDecimal.ZERO.compareTo(userBuyTotalAmount) != 0 && BigDecimal.ZERO.compareTo(totalIssueAmount) != 0) {
            BigDecimal userBuyTotalPercent = new BigDecimal(100).subtract(redemptionOrderStatisticsDTO.getTotalBalancePercent());
            redemptionOrderStatisticsDTO.setUserBuyTotalAmountPercent(userBuyTotalPercent);
        }

        //手续费百分比
        if (BigDecimal.ZERO.compareTo(feeTotalAmount) != 0 && BigDecimal.ZERO.compareTo(userBuyTotalAmount) != 0) {
            BigDecimal feePercent = feeTotalAmount.divide(userBuyTotalAmount, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            redemptionOrderStatisticsDTO.setFeeTotalAmountPercent(null == feePercent ? BigDecimal.ZERO : feePercent);
        }

        //做市百分比
        if (BigDecimal.ZERO.compareTo(marketMakeTotalHoldAmount) != 0 && BigDecimal.ZERO.compareTo(userBuyTotalAmount) != 0) {
            BigDecimal marketPercent = marketMakeTotalHoldAmount.divide(userBuyTotalAmount, 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            redemptionOrderStatisticsDTO.setMarketMakeTotalHoldAmountPercent(null == marketPercent ? BigDecimal.ZERO : marketPercent);
        }
        //用户持仓百分比
        BigDecimal userTotalHoldAmount = redemptionOrderStatisticsDTO.getUserTotalHoldAmount();
        if (BigDecimal.ZERO.compareTo(userTotalHoldAmount) != 0 && BigDecimal.ZERO.compareTo(userBuyTotalAmount) != 0) {
            //BigDecimal userTotalHoldPercent = userTotalHoldAmount.divide(userBuyTotalAmount, 2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100));
            BigDecimal userTotalHoldPercent = new BigDecimal(100)
                    .subtract(redemptionOrderStatisticsDTO.getMarketMakeTotalHoldAmountPercent())
                    .subtract(redemptionOrderStatisticsDTO.getFeeTotalAmountPercent());
            redemptionOrderStatisticsDTO.setUserTotalHoldAmountPercent(userTotalHoldPercent);
        }
        return redemptionOrderStatisticsDTO;
    }


    @Override
    public List<RedemptionOrderStatisticsDTO> getAllRedemptionOrderStatistics() {
        List<RedemptionOrderStatistics> redemptionOrderStatistics = redemptionOrderStatisticsMapper.selectList(null);
        List<RedemptionOrderStatisticsDTO> redemptionOrderStatisticsDTOS = new ArrayList<>();
        BeanUtils.copyProperties(redemptionOrderStatistics, redemptionOrderStatisticsDTOS);
        return redemptionOrderStatisticsDTOS;
    }

    @Override
    public RedemptionOrderTotalDTO getRedemptionOrderTotalInfo(RedemptionOrderCommand redemptionOrderCommand) {
        QueryWrapper<RedemptionOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper
                .select("IFNULL(sum(count),0) as orderCountTotal,IFNULL(sum(price),0) as orderPriceTotal,count_unit as countUnit,\n" +
                        "price_unit as priceUnit")
                .lambda()
                .eq(redemptionOrderCommand.getCountUnit() != null, RedemptionOrder::getCountUnit, redemptionOrderCommand.getCountUnit())
                .eq(redemptionOrderCommand.getUId() != null, RedemptionOrder::getuId, redemptionOrderCommand.getUId())
                .eq(StringUtils.isNotBlank(redemptionOrderCommand.getStatus()), RedemptionOrder::getStatus, redemptionOrderCommand.getStatus())
                .eq(StringUtils.isNotBlank(redemptionOrderCommand.getDealType()), RedemptionOrder::getDealType, redemptionOrderCommand.getDealType())
                .eq(StringUtils.isNotBlank(redemptionOrderCommand.getUType()), RedemptionOrder::getuType, redemptionOrderCommand.getUType())
                .ge(redemptionOrderCommand.getCreateTimeStart() != null, RedemptionOrder::getCreateTime, redemptionOrderCommand.getCreateTimeStart())
                .le(redemptionOrderCommand.getGetCreateTimeEnd() != null, RedemptionOrder::getCreateTime, redemptionOrderCommand.getGetCreateTimeEnd())
                .last("group by count_unit,price_unit")
        ;
        List<Map<String, Object>> maps = this.getBaseMapper().selectMaps(queryWrapper);
        RedemptionOrderTotalDTO redemptionOrderTotalDTO = new RedemptionOrderTotalDTO();
        if (!CollectionUtils.isEmpty(maps)) {
            Map<String, Object> result = maps.get(0);
            redemptionOrderTotalDTO.setOrderCountTotal(new BigDecimal(result.get("orderCountTotal").toString()));
            redemptionOrderTotalDTO.setOrderPriceTotal(new BigDecimal(result.get("orderPriceTotal").toString()));
            redemptionOrderTotalDTO.setCountUnit(result.get("countUnit").toString());
            redemptionOrderTotalDTO.setPriceUnit(result.get("priceUnit").toString());
            redemptionOrderTotalDTO.setFeeUnit("usdt");
        }
        BigDecimal feeToUsdtTotal = this.getFeeToUsdtTotal(redemptionOrderCommand);
        redemptionOrderTotalDTO.setFeeTotalUsdt(null==feeToUsdtTotal?BigDecimal.ZERO:feeToUsdtTotal);
        return redemptionOrderTotalDTO;
    }

    private BigDecimal getFeeToUsdtTotal(RedemptionOrderCommand redemptionOrderCommand) {
        String countUnit = redemptionOrderCommand.getCountUnit();
        XtGetTickersResponse coin = xtApiService.getTicker(countUnit + "_usdt");
        if (null==coin||null==coin.getPrice()) {
            log.error("币种:{}汇率查询失败!",countUnit);
            throw new BusinessException("币种:"+countUnit+"汇率查询失败!");
        }
        BigDecimal price =  coin.getPrice();
        QueryWrapper<RedemptionOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("fee_unit as feeUnit,sum(fee) as fee")
                .lambda().groupBy(RedemptionOrder::getFeeUnit)
                .eq(RedemptionOrder::getStatus,2)
                .eq(redemptionOrderCommand.getUId() != null, RedemptionOrder::getuId, redemptionOrderCommand.getUId())
                .eq(StringUtils.isNotBlank(redemptionOrderCommand.getStatus()), RedemptionOrder::getStatus, redemptionOrderCommand.getStatus())
                .eq(StringUtils.isNotBlank(redemptionOrderCommand.getDealType()), RedemptionOrder::getDealType, redemptionOrderCommand.getDealType())
                .eq(StringUtils.isNotBlank(redemptionOrderCommand.getUType()), RedemptionOrder::getuType, redemptionOrderCommand.getUType())
                .ge(redemptionOrderCommand.getCreateTimeStart() != null, RedemptionOrder::getCreateTime, redemptionOrderCommand.getCreateTimeStart())
                .le(redemptionOrderCommand.getGetCreateTimeEnd() != null, RedemptionOrder::getCreateTime, redemptionOrderCommand.getGetCreateTimeEnd())
                .eq(redemptionOrderCommand.getCountUnit() != null, RedemptionOrder::getCountUnit, redemptionOrderCommand.getCountUnit())
        ;
        BigDecimal usdtTotal = BigDecimal.ZERO;
        BigDecimal countUnitPriceTotal=BigDecimal.ZERO;
        List<Map<String, Object>> maps = this.getBaseMapper().selectMaps(queryWrapper);
        if (!CollectionUtils.isEmpty(maps)) {
            Map<String, BigDecimal> collect = maps.stream().collect(Collectors.toMap(e -> e.get("feeUnit").toString(), e -> new BigDecimal(e.get("fee").toString())));
            usdtTotal = collect.get("usdt");
            countUnitPriceTotal=collect.get(countUnit);
        }
        return price.multiply(countUnitPriceTotal).add(null == usdtTotal ? new BigDecimal(0) : usdtTotal);
    }


}
