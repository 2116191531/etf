package com.code.etf.admin.server.config;

import com.aliyun.oss.ClientConfiguration;
import com.aliyun.oss.OSSClient;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "oss")
@Data
public class OssConfig {

    private String endpoint;
    private String accessKeyId;
    private String secretAccessKey;
    private String bucketName;
    private String webServer;

    @Bean
    OSSClient getOSSClient() {
        ClientConfiguration conf = new ClientConfiguration();
        conf.setMaxConnections(1000);
        conf.setConnectionTimeout(600000);
        conf.setSocketTimeout(600000);
        if (!webServer.endsWith("/")) {
            webServer = webServer + "/";
        }
        return new OSSClient(endpoint, accessKeyId, secretAccessKey, conf);
    }

}
