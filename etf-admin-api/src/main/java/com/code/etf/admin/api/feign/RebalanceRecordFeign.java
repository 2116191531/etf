package com.code.etf.admin.api.feign;

import com.code.commons.mojo.result.ResultData;
import com.code.etf.admin.api.dto.RebalanceRecordDTO;
import feign.hystrix.FallbackFactory;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author qilou
 * @date 2021-6-29
 */
@FeignClient(name = "etf-admin", fallbackFactory = RebalanceRecordFeign.RebalanceRecordFeignFallbackFactory.class)
@Repository
public interface RebalanceRecordFeign {


    @PostMapping("/etf-admin/rebalance-record")
    RebalanceRecordDTO create(@RequestBody @Validated RebalanceRecordDTO dto);


    @GetMapping("/etf-admin/rebalance-record/{id}")
    @ApiOperation(value = "查询单个", notes = "查询单个")
    RebalanceRecordDTO getOne(@PathVariable("id") Long id);


    @GetMapping("/etf-admin/rebalance-record/find-one-by-type-timing")
    ResultData<RebalanceRecordDTO> findOneByTypeTiming(@RequestParam String dealPair);

    @GetMapping("/etf-admin/open-api/find-one-by-now")
    ResultData<RebalanceRecordDTO> findOneRebalanceRecordByNow(@RequestParam String dealPair);


    @Slf4j
    @Component
    class RebalanceRecordFeignFallbackFactory implements FallbackFactory<RebalanceRecordFeign> {
        @Override
        public RebalanceRecordFeign create(Throwable throwable) {
            return new RebalanceRecordFeign() {

                @Override
                public RebalanceRecordDTO create(RebalanceRecordDTO dto) {
                    return null;
                }

                @Override
                public RebalanceRecordDTO getOne(Long id) {
                    return null;
                }

                @Override
                public ResultData<RebalanceRecordDTO> findOneByTypeTiming(String dealPair) {
                    return null;
                }

                @Override
                public ResultData<RebalanceRecordDTO> findOneRebalanceRecordByNow(String dealPair) {
                    return null;
                }

            };
        }
    }
}
