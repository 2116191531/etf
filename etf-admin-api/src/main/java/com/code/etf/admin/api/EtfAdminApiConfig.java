package com.code.etf.admin.api;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients("com.code.etf.admin.api")
public class EtfAdminApiConfig {


}
