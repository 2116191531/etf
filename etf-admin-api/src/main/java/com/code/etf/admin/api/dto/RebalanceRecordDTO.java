package com.code.etf.admin.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * <p>
 * 调仓记录
 * </p>
 *
 * @author 七楼
 * @since 2021-06-29
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "EtfRebalanceRecord对象", description = "调仓记录")
public class RebalanceRecordDTO {

    private Long id;

    @ApiModelProperty(value = "交易对")
    private String dealPair;

    @ApiModelProperty(value = "调仓类型：1 定时 2临时")
    private String type;

    @ApiModelProperty(value = "计价货币单位")
    private String coinTrade;

    @ApiModelProperty(value = "交易货币单位")
    private String coinValuation;

    @ApiModelProperty(value = "上一次调仓：净值")
    private BigDecimal netWorthOld;

    @ApiModelProperty(value = "本次调仓：净值")
    private BigDecimal netWorth;

    @ApiModelProperty(value = "上一次调仓：标的价格")
    private BigDecimal dealPairTrackPriceOld;

    @ApiModelProperty(value = "本次调仓：标的价格")
    private BigDecimal dealPairTrackPrice;

    @ApiModelProperty(value = "标的交易对")
    private String dealPairTrack;

    @ApiModelProperty(value = "调仓前：篮子-交易货币值")
    private BigDecimal coinTradePriceOld;

    @ApiModelProperty(value = "调仓后：篮子-交易货币")
    private BigDecimal coinTradePrice;

    @ApiModelProperty(value = "调仓前：篮子-计价货币值")
    private BigDecimal coinValuationPriceOld;

    @ApiModelProperty(value = "调仓后：篮子-计价货币")
    private BigDecimal coinValuationPrice;

    @ApiModelProperty(value = "调仓前：杠杠倍数")
    private BigDecimal leverageMultipleOld;

    @ApiModelProperty(value = "调仓后：杠杠倍数")
    private Integer leverageMultiple;

    @ApiModelProperty(value = "基础杠杠倍数")
    private Integer multiple;

    @ApiModelProperty(value = "用户持仓总量")
    private BigDecimal userPositionToal;

    private Long createTime;

    private String createBy;

    private Long updateTime;

    private String updateBy;
}
