package com.code.etf.admin.api.dto;

import com.code.etf.admin.api.command.PageCommand;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;


@Data
public class RedemptionOrderCommand extends PageCommand implements Serializable {

    @NotBlank(message = "货币单位不能为空")
    @ApiModelProperty(value = "货币单位")
    private String countUnit;

    @ApiModelProperty(value = "用户id")
    private Long uId;

    @ApiModelProperty(value = "订单状态：1 未交易 2 交易成功 3 交易失败 4 未知")
    private String status;

    @ApiModelProperty(value = "交易类型：1 申购 2 赎回")
    private String dealType;

    @ApiModelProperty(value = "用户类型")
    private String uType;

    private Long createTimeStart;

    private Long getCreateTimeEnd;

}
