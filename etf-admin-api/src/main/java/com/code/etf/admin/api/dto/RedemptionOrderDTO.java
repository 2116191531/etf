package com.code.etf.admin.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 申赎记录表
 * </p>
 *
 * @author wyf
 * @since 2021-07-08
 */
@Data
@ApiModel(value = "RedemptionOrder对象", description = "申赎记录表")
public class RedemptionOrderDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "订单id")
    private String id;

    @ApiModelProperty(value = "交易货币单位：例 btc3l")
    private String countUnit;

    @ApiModelProperty(value = "用户id")
    private Long uId;

    @ApiModelProperty(value = "用户类型 1普通 2做市")
    private String uType;

    @ApiModelProperty(value = "交易类型：1 申购 2 赎回")
    private String dealType;

    @ApiModelProperty(value = "订单状态：1 未交易 2 交易成功 3 交易失败 4 未知")
    private String status;

    @ApiModelProperty(value = "订单数量 (交易货币)")
    private BigDecimal count;


    @ApiModelProperty(value = "交易净值")
    private BigDecimal netWorth;

    @ApiModelProperty(value = "手续费率")
    private BigDecimal feeRate;

    @ApiModelProperty(value = "手续费")
    private BigDecimal fee;

    @ApiModelProperty(value = "手续费单位")
    private String feeUnit;

    @ApiModelProperty(value = "订单金额（计价货币）")
    private BigDecimal price;

    @ApiModelProperty(value = "订单金额单位")
    private String priceUnit;

    private Long createTime;

}
