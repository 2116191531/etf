package com.code.etf.admin.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * etf产品配置保存请求
 */
@Data
@ApiModel(value = "产品配置请求", description = "etf产品配置请求封装")
public class ProductConfigSaveCommand implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @NotBlank(message = "交易对不能为空")
    @ApiModelProperty(value = "交易对")
    private String dealPair;

    @NotNull(message = "倍数不能为空")
    @ApiModelProperty(value = "倍数")
    private Integer multiple;

    @DecimalMin(value = "0.0000000000000000001",message ="管理费(展示)不能为空,且必须大于0")
    @DecimalMax(value = "0.999999999999999999",message ="管理费(展示)不能为空,且不能大于等于1")
    @ApiModelProperty(value = "管理费率(展示)")
    private BigDecimal feeMangeExpect;

    @DecimalMin(value = "0.0000000000000000001",message ="管理费率(实际)不能为空,且必须大于0")
    @DecimalMax(value = "0.999999999999999999",message ="管理费率(实际)不能为空,且不能大于等于1")
    @ApiModelProperty(value = "管理费(实际)")
    private BigDecimal feeMangeActual;

    @DecimalMin(value = "0.0000000000000000001",message ="购费率不能为空,且必须大于0")
    @DecimalMax(value = "0.999999999999999999",message ="申购费率不能为空,且不能大于等于1")
    @ApiModelProperty(value = "申购费率")
    private BigDecimal feeRateBuy;

    @DecimalMin(value = "0.0000000000000000001",message ="申购费率不能为空,且必须大于0")
    @DecimalMax(value = "0.999999999999999999",message ="申购费率不能为空,且不能大于等于1")
    @ApiModelProperty(value = "赎回费率")
    private BigDecimal feeRateRedeem;

    @DecimalMin(value = "0.00000000000000000001",message ="最小申购不能为空,且必须大于0")
    @DecimalMin(value = "0.00000000000000000001", message = "最小申购不能为空,且不能低于0.00000000000000000001")
    @ApiModelProperty(value = "最小申购")
    private BigDecimal buyMinPrice;

    @DecimalMin(value = "0.00000000000000000001",message ="最小赎回不能为空,且必须大于0")
    @DecimalMin(value = "0.00000000000000000001", message = "最小赎回不能为空,且不能低于0.00000000000000000001")
    @ApiModelProperty(value = "最小赎回")
    private BigDecimal redeemMinPrice;

    @DecimalMin(value = "0.00000000000000000001",message ="产品维度每日购买限额不能为空,且必须大于0")
    @DecimalMin(value = "0.00000000000000000001", message = "产品维度每日购买限额不能为空,且不能低于0.00000000000000000001")
    @ApiModelProperty(value = "产品维度每日购买限额")
    private BigDecimal dayBuyLimitGlobal;

    @DecimalMin(value = "0.00000000000000000001", message = "产品维度每日赎回限额不能为空,且不能低于0.00000000000000000001")
    @ApiModelProperty(value = "产品维度每日赎回限额")
    private BigDecimal dayRedeemLimitGlobal;

    @DecimalMin(value = "0.00000000000000000001", message = "个人每日购买限额不能为空,且不能低于0.00000000000000000001")
    @ApiModelProperty(value = "个人每日购买限额")
    private BigDecimal dayBuyLimitPersonal;

    @DecimalMin(value = "0.00000000000000000001", message = "个人每日赎回限额不能为空,且不能低于0.00000000000000000001")
    @ApiModelProperty(value = "个人每日赎回限额")
    private BigDecimal dayRedeemLimitPersonal;

    @DecimalMin(value = "0.00000000000000000001", message = "再平衡阈值不能为空,且不能低于0.00000000000000000001")
    @ApiModelProperty(value = "	再平衡阈值（反方向涨跌幅）")
    private BigDecimal thresholdUpDown;

    @Min(message="定时平衡时间必须为 0-23",value=0)
    @Max(message="定时平衡时间必须为 0-23",value=23)
    @ApiModelProperty(value = "定时平衡时间(0-23（默认0点）)")
    private Integer timeEquilibrium;

    @ApiModelProperty(value = "做市用户ids 数组")
    private List<Long> marketMakerIds;

    @NotBlank(message = "标的交易对不能为空")
    @ApiModelProperty(value = "标的交易对")
    private String dealPairTrack;

    @NotNull(message = "发行时间不能为空")
    @ApiModelProperty(value = "发行时间")
    private Long issueTime;

    //@NotNull(message = "初始净值价格不能为空")
    @DecimalMin(value = "0.00000000000000000001", message = "初始净值价格不能为空,且不能低于0.00000000000000000001")
    @ApiModelProperty(value = "初始净值价格")
    private BigDecimal priceNetInit;

    @NotNull(message = "交易币种余额阀值不能为空")
    @ApiModelProperty(value = "交易币种余额阀值")
    private BigDecimal thresholdTradeCoin;

    @NotNull(message = "计价币种余额阀值不能为空")
    @ApiModelProperty(value = "计价币种余额阀值")
    private BigDecimal thresholdValuationCoin;

    @NotNull(message = "净值合并阀值不能为空")
    @ApiModelProperty(value = "净值合并阀值")
    private BigDecimal thresholdNetMerge;

    @NotBlank(message = "产品图标不能为空")
    @ApiModelProperty(value = "产品图标")
    private String icon;

    @ApiModelProperty(value = "估值币种")
    private String coinValuation;

    @ApiModelProperty(value = "交易币种")
    private String coinTrade;

    @ApiModelProperty(value = "创建人")
    private String createUser;

    @ApiModelProperty("国际化数据_jsonString")
    private String international;

}
