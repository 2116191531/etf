package com.code.etf.admin.api.menu;

import lombok.Getter;

@Getter
public enum RedisKeysEnum {
    /**
     * 交易对:杠杠 最后一次调仓记录;
     */
    ETF_RE_BALANCE_RECORD("ETF_RE_BALANCE_RECORD:%s", " 第一个参数是交易对"),
    /**
     * 交易对:杠杠 最新净值;
     */
    ETF_NET_WORTH_NEW("ETF_NET_WORTH_NEW:%s", "第一个参数是交易对"),
    /**
     * 交易对:杠杠 净值计算因子;
     */
    ETF_DEAL_PAIR_NET_WORTH_LOAD_FACTOR("ETF_DEAL_PAIR_NET_WORTH_LOAD_FACTOR:%s", "第一个参数是交易对"),
    /**
     * 交易对:最新追踪标的价格;
     */
    ETF_DEAL_PAIR_TRACK_PRICE("ETF_DEAL_PAIR_TRACK_PRICE:%s", "第一个参数是追踪交易对"),

    /**
     * ETF产品:产品维护开关
     */
    ETF_DEAL_PAIR_SWITCH("ETF_DEAL_PAIR_SWITCH:", "etf产品维护开关 1 为开启 0 为关闭");


    private final String key;
    private final String desc;

    RedisKeysEnum(String key, String desc) {
        this.key = key;
        this.desc = desc;
    }
}
