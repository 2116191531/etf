package com.code.etf.admin.api.feign;


import com.code.commons.mojo.result.ResultData;
import com.code.etf.admin.api.dto.ProductConfigDTO;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * <p>
 * etf产品配置表 前端控制器
 * </p>
 *
 * @author 七楼
 * @since 2021-06-29
 */

@FeignClient(name = "etf-job", fallbackFactory = EtfOpenApiFeign.EtfOpenApiFeignFallbackFactory.class)
@Repository
public interface EtfOpenApiFeign {
    @PostMapping(value = "/etf-job/open-api/init-task/")
    ResultData initTask(@RequestBody ProductConfigDTO dto);

    @PostMapping(value = "/etf-job/open-api/update-task/")
    ResultData updateTask(@RequestBody ProductConfigDTO dto);

    @PostMapping(value = "/etf-job/open-api/delete-task/")
    ResultData deleateTask(@RequestBody ProductConfigDTO dto);

    @Slf4j
    @Component
    class EtfOpenApiFeignFallbackFactory implements FallbackFactory<EtfOpenApiFeign> {
        @Override
        public EtfOpenApiFeign create(Throwable throwable) {
            log.error("EtfOpenApiFeignFallbackFactory: ", throwable);
            return new EtfOpenApiFeign() {

                @Override
                public ResultData initTask(ProductConfigDTO dto) {
                    return null;
                }

                @Override
                public ResultData updateTask(ProductConfigDTO dto) {
                    return null;
                }

                @Override
                public ResultData deleateTask(ProductConfigDTO dto) {
                    return null;
                }
            };
        }
    }
}