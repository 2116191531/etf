package com.code.etf.admin.api.command;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "调仓记录分页查询", description = "调仓记录分页查询")
public class RebalanceRecordPageCommand extends PageCommand {
    @ApiModelProperty(value = "交易对")
    private String dealPair;
    @ApiModelProperty(value = "调仓类型：1 定时 2临时")
    private String type;
    @ApiModelProperty(value = "基础：杠杠倍数")
    private Integer multiple;
    @ApiModelProperty(value = "开始时间", notes = "和结束时间组合传递才能生效")
    private Long startTime;
    @ApiModelProperty(value = "结束时间")
    private Long endTime;
}