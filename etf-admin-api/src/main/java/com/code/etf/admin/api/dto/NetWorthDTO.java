package com.code.etf.admin.api.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@ApiModel(value = "NetWorth对象", description = "净值")
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class NetWorthDTO {
    private String dealPair;
    private Long time;
    private BigDecimal val;
}
