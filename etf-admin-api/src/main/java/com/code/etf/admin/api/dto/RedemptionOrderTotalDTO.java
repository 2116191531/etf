package com.code.etf.admin.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class RedemptionOrderTotalDTO {
    /**
     * 订单数
     */
    @ApiModelProperty("交易货币订单数")
    private BigDecimal orderCountTotal;

    @ApiModelProperty(value = "交易货币单位：例 btc3l")
    private String countUnit;

    /**
     * 总费用 usdt
     */
    @ApiModelProperty("总费用 usdt")
    private BigDecimal feeTotalUsdt=BigDecimal.ZERO;

    @ApiModelProperty(value = "手续费单位")
    private String feeUnit;

    /**
     * 订单总金额 usdt
     */
    @ApiModelProperty("订单总金额 usdt")
    private BigDecimal orderPriceTotal=BigDecimal.ZERO;

    @ApiModelProperty(value = "订单金额单位")
    private String priceUnit;

}
