package com.code.etf.admin.api.menu;

import lombok.Getter;

@Getter
public class KafkaTopicKeys {
    /**
     * 交易对净值
     */
    public static final String ETF_DEAL_PAIR_NET_WORTH = "ETF_DEAL_PAIR_NET_WORTH";

    /**
     * 产品配置变更:新增
     */
    public static final String ETF_DEAL_PAIR_CONFIG_ADD = "ETF_DEAL_PAIR_CONFIG_ADD";

    /**
     * 产品配置变更:修改
     */
    public static final String ETF_DEAL_PAIR_CONFIG_UPDATE = "ETF_DEAL_PAIR_CONFIG_UPDATE";

    /**
     * 产品配置变更:删除
     */
    public static final String ETF_DEAL_PAIR_CONFIG_DELETE = "ETF_DEAL_PAIR_CONFIG_DELETE";

    /**
     * etf产品维护开关
     */
    public static final String ETF_DEAL_PAIR_SWITCH = "ETF_DEAL_PAIR_SWITCH";

}
