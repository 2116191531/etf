package com.code.etf.admin.api.menu;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OrderStatusDict {
    UN_TRADED("1", "未交易"),
    SUCCESS("2", "交易成功"),
    FAILURE("3", "交易失败"),
    UNKNOWN("4", "未知");

    private final String code;
    private final String msg;
}
