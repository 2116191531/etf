package com.code.etf.admin.api.dto;

import lombok.Data;

@Data
public class EtfMultipleDTO {

    /**
     * 倍数
     */
    private Integer value;
    /**
     * 名称
     */
    private String name;
}
