package com.code.etf.admin.api.dto;


import lombok.Data;

import java.math.BigDecimal;

/**
 *
 */
@Data
public class XtGetTickersResponse {
    /**
     * 24H最高
     */
    private BigDecimal high;
    /**
     * 成交额
     */
    private BigDecimal moneyVol;
    /**
     * 24H涨跌幅
     */
    private BigDecimal rate;
    /**
     * 24H最低
     */
    private BigDecimal low;
    /**
     * 最新价
     */
    private BigDecimal price;
    /**
     *
     */
    private BigDecimal ask;
    /**
     *
     */
    private BigDecimal bid;
    /**
     * 成交量
     */
    private BigDecimal coinVol;
}
