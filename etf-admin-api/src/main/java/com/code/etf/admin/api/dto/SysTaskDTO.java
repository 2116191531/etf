package com.code.etf.admin.api.dto;

import com.code.etf.admin.api.menu.QuotationTaskTypeDict;
import lombok.Data;

@Data
public class SysTaskDTO {
    private Long id;
    private String dealPair;
    private QuotationTaskTypeDict type;
    private Long sum;
}
