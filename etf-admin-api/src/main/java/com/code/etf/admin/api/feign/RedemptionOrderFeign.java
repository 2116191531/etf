package com.code.etf.admin.api.feign;

import com.code.commons.mojo.result.ResultData;
import com.code.etf.admin.api.dto.RedemptionOrderStatisticsDTO;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Repository
@FeignClient(name = "etf-admin", fallbackFactory = RedemptionOrderFeign.RedemptionOrderStatisticsFallbackFactory.class)
public interface RedemptionOrderFeign {


    @GetMapping("/etf-admin/redemption-order/statistics/{countUnit}")
    ResultData<RedemptionOrderStatisticsDTO> getRedemptionOrderStatistics(@PathVariable("countUnit") String countUnit);

    @Slf4j
    @Component
    class RedemptionOrderStatisticsFallbackFactory implements FallbackFactory<RedemptionOrderFeign> {
        @Override
        public RedemptionOrderFeign create(Throwable throwable) {
            log.error("fallbackFactory:{}", throwable.getMessage());
            return new RedemptionOrderFeign() {

                @Override
                public ResultData<RedemptionOrderStatisticsDTO> getRedemptionOrderStatistics(String countUnit) {
                    return null;
                }
            };
        }
    }
}
