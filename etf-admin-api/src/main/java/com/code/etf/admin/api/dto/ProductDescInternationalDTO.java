package com.code.etf.admin.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 产品描述国际化多语言字典
 */
@Data
public class ProductDescInternationalDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "产品id")
    private String productId;

    @ApiModelProperty(value = "json数据")
    private String jsonData;

}
