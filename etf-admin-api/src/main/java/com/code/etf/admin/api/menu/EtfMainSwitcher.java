package com.code.etf.admin.api.menu;

import lombok.Getter;

@Getter
public enum EtfMainSwitcher {

    ON(1, "已开启(不能访问)"),
    OFF(0, "已关闭");

    private final Integer code;
    private final String desc;

    EtfMainSwitcher(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

}
