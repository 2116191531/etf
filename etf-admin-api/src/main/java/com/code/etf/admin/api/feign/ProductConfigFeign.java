package com.code.etf.admin.api.feign;


import com.code.etf.admin.api.dto.ProductConfigDTO;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * <p>
 * etf产品配置表 前端控制器
 * </p>
 *
 * @author 七楼
 * @since 2021-06-29
 */

@FeignClient(name = "etf-admin", fallbackFactory = ProductConfigFeign.ProductConfigFeignFallbackFactory.class)
@Repository
public interface ProductConfigFeign {
    @GetMapping("/etf-admin/product-config/list")
    List<ProductConfigDTO> findAll();

    @Slf4j
    @Component
    class ProductConfigFeignFallbackFactory implements FallbackFactory<ProductConfigFeign> {
        @Override
        public ProductConfigFeign create(Throwable throwable) {
            log.error("fallbackFactory: ", throwable);
            return new ProductConfigFeign() {
                @Override
                public List<ProductConfigDTO> findAll() {
                    return null;
                }
            };
        }
    }
}