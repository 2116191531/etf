package com.code.etf.admin.api.menu;

import lombok.Getter;

@Getter
public enum RebalanceTypeEnum {
    TIMING_RE_BALANCE("1", " 定时调仓类型"),
    THRESHOLD_CHECK_RE_BALANCE("2", " 不定时调仓类型");


    private final String key;
    private final String desc;

    RebalanceTypeEnum(String key, String desc) {
        this.key = key;
        this.desc = desc;
    }
}
