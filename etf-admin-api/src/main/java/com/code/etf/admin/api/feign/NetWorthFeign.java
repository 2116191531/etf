package com.code.etf.admin.api.feign;

import com.code.commons.mojo.result.ResultData;
import com.code.etf.admin.api.dto.NetWorthDTO;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Repository
@FeignClient(name = "etf-admin", fallbackFactory = NetWorthFeign.NetWorthFallbackFactory.class)
public interface NetWorthFeign {


    @PostMapping("/etf-admin/net-worth")
    ResultData create(@RequestBody @Validated NetWorthDTO dto);

    @Slf4j
    @Component
    class NetWorthFallbackFactory implements FallbackFactory<NetWorthFeign> {
        @Override
        public NetWorthFeign create(Throwable throwable) {
            log.error("fallbackFactory:{}", throwable.getMessage());
            return new NetWorthFeign() {
                @Override
                public ResultData create(NetWorthDTO dto) {
                    return null;
                }
            };
        }
    }
}
