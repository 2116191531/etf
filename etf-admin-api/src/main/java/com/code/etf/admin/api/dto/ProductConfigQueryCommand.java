package com.code.etf.admin.api.dto;

import com.code.etf.admin.api.command.PageCommand;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * etf产品配置保存请求
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "产品配置请求", description = "etf产品配置请求封装")
public class ProductConfigQueryCommand extends PageCommand implements Serializable {

    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "产品id")
    private String id;

    @ApiModelProperty(value = "交易对")
    private String dealPair;

    @ApiModelProperty(value = "倍数")
    private Integer multiple;

    @ApiModelProperty(value = "发布状态 1 开启中 0 已隐藏")
    private Boolean isRelease;

    @ApiModelProperty(value = "创建时间")
    private Long createTimeStart;

    @ApiModelProperty(value = "创建时间")
    private Long createTimeEnd;
}
