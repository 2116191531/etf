package com.code.etf.admin.api.command;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
public class PageCommand {
    @ApiModelProperty(value = "当前页码", required = true)
    @Min(value = 1, message = "当前页码限制最小值1")
    private Integer current;
    @ApiModelProperty(value = "条数", required = true)
    @Min(value = 1, message = "条数限制最小值1")
    @Max(value = 500, message = "条数限制最大值500")
    private Integer size;
}
