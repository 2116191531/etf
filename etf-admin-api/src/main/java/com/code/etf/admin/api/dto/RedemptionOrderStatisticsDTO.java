package com.code.etf.admin.api.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.boot.jackson.JsonComponent;

import java.math.BigDecimal;

@Data
public class RedemptionOrderStatisticsDTO {

    /**
     * 币种id
     */
    @ApiModelProperty("币种id")
    private Long id;

    /**
     * 市场id
     */
    @ApiModelProperty("市场id")
    private Long marketId;

    /**
     * 币种
     */
    @ApiModelProperty("币种")
    private String countUnit;

    /**
     * 总发行量
     */
    @ApiModelProperty("总发行量")
    private BigDecimal totalIssueAmount = BigDecimal.ZERO;
    /**
     * 已申购总额
     */
    @ApiModelProperty("已申购总额")
    private BigDecimal userBuyTotalAmount = BigDecimal.ZERO;
    /**
     * 账户份额余额
     */
    @ApiModelProperty("账户份额余额")
    private BigDecimal totalBalance = BigDecimal.ZERO;
    /**
     * 做市总持仓
     */
    @ApiModelProperty("做市总持仓")
    private BigDecimal marketMakeTotalHoldAmount = BigDecimal.ZERO;
    /**
     * 用户总持仓
     */
    @ApiModelProperty("用户总持仓")
    private BigDecimal userTotalHoldAmount = BigDecimal.ZERO;
    /**
     * 手续费总持仓
     */
    @ApiModelProperty("手续费总持仓")
    private BigDecimal feeTotalAmount = BigDecimal.ZERO;


    /**
     * 已申购总额百分比
     */
    @ApiModelProperty("已申购总额")
    private BigDecimal userBuyTotalAmountPercent = BigDecimal.ZERO;
    /**
     * 账户份额余额百分比
     */
    @ApiModelProperty("账户份额余额")
    private BigDecimal totalBalancePercent = BigDecimal.ZERO;
    /**
     * 做市总持仓百分比
     */
    @ApiModelProperty("做市总持仓")
    private BigDecimal marketMakeTotalHoldAmountPercent = BigDecimal.ZERO;
    /**
     * 用户总持仓百分比
     */
    @ApiModelProperty("用户总持仓")
    private BigDecimal userTotalHoldAmountPercent = BigDecimal.ZERO;
    /**
     * 手续费总持仓百分比
     */
    @ApiModelProperty("手续费总持仓")
    private BigDecimal feeTotalAmountPercent = BigDecimal.ZERO;

}
