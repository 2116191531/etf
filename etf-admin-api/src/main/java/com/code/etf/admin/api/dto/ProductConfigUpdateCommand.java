package com.code.etf.admin.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * etf产品配置修改请求
 */
@Data
@ApiModel(value = "产品配置修改请求", description = "etf产品配置修改请求封装")
public class ProductConfigUpdateCommand implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "id不能为空")
    private Long id;

    @DecimalMin(value = "0.00000000000000000001", message = "最小申购不能为空,且不能低于0.00000000000000000001")
    @ApiModelProperty(value = "最小申购")
    private BigDecimal buyMinPrice;

    @DecimalMin(value = "0.00000000000000000001", message = "最小赎回不能为空,且不能低于0.00000000000000000001")
    @ApiModelProperty(value = "最小赎回")
    private BigDecimal redeemMinPrice;

    @DecimalMin(value = "0.00000000000000000001", message = "产品维度每日购买限额不能为空,且不能低于0.00000000000000000001")
    @ApiModelProperty(value = "产品维度每日购买限额")
    private BigDecimal dayBuyLimitGlobal;

    @DecimalMin(value = "0.00000000000000000001", message = "产品维度每日赎回限额不能为空,且不能低于0.00000000000000000001")
    @ApiModelProperty(value = "产品维度每日赎回限额")
    private BigDecimal dayRedeemLimitGlobal;

    @DecimalMin(value = "0.00000000000000000001", message = "个人每日购买限额不能为空,且不能低于0.00000000000000000001")
    @ApiModelProperty(value = "个人每日购买限额")
    private BigDecimal dayBuyLimitPersonal;

    @DecimalMin(value = "0.00000000000000000001", message = "个人每日赎回限额不能为空,且不能低于0.00000000000000000001")
    @ApiModelProperty(value = "个人每日赎回限额")
    private BigDecimal dayRedeemLimitPersonal;

    @DecimalMin(value = "0.00000000000000000001", message = "再平衡阈值不能为空,且不能低于0.00000000000000000001")
    @ApiModelProperty(value = "	再平衡阈值（反方向涨跌幅）")
    private BigDecimal thresholdUpDown;

    @Min(message="定时平衡时间必须为 0-23",value=0)
    @Max(message="定时平衡时间必须为 0-23",value=23)
    @ApiModelProperty(value = "定时平衡时间(0-23（默认0点）)")
    private Integer timeEquilibrium;

    @DecimalMin(value = "0.00000000000000000001", message = "管理费(展示)不能为空,且不能低于0.00000000000000000001")
    @DecimalMax(value = "0.999999999999999999",message ="管理费(展示)不能为空,且不能大于等于1")
    @ApiModelProperty(value = "管理费率(展示)")
    private BigDecimal feeMangeExpect;

    @DecimalMin(value = "0.00000000000000000001", message = "管理费率(实际)不能为空,且不能低于0.00000000000000000001")
    @DecimalMax(value = "0.999999999999999999",message ="管理费率(实际)不能为空,且不能大于等于1")
    @ApiModelProperty(value = "管理费(实际)")
    private BigDecimal feeMangeActual;

    @DecimalMin(value = "0.00000000000000000001", message = "申购费率不能为空,且不能低于0.00000000000000000001")
    @DecimalMax(value = "0.999999999999999999",message ="申购费率不能为空,且不能大于等于1")
    @ApiModelProperty(value = "申购费率")
    private BigDecimal feeRateBuy;

    @DecimalMin(value = "0.00000000000000000001", message = "赎回费率不能为空,且不能低于0.00000000000000000001")
    @DecimalMax(value = "0.999999999999999999",message ="赎回费率不能为空,且不能大于等于1")
    @ApiModelProperty(value = "赎回费率")
    private BigDecimal feeRateRedeem;

    @ApiModelProperty(value = "做市账号集,逗号分割")
    private List<Long> marketMakerIds;

    @ApiModelProperty(value = "交易币种余额阀值")
    private BigDecimal thresholdTradeCoin;

    @ApiModelProperty(value = "计价币种余额阀值")
    private BigDecimal thresholdValuationCoin;

    @ApiModelProperty(value = "净值合并阀值")
    private BigDecimal thresholdNetMerge;

    @NotBlank(message = "产品图标不能为空")
    @ApiModelProperty(value = "产品图标")
    private String icon;

    @ApiModelProperty(value = "发布状态 1 开启中 0 已隐藏")
    private Boolean isRelease;

    @ApiModelProperty(value = "估值币种")
    private String coinValuation;

    @ApiModelProperty(value = "交易币种")
    private String coinTrade;

    @ApiModelProperty(value = "申赎开关 1 申购 0 赎回")
    private Boolean switchBuyRedemption;

    @ApiModelProperty(value = "操作员")
    private String operator;

    @ApiModelProperty("国际化数据_jsonString")
    private String international;

}
