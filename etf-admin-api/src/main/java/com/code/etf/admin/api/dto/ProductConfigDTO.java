package com.code.etf.admin.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * etf产品配置表
 * </p>
 *
 * @author 七楼
 * @since 2021-06-29
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "ProductConfig对象", description = "etf产品配置表")
public class ProductConfigDTO {

    private Long id;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "交易对")
    private String dealPair;

    @ApiModelProperty(value = "倍数")
    private Integer multiple;

    @ApiModelProperty(value = "倍数名称")
    private String multipleName;

    @ApiModelProperty(value = "做市用户ids 逗号分隔")
    private List<Long> marketMakerIds;

    @ApiModelProperty(value = "交易币种数量")
    private BigDecimal coinAmountTrade;

    @ApiModelProperty(value = "计价币种数量")
    private BigDecimal coinAmountValuation;

    @ApiModelProperty(value = "最小交易金额")
    private BigDecimal minAmountTrade;

    @ApiModelProperty(value = "最大交易金额")
    private BigDecimal maxAmountTrade;

    @ApiModelProperty(value = "交易费率")
    private BigDecimal feeRateTrade;

    @ApiModelProperty(value = "管理费(展示)")
    private BigDecimal feeMangeExpect;

    @ApiModelProperty(value = "管理费(实际)")
    private BigDecimal feeMangeActual;

    @ApiModelProperty(value = "申购费率")
    private BigDecimal feeRateBuy;

    @ApiModelProperty(value = "赎回费率")
    private BigDecimal feeRateRedeem;

    @ApiModelProperty(value = "最小申购")
    private BigDecimal buyMinPrice;

    @ApiModelProperty(value = "最小赎回")
    private BigDecimal redeemMinPrice;

    @ApiModelProperty(value = "产品维度每日购买限额")
    private BigDecimal dayBuyLimitGlobal;

    @ApiModelProperty(value = "产品维度每日赎回限额")
    private BigDecimal dayRedeemLimitGlobal;

    @ApiModelProperty(value = "个人每日购买限额")
    private BigDecimal dayBuyLimitPersonal;

    @ApiModelProperty(value = "个人每日赎回限额")
    private BigDecimal dayRedeemLimitPersonal;

    @ApiModelProperty(value = "	再平衡阈值		（反方向涨跌幅）")
    private BigDecimal thresholdUpDown;

    @ApiModelProperty(value = "定时平衡时间(0-23（默认0点）)")
    private Integer timeEquilibrium;

    @ApiModelProperty(value = "标的交易对")
    private String dealPairTrack;

    @ApiModelProperty(value = "发行时间")
    private Long issueTime;

    @ApiModelProperty(value = "初始净值价格")
    private BigDecimal priceNetInit;

    @ApiModelProperty(value = "交易币种余额阀值")
    private BigDecimal thresholdTradeCoin;

    @ApiModelProperty(value = "计价币种余额阀值")
    private BigDecimal thresholdValuationCoin;

    @ApiModelProperty(value = "净值合并阀值")
    private BigDecimal thresholdNetMerge;

    @ApiModelProperty(value = "产品图标")
    private String icon;

    @ApiModelProperty(value = "发布状态 1 开启中 0 已隐藏")
    private Boolean isRelease;

    @ApiModelProperty(value = "估值币种")
    private String coinValuation;

    @ApiModelProperty(value = "交易币种")
    private String coinTrade;

    @ApiModelProperty(value = "申赎开关 1 开启中 0 已关闭")
    private Boolean switchBuyRedemption;

    @ApiModelProperty(value = "创建人")
    private String createUser;

    @ApiModelProperty(value = "操作员")
    private String operator;

    @ApiModelProperty("国际化数据_jsonString")
    private String international;

    @ApiModelProperty(value = "创建时间")
    private Long createTime;

    @ApiModelProperty(value = "修改时间")
    private Long updateTime;

}
