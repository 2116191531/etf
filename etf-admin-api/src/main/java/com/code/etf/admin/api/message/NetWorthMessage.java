package com.code.etf.admin.api.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Data
public class NetWorthMessage {
    private String dealPair;
    private Long time;
    private BigDecimal val;
}
