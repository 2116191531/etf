package com.code.etf.admin.api.command;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ProductSortCommand {

    @NotNull(message = "productId不能为空")
    private Long productId;
    @NotNull(message = "排序值不能为空")
    private Integer sort;
}
