package com.code.etf.admin.api.menu;

import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

@Getter
public enum QuotationTaskTypeDict {
    DEAL_PAIR_PRICE("0", "最新成交价"),
    DEAL_PAIR_DEPTH("1", "盘口");
    private final String type;
    private final String desc;


    QuotationTaskTypeDict(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public static QuotationTaskTypeDict mustGetByDict(String type) {
        Optional<QuotationTaskTypeDict> target = Arrays.stream(values()).filter((x) -> x.type.equals(type)).findFirst();

        assert target.isPresent();

        return target.get();
    }
}
