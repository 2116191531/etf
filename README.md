### 0、项目包

```
    etf-amdin etf产品管理及相关报表统计
    etf-job 核心包，负责产品etf所有定时任务
```

### 1、部署环境

##### 1.1 运行环境 

- JDK1.8

##### 1.2 数据库

- MySQL 5.7

##### 1.3 中间件

- Nacos 最新版

- Kafka 2.12-2.6

- Redis 4.0

### 2、初始化数据

> database需要自己建立

##### 2.1 etf-admin项目初始化sql

路径：depoly/etf-admin.sql

##### 2.2 etf-job项目初始化sql

路径：depoly/quarz.sql

### 3、配置文件

> 一下只是针对环境变量说明，需要提供如下环境变量才可正常运行。注意相关地址请对应环境，dev、test、prod等

##### 3.1 etf_admin服务环境变量:

```shell
#Nacos地址
ETF_NACOS_ADDRESS=*.*.*.*:8848
 #etf_admin MySQL地址
ETF_MYSQL_URL=jdbc:mysql://*.*.*.*:3306/etf?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai&allowPublicKeyRetrieval=true&verifyServerCertificate=false&useSSL=false
#etf_admin MySQL用户名
ETF_MYSQL_USERNAME=****
#etf_admin MySQL密码，需要加密，连续陈老师
ETF_MYSQL_PASSWORD=******
#xt：service-redemption-impl服务 MySQL地址
XT_MYSQL_URL=jdbc:mysql://*.*.*.*:3311/xt_redemption?useUnicode=true&characterEncoding=utf8&useSSL=false&autoReconnect=true&useCursorFetch=true&serverTimezone=GMT%2B8 
#xt：service-redemption-impl服务 MySQL用户名
XT_MYSQL_USERNAME=****
#xt：service-redemption-impl服务 MySQL密码
XT_MYSQL_PASSWORD=******
#Kafka 集群地址
ETF_KAFKA_SERVERS=*.*.*.*:9092,*.*.*.*:9093,*.*.*.*:9094
#Redis地址
ETF_REDIS_HOST=*.*.*.*
#Redis database。需要和etf-job保持一直
ETF_REDIS_DATABASE=8
# Redis 端口
ETF_REDIS_PORT=6379
# Redis 密码，需要加密，连续陈老师
ETF_REDIS_PASSWORD=******
# etf-admin 系统Id，用户中心分配，dev、test、prod可能会分配不同
ETF_SYSTEM_ID=3
# 成都：user-center服务地址，请对应环境
USER_CENTER_SERVER_HOST=*.*.*.*.*
# 成都：用户中心端口
USER_CENTER_SERVER_PORT=777
# 成都：用户中心服务名，也是前缀uri
USER_CENTER_SERVER_NAME=user-center
# 成都：行情服务地址
QUOTATION_BASE_URL=http://*.*.*.*:8080/service-quotation-process
# xt：open-api地址
BASE_XT_URL:http=//api3.fengzzz.com
# 成都：统一用户中心配置的字典（user-center）
DICT_MUL_CODE=etf-mul
```

##### 3.2 etf_job服务:

```shell
# Nacos地址
ETF_NACOS_ADDRESS=*.*.*.*:8848
# etf-job MySQL地址
ETF_MYSQL_URL:jdbc:mysql=//*.*.*.*:3306/etf?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai&allowPublicKeyRetrieval=true&verifyServerCertificate=false&useSSL=false
# etf-job MySQL用户名
ETF_MYSQL_USERNAME=****
# etf-job MySQL 密码
ETF_MYSQL_PASSWORD=******
# Kafka 集群地址
ETF_KAFKA_SERVERS=*.*.*.*:9092,*.*.*.*:9093,*.*.*.*:9094
# Redis 地址
ETF_REDIS_HOST=*.*.*.*
# Redis database 请和etf-admin保持一直
ETF_REDIS_DATABASE=5
# Redis 端口
ETF_REDIS_PORT=6379
# Redis 密码，需要加密，请联系陈老师
ETF_REDIS_PASSWORD=****
# etf-job 定时任务MySQL表
ETF_JOB_MYSQL_URL=jdbc:mysql://*.*.*.*:3306/quotation?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai&allowPublicKeyRetrieval=true&verifyServerCertificate=false&useSSL=false
# etf-job 定时任务MySQL 的账户
ETF_JOB_MYSQL_USERNAME=****
# etf-job 定时任务MySQL 密码
ETF_JOB_MYSQL_PASSWORD=******
# 成都：行情服务地址
QUOTATION_BASE_URL=http://*.*.*.*:8080/service-quotation-process
```





