package com.code.etf.admin.server.controller

import com.alibaba.fastjson.JSON
import com.code.commons.mojo.result.ResultData
import com.code.etf.admin.api.dto.ProductConfigDTO
import com.code.etf.admin.api.dto.RebalanceRecordDTO
import com.code.etf.admin.api.dto.RedemptionOrderStatisticsDTO
import com.code.etf.admin.api.feign.ProductConfigFeign
import com.code.etf.admin.api.feign.RebalanceRecordFeign
import com.code.etf.admin.api.feign.RedemptionOrderFeign
import com.code.etf.job.server.EtfJobApplication
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootContextLoader
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import spock.lang.Specification

@WebAppConfiguration
@ContextConfiguration(classes = [EtfJobApplication.class], loader = SpringBootContextLoader.class)
@ActiveProfiles("dev")
class EtfJobTest extends Specification {

    @Autowired
    RedemptionOrderFeign redemptionOrderFeign

    def "testFeign"() {
        given:
        RedemptionOrderStatisticsDTO redemptionOrderStatisticsDTO = redemptionOrderFeign.getRedemptionOrderStatistics("btc3l")
        expect:
        println "result:=>" + JSON.toJSONString(redemptionOrderStatisticsDTO.toString())
    }


    @Autowired
    ProductConfigFeign productConfigFeign

    def "testProductConfigFeign"() {
        given:
        List<ProductConfigDTO> productConfigDTOList = productConfigFeign.findAll()
        expect:
        println "result:=>" + JSON.toJSONString(productConfigDTOList.toString())
    }

    @Autowired
    RebalanceRecordFeign rebalanceRecordFeign;
    def "feignTest" (){
        given:
        ResultData<RebalanceRecordDTO> rebalanceRecordByNow= rebalanceRecordFeign.findOneRebalanceRecordByNow()
        expect:
        println "result:"+JSON.toJSONString(rebalanceRecordByNow)
    }
}
