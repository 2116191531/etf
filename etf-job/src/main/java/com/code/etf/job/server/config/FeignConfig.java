package com.code.etf.job.server.config;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author xt
 */
@EnableFeignClients(basePackages = "com.code")
@Configuration
@ComponentScan("com.code")
public class FeignConfig {
}
