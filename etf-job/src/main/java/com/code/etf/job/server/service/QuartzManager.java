package com.code.etf.job.server.service;

import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * quartz工具类
 */
@Component
@Slf4j
public class QuartzManager {

    private final static String JOB_GROUP_NAME = "DEFAULT_JOB_GROUP_NAME";
    private final static String TRIGGER_GROUP_NAME = "DEFAULT_TRIGGER_GROUP_NAME";

    @Autowired
    private Scheduler scheduler;

    //addSimpleJob简略版无额外参数
    public boolean addSimpleJob(String jobName, Integer interval, TimeUnit timeUnit, Class<? extends Job> jobClass) {
        return addSimpleJob(jobName, JOB_GROUP_NAME, jobName, TRIGGER_GROUP_NAME, interval, timeUnit, null, jobClass);
    }

    //addSimpleJob简略版有额外参数
    public boolean addSimpleJob(String jobName, Integer interval, TimeUnit timeUnit, Map<String, Object> extraParam, Class<? extends Job> jobClass) {
        return addSimpleJob(jobName, JOB_GROUP_NAME, jobName, TRIGGER_GROUP_NAME, interval, timeUnit, extraParam, jobClass);
    }

    //addCronJob简略版无额外参数
    public boolean addCronJob(String jobName, String cronExpression, Class<? extends Job> jobClass) {
        return addCronJob(jobName, JOB_GROUP_NAME, jobName, TRIGGER_GROUP_NAME, cronExpression, null, jobClass);
    }

    //addCronJob简略版有额外参数
    public boolean addCronJob(String jobName, String cronExpression, Map<String, Object> extraParam, Class<? extends Job> jobClass) {
        return addCronJob(jobName, JOB_GROUP_NAME, jobName, TRIGGER_GROUP_NAME, cronExpression, extraParam, jobClass);
    }

    //modifySimpleJobTime简略版
    public boolean modifySimpleJobTime(String triggerName, Integer interval, TimeUnit timeUnit) {
        return modifySimpleJobTime(triggerName, TRIGGER_GROUP_NAME, interval, timeUnit);
    }

    //modifyCronJobTime简略版
    public boolean modifyCronJobTime(String triggerName, String cronExpression) {
        return modifyCronJobTime(triggerName, TRIGGER_GROUP_NAME, cronExpression);
    }

    //removeJob简略版
    public boolean removeJob(String jobName) {
        return removeJob(jobName, JOB_GROUP_NAME, jobName, TRIGGER_GROUP_NAME);
    }

    /**
     * 添加简单定时任务
     *
     * @param jobName      任务名称
     * @param jobGroup     任务组名称
     * @param triggerName  触发器名称
     * @param triggerGroup 触发器组名称
     * @param interval     间隔时间
     * @param timeUnit     间隔时间单位
     * @param extraParam   携带参数
     * @param JobClass     任务类Class
     * @return boolean
     */
    public boolean addSimpleJob(String jobName, String jobGroup, String triggerName, String triggerGroup, Integer interval, TimeUnit timeUnit, Map<String, Object> extraParam, Class<? extends Job> JobClass) {

        try {
            JobDetail jobDetail = JobBuilder
                    .newJob(JobClass)
                    .withIdentity(jobName, jobGroup)
                    .build();
            if (extraParam != null) {
                jobDetail.getJobDataMap().putAll(extraParam);
            }
            SimpleTrigger simpleTrigger = TriggerBuilder
                    .newTrigger()
                    .withIdentity(triggerName, triggerGroup)
                    .withSchedule(getSimpleScheduleBuilder(interval, timeUnit))
                    .startNow()
                    .build();
            scheduler.scheduleJob(jobDetail, simpleTrigger);
        } catch (SchedulerException e) {
            log.error("添加简单定时任务失败", e);
            return false;
        }
        return true;
    }


    /**
     * 添加cron定时任务
     *
     * @param jobName        任务名称
     * @param jobGroup       任务组名称
     * @param triggerName    触发器名称
     * @param triggerGroup   触发器组名称
     * @param cronExpression cron 表达式
     * @param extraParam     携带参数
     * @param JobClass       任务类Class
     * @return boolean
     */
    public boolean addCronJob(String jobName, String jobGroup, String triggerName, String triggerGroup, String cronExpression, Map<String, Object> extraParam, Class<? extends Job> JobClass) {

        try {
            JobDetail jobDetail = JobBuilder
                    .newJob(JobClass)
                    .withIdentity(jobName, jobGroup)
                    .build();
            if (extraParam != null) {
                jobDetail.getJobDataMap().putAll(extraParam);
            }
            CronTrigger cronTrigger = TriggerBuilder
                    .newTrigger()
                    .withIdentity(triggerName, triggerGroup)
                    .withSchedule(CronScheduleBuilder.cronSchedule(cronExpression))
                    .build();
            scheduler.scheduleJob(jobDetail, cronTrigger);

            scheduler.start();
            if (!scheduler.isShutdown()) {
                scheduler.start();
            }
        } catch (SchedulerException e) {
            log.error("添加cron定时任务失败", e);
            return false;
        }
        return true;
    }

    /**
     * 修改simple任务的时间的触发时间
     *
     * @param triggerName  触发器名称
     * @param triggerGroup 触发器组名称
     * @param interval     间隔时间
     * @param timeUnit     间隔时间单位
     * @return boobean
     */
    public boolean modifySimpleJobTime(String triggerName, String triggerGroup, Integer interval, TimeUnit timeUnit) {
        try {
            TriggerKey triggerKey = TriggerKey.triggerKey(triggerName, triggerGroup);
            SimpleTrigger oldTrigger = (SimpleTrigger) scheduler.getTrigger(triggerKey);
            if (oldTrigger == null) {
                log.error("未找到相关任务");
                return false;
            }
            //unit:milliseconds
            Long oldInterval = oldTrigger.getRepeatInterval();
            if (!oldInterval.equals(getMilliseconds(interval, timeUnit))) {
                SimpleTrigger simpleTrigger = TriggerBuilder
                        .newTrigger()
                        .withIdentity(triggerName, triggerGroup)
                        .withSchedule(getSimpleScheduleBuilder(interval, timeUnit))
                        .startNow()
                        .build();
                scheduler.rescheduleJob(triggerKey, simpleTrigger);
            }
        } catch (SchedulerException e) {
            log.error("修改简单定时任务间隔时间失败", e);
            return false;
        }
        return true;
    }

    /**
     * 修改cron任务的时间的触发时间
     *
     * @param triggerName    触发器名称
     * @param triggerGroup   触发器组名称
     * @param cronExpression corn表达式
     */
    public boolean modifyCronJobTime(String triggerName, String triggerGroup, String cronExpression) {
        try {
            TriggerKey triggerKey = TriggerKey.triggerKey(triggerName, triggerGroup);
            CronTrigger oldTrigger = (CronTrigger) scheduler.getTrigger(triggerKey);
            if (oldTrigger == null) {
                log.error("未找到相关任务");
                return false;
            }
            String oldCronExpression = oldTrigger.getCronExpression();
            if (!oldCronExpression.equalsIgnoreCase(cronExpression)) {
                CronTrigger cronTrigger = TriggerBuilder
                        .newTrigger()
                        .withIdentity(triggerName, triggerGroup)
                        .withSchedule(CronScheduleBuilder.cronSchedule(cronExpression))
                        .build();
                scheduler.rescheduleJob(triggerKey, cronTrigger);
            }
        } catch (SchedulerException e) {
            log.error("修改cron定时任务时间失败", e);
            return false;
        }
        return true;
    }

    /**
     * 删除指定定时任务
     *
     * @param jobName      任务名称
     * @param jobGroup     任务组名称
     * @param triggerName  触发器名称
     * @param triggerGroup 触发器组名称
     * @return boolean
     */
    public boolean removeJob(String jobName, String jobGroup, String triggerName, String triggerGroup) {
        try {
            TriggerKey triggerKey = TriggerKey.triggerKey(triggerName, triggerGroup);

            scheduler.pauseTrigger(triggerKey);

            scheduler.unscheduleJob(triggerKey);

            scheduler.deleteJob(JobKey.jobKey(jobName, jobGroup));
        } catch (SchedulerException e) {
            log.error("删除定时任务失败", e);
            return false;
        }
        return true;
    }


    private SimpleScheduleBuilder getSimpleScheduleBuilder(Integer interval, TimeUnit timeUnit) {

        switch (timeUnit) {
            case SECONDS:
                return SimpleScheduleBuilder.repeatSecondlyForever(interval);
            case MINUTES:
                return SimpleScheduleBuilder.repeatMinutelyForever(interval);
            case HOURS:
                return SimpleScheduleBuilder.repeatHourlyForever(interval);
            default:
                log.error("设置的时间间隔超出范围");
                return null;
        }
    }

    private Long getMilliseconds(Integer interval, TimeUnit timeUnit) {

        switch (timeUnit) {
            case SECONDS:
                return (long) (1000 * interval);
            case MINUTES:
                return (long) (60 * 1000 * interval);
            case HOURS:
                return (long) (60 * 60 * 1000 * interval);
            default:
                log.error("间隔时间转换错误");
                return null;
        }
    }

    public void shutdownJobs() {
        try {
            if (scheduler.isStarted()) {
                scheduler.shutdown(true);
            }
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    public void startJobs() {
        try {
            if (!scheduler.isStarted()) {
                scheduler.start();
            }
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }
}
