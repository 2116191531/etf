package com.code.etf.job.server.job;

import com.code.api.RespDict;
import com.code.commons.mojo.result.ResultData;
import com.code.commons.mojo.result.ResultStatus;
import com.code.etf.admin.api.dto.ProductConfigDTO;
import com.code.etf.admin.api.dto.RebalanceRecordDTO;
import com.code.etf.admin.api.menu.RebalanceTypeEnum;
import com.code.exception.ExceptionBuilder;
import com.code.message.ExternalMessage;
import com.code.message.InternalMessage;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.PersistJobDataAfterExecution;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 净值计算
 * 每天再平衡前10分钟变更净值【净值 = 上个周期的基准净值 * （1 - 管理费率*M）】
 * 直到下个再平衡点计算完净值后恢复正常公式
 *
 * @author qilou
 * @date 2021-6-29
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
@Slf4j
public class NetWorthAndRebalanceJob extends AbstractEtfJob {

    @Override
    public void postExecute(JobExecutionContext context, ProductConfigDTO dto) {
        log.info("==================开始执行任务: 标的价格是否超出==================");
        log.info("执行任务线程ID{}", Thread.currentThread().getId());
        final String pair = dto.getDealPair();
        // 追踪交易对价格

        BigDecimal dealPairTrackPrice = quotationManager.getDealPairPrice(dto.getDealPairTrack());
        log.info("执行{}交易对标的价格是否超出，追踪标的价格:{}", pair, dealPairTrackPrice);
        if (dealPairTrackPrice == null || dealPairTrackPrice.compareTo(BigDecimal.ZERO) <= 0) {
            log.error("执行{}交易对标的价格是否超出，标的交易对数据不合法，请关闭当前交易对，{}最新标的价格:{}", pair, dto.getDealPairTrack(), dealPairTrackPrice);
            ExceptionBuilder.service(InternalMessage.builder().template("交易对标的价格是否超出，标的交易对数据不合法，请关闭当前交易对!").build()
                    , ExternalMessage.builder().template(RespDict.SERVICE_ERROR.getMsg()).code(RespDict.SERVICE_ERROR.getCode().toString()).build());
        }

        //获取最新调仓记录
        ResultData<RebalanceRecordDTO> reBalanceRecordResult = rebalanceRecordFeign.findOneRebalanceRecordByNow(pair);
        log.info("执行{}交易对标的价格是否超出，最新调仓记录:{}", pair, reBalanceRecordResult);
        if (reBalanceRecordResult == null || !ResultStatus.SUCCESS.getCode().equals(reBalanceRecordResult.getCode()) || reBalanceRecordResult.getData() == null) {
            log.error("执行{}交易对标的价格是否超出，最新调仓记录失败:{}", pair, reBalanceRecordResult);
            ExceptionBuilder.service(InternalMessage.builder().template("交易对调仓阈值验证时，最新调仓记录失败!").build()
                    , ExternalMessage.builder().template(RespDict.SERVICE_ERROR.getMsg()).code(RespDict.SERVICE_ERROR.getCode().toString()).build());
        }
        RebalanceRecordDTO reBalanceRecord = reBalanceRecordResult.getData();

        //计算涨跌幅
        final BigDecimal thresholdUpDown = dto.getThresholdUpDown();
        log.info("执行{}交易对标的价格是否超出，涨跌幅阈值：{}", pair, thresholdUpDown);
        final BigDecimal difference = dealPairTrackPrice.subtract(reBalanceRecord.getDealPairTrackPrice());
        if (difference.compareTo(BigDecimal.ZERO) == 0) {
            log.info("执行{}交易对标的价格是否超出，最新标的和上一次调仓标的价格无变化----------1、公式： 最新标的价格 - 上一次调仓标的价格 , 2、结果：{} = {} - {}", pair, difference, dealPairTrackPrice, reBalanceRecord.getDealPairTrackPrice());
            return;
        }
        BigDecimal currentUpDown = difference.divide(reBalanceRecord.getDealPairTrackPrice(), 3, RoundingMode.DOWN).abs();
        log.info("执行{}交易对标的价格是否超出，计算涨跌幅值----------1、公式：(最新标的价格 - 上一次调仓标的价格) / 上一次调仓标的价格, 2、结果：{} = ({} - {}) / {}", pair, currentUpDown, dealPairTrackPrice, reBalanceRecord.getDealPairTrackPrice(), reBalanceRecord.getDealPairTrackPrice());


        boolean isTriggerReBalance = false;
        // 处理标的价格，避免网络波动价格变化大
        if (currentUpDown.compareTo(thresholdUpDown) >= 0) {
            log.info("执行{}交易对, 阈值{}已经超过配置再平衡阈值{}", pair, currentUpDown, thresholdUpDown);

            BigDecimal thresholdUpDownMax = BigDecimal.valueOf(0.99).divide(BigDecimal.valueOf(dto.getMultiple()).abs(), 3, RoundingMode.DOWN);

            isTriggerReBalance = true;
            if (currentUpDown.compareTo(thresholdUpDownMax) >= 0) {
                // 如果超出最大值，则进行最大涨跌幅平衡多次
                log.info("执行{}交易对标的价格是否超出, 触发了最大保护。此交易对最大阈值:{},本次阈值:{},再平后涨幅值:{}", pair, thresholdUpDownMax, currentUpDown, thresholdUpDownMax);
                currentUpDown = thresholdUpDownMax;
            }
            BigDecimal change = reBalanceRecord.getDealPairTrackPrice().multiply(currentUpDown);
            // 涨
            if (difference.compareTo(BigDecimal.ZERO) > 0) {
                dealPairTrackPrice = reBalanceRecord.getDealPairTrackPrice().add(change);
            }
            // 跌
            if (difference.compareTo(BigDecimal.ZERO) < 0) {
                dealPairTrackPrice = reBalanceRecord.getDealPairTrackPrice().subtract(change);
            }
        }
        // TODO 总任务负责执行所有交易对

        // 计算净值
        netWorthService.execute(dto, reBalanceRecord, dealPairTrackPrice);

        // 指定不定时在平衡
        if (isTriggerReBalance) {
            rebalanceService.execute(RebalanceTypeEnum.THRESHOLD_CHECK_RE_BALANCE, dto, reBalanceRecord, dealPairTrackPrice);
        }
    }
}
