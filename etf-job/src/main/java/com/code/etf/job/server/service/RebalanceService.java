package com.code.etf.job.server.service;

import com.code.etf.admin.api.dto.ProductConfigDTO;
import com.code.etf.admin.api.dto.RebalanceRecordDTO;
import com.code.etf.admin.api.menu.RebalanceTypeEnum;

import java.math.BigDecimal;

/**
 * 篮子计算公式：
 * 当期篮子（交易货币） = 当期基础净值价格 * 杠杆倍数 / 当期基础标的价格
 * 当期篮子（计价货币） = 当期基础净值价格 - 当期篮子（交易货币） * 当期基础标的价格
 * 当期期末杠杠倍数(调仓前杠杠) = 当期篮子(计划货币) * 最新标的价格 / 最新净值
 * 调仓后篮子(交易货币) = 基础杠杠倍数 * 最新净值 / 最新标的价格
 * 调仓后篮子(交易货币) = 最新净值 - (调仓后篮子(交易货币) * 最新标的价格)
 */
public interface RebalanceService {
    void execute(RebalanceTypeEnum type, ProductConfigDTO dto, RebalanceRecordDTO reBalanceRecord, BigDecimal dealPairTrackPrice);
}
