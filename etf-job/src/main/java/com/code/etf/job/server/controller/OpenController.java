package com.code.etf.job.server.controller;

import com.code.etf.admin.api.dto.ProductConfigDTO;
import com.code.etf.admin.api.message.NetWorthMessage;
import com.code.etf.job.server.job.SchedulerListener;
import com.code.etf.job.server.job.TimingReBalanceJob;
import com.code.etf.job.server.service.NetWorthService;
import com.code.springboot.redis.server.RedisManager;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags = "etf-job公开接口")
@Slf4j
@RestController
@RequestMapping("/open-api")
public class OpenController {
    @Autowired
    private RedisManager redisManager;
    @Autowired
    private SchedulerListener schedulerListener;
    @Autowired
    private NetWorthService netWorthService;

    @GetMapping(value = "/product/net-wort")
    public NetWorthMessage productByDealPair(@RequestParam String symbol) {
        return netWorthService.productByDealPair(symbol);
    }


    @PostMapping(value = "/init-task/")
    public void initTask(@RequestBody ProductConfigDTO dto) {
        schedulerListener.cleanJob(dto);
        schedulerListener.addJob(dto);
    }

    @PostMapping(value = "/update-task/")
    public void updateTask(@RequestBody ProductConfigDTO dto) {
        schedulerListener.cleanJob(dto);
        schedulerListener.addJob(dto);
    }

    @PostMapping(value = "/delete-task/")
    public void deleateTask(@RequestBody ProductConfigDTO dto) {
        schedulerListener.cleanJob(dto);
    }

    @PostMapping("/execute/{dealPair}")
    public void executeTimingReBalanceJob(@PathVariable("dealPair") String dealPair,@RequestBody ProductConfigDTO dto) {
        //TODO 临时的紧急情况的调仓
        // 执行
        if (dto.getIsRelease()) {
            new TimingReBalanceJob().postExecute(null, dto);
        }
    }

}
