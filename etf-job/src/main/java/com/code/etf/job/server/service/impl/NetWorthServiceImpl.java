package com.code.etf.job.server.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.code.api.RespDict;
import com.code.etf.admin.api.dto.ProductConfigDTO;
import com.code.etf.admin.api.dto.RebalanceRecordDTO;
import com.code.etf.admin.api.menu.KafkaTopicKeys;
import com.code.etf.admin.api.menu.RedisKeysEnum;
import com.code.etf.admin.api.message.NetWorthMessage;
import com.code.etf.job.server.service.NetWorthService;
import com.code.exception.ExceptionBuilder;
import com.code.message.ExternalMessage;
import com.code.message.InternalMessage;
import com.code.springboot.redis.server.RedisManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * S(0)：基金初值;
 * S(t)：基金在 t 时刻的净值，t ≥ 0；
 * P(0)：标的资产在初始时刻的价格；
 * P(t)：标的资产在 t 时刻的价格；
 * M：目标杠杆倍数，可取 2、3、-1、-2、-3。
 * K：当日标的价格反方向涨跌幅不超过15%（后台可配置），k为一天；当日标的价格第一次反方向涨跌幅超过15%，从0点到此刻记为一个周期k。
 * 上一个周期净值 * (1 + 目标杠杠倍数 * (当前标的价格 - 上个周期标的价格) / 上一个周期标的价格)
 * 净值公式：  S(v) = S(tk - 1) * ( 1 + M * (P(v) - P(tk - 1)) / P(tk - 1) )
 */
@Slf4j
@Service
public class NetWorthServiceImpl implements NetWorthService {
    @Autowired
    private RedisManager redisManager;
    @Autowired
    private KafkaTemplate kafkaTemplate;

    @Override
    public void execute(ProductConfigDTO dto, RebalanceRecordDTO reBalanceRecord, BigDecimal dealPairTrackPrice) {
        log.info("==================开始执行任务: 净值计算==================");
        final String pair = dto.getDealPair();

        // 当前产品的计算因子
        String dealPairNetWorthLoadFactorStr = String.valueOf(redisManager.get(String.format(RedisKeysEnum.ETF_DEAL_PAIR_NET_WORTH_LOAD_FACTOR.getKey(), pair)));
        BigDecimal dealPairNetWorthLoadFactor = new BigDecimal(dealPairNetWorthLoadFactorStr);
        log.info("执行{}交易对净值计算时，当前产品的计算因子:{}", pair, dealPairNetWorthLoadFactorStr);


        BigDecimal result = getBigDecimal(pair, reBalanceRecord.getNetWorth(), BigDecimal.valueOf(reBalanceRecord.getMultiple()), reBalanceRecord.getDealPairTrackPrice(), dealPairTrackPrice, dealPairNetWorthLoadFactor);

        if (result.compareTo(BigDecimal.ZERO) <= 0) {
            log.error("执行{}交易对净值计算时, 净值出现负数:{},净值计算因子:{}", pair, result, dealPairNetWorthLoadFactor);
            ExceptionBuilder.service(InternalMessage.builder().template("交易对净值计算时, 净值出现负数!").build()
                    , ExternalMessage.builder().template(RespDict.SERVICE_ERROR.getMsg()).code(RespDict.SERVICE_ERROR.getCode().toString()).build());
        }

        saveRedisAndPushEvent(pair, result);
    }

    /**
     * 发送净值到redis和kafka
     *
     * @param dealPair
     * @param result
     */
    @Override
    public void saveRedisAndPushEvent(String dealPair, BigDecimal result) {
        //TODO 后期存储优化，做k线

        NetWorthMessage netWorth = NetWorthMessage.builder()
                .dealPair(dealPair)
                .val(result)
                .time(System.currentTimeMillis())
                .build();
        redisManager.set(String.format(RedisKeysEnum.ETF_NET_WORTH_NEW.getKey(), dealPair), JSONObject.toJSONString(netWorth));


        kafkaTemplate.send(KafkaTopicKeys.ETF_DEAL_PAIR_NET_WORTH, JSONObject.toJSONString(netWorth));
    }

    /**
     * 净值计算
     *
     * @param dealPair                   交易对
     * @param stk1                       上一个周期净值
     * @param m                          目标刚刚倍数
     * @param ptk1                       上一个周期标的价格
     * @param dealPairTrackPrice         当前标的追踪价格
     * @param dealPairNetWorthLoadFactor 净值计算因子
     * @return 净值
     */
    @Override
    public BigDecimal getBigDecimal(String dealPair, BigDecimal stk1, BigDecimal m, BigDecimal ptk1, BigDecimal dealPairTrackPrice, BigDecimal dealPairNetWorthLoadFactor) {
        BigDecimal result = stk1.multiply(BigDecimal.ONE.add(m.multiply(dealPairTrackPrice.subtract(ptk1).divide(ptk1, 18, RoundingMode.DOWN))));
        result = result.multiply(dealPairNetWorthLoadFactor).setScale(4, RoundingMode.DOWN);
        log.info("执行{}交易对净值计算时保留4位, : S(v) = [S(tk-1) * ( 1 + M * (P(v) - P(tk-1)) / P(tk-1) )] * factor, 净值计算：{} = [{} * (1  + {} * ({} - {}) / {})] * {}"
                , dealPair, result, stk1, m, dealPairTrackPrice, ptk1, ptk1, dealPairNetWorthLoadFactor);
        return result;
    }

    @Override
    public NetWorthMessage productByDealPair(@RequestParam String symbol) {
        if (StringUtils.isBlank(symbol)) {
            return NetWorthMessage.builder().build();
        }
        symbol = symbol.toLowerCase();
        Object val = redisManager.get(String.format(RedisKeysEnum.ETF_NET_WORTH_NEW.getKey(), symbol));
        NetWorthMessage netWorth = JSONObject.parseObject(String.valueOf(val), NetWorthMessage.class);
        if (netWorth == null) {
            return NetWorthMessage.builder().dealPair(symbol).val(BigDecimal.ZERO).time(0L).build();
        }
        return netWorth;
    }
}
