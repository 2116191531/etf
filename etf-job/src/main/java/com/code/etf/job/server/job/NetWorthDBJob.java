package com.code.etf.job.server.job;

import com.code.etf.admin.api.dto.NetWorthDTO;
import com.code.etf.admin.api.dto.ProductConfigDTO;
import com.code.etf.admin.api.message.NetWorthMessage;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.PersistJobDataAfterExecution;

import java.math.BigDecimal;

/**
 * 净值计算因子恢复
 * 每次定时再平衡 5分钟执行。此举纯属补偿措施
 *
 * @author qilou
 * @date 2021-6-29
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
@Slf4j
public class NetWorthDBJob extends AbstractEtfJob {
    @Override
    public void postExecute(JobExecutionContext context, ProductConfigDTO productConfig) {
        final String pair = productConfig.getDealPair();
        log.info("==================开始执行任务: {} 净值存储==================", pair);
        log.info("执行任务线程ID{}", Thread.currentThread().getId());

        NetWorthMessage netWorth = netWorthService.productByDealPair(pair);

        if (netWorth.getVal().compareTo(BigDecimal.ZERO) <= 0) {
            log.error("执行{}交易对净值存储时, 净值出现负数或0:{}", pair, netWorth.getVal());
            return;
        }

        netWorthFeign.create(NetWorthDTO.builder().dealPair(pair).val(netWorth.getVal()).time(netWorth.getTime()).build());

    }
}