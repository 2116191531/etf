package com.code.etf.job.server.job;

import com.alibaba.fastjson.JSONObject;
import com.code.etf.admin.api.dto.ProductConfigDTO;
import com.code.etf.admin.api.feign.ProductConfigFeign;
import com.code.etf.admin.api.menu.EtfMainSwitcher;
import com.code.etf.admin.api.menu.RedisKeysEnum;
import com.code.etf.job.server.service.QuartzManager;
import com.code.springboot.redis.server.RedisManager;
import com.code.util.web.SpringContextUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

@Component
@Slf4j
public class SchedulerListener implements ApplicationListener<ContextRefreshedEvent> {
    private static final AtomicBoolean SWITCH = new AtomicBoolean(false);

    @Autowired
    private QuartzManager quartzManager;


    @Override
    public synchronized void onApplicationEvent(ContextRefreshedEvent event) {
        if (SWITCH.compareAndSet(false, true)) {
            if (SpringContextUtils.getApplicationContext() == null) {
                SpringContextUtils.setApp(event.getApplicationContext());
            }
            try {
                ProductConfigFeign productConfigFeign = SpringContextUtils.getBean(ProductConfigFeign.class);

                List<ProductConfigDTO> list = productConfigFeign.findAll();
                log.info("初始化etf产品配置列表:{}", list);

                if (list == null || list.size() <= 0) {
                    log.warn("没有etf产品");
                    return;
                }
                quartzManager.startJobs();
                //TODO  GOURP  查詢刪除，避免以前存在的沒有刪除
                for (ProductConfigDTO dto : list) {
                    cleanJob(dto);
                    addJob(dto);
                }
            } catch (Exception e) {
                log.error("定时任务初始化失败", e);
            }
        }
    }


    public void addJob(ProductConfigDTO productConfigDTO) {
        int hour = productConfigDTO.getTimeEquilibrium();
        int lastHour;
        if (hour <= 0 || hour >= 24) {
            hour = 0;
            lastHour = 23;
        } else {
            lastHour = hour - 1;
        }
        final JSONObject dto = JSONObject.parseObject(JSONObject.toJSONString(productConfigDTO));
        final String pair = productConfigDTO.getDealPair();
        // 净值、不定时再平衡
        String jobName = String.format(JobEnum.ETF_NET_WORTH_TASK.getTaskName(), pair);
        quartzManager.addSimpleJob(jobName, 1, TimeUnit.SECONDS, dto, NetWorthAndRebalanceJob.class);

        //净值存储
        jobName = String.format(JobEnum.ETF_NET_WORTH_DB_TASK.getTaskName(), pair);
        quartzManager.addSimpleJob(jobName, 60, TimeUnit.SECONDS, dto, NetWorthDBJob.class);


        // 净值计算因子变化
        jobName = String.format(JobEnum.ETF_NET_WORTH_CHANGE_TASK.getTaskName(), pair);
        quartzManager.addCronJob(jobName, "0 " + 55 + " " + lastHour + " * * ?", dto, NetWorthChangeJob.class);

        // 定时再平衡
        jobName = String.format(JobEnum.ETF_TIMING_RE_BALANCE_TASK.getTaskName(), pair);
        quartzManager.addCronJob(jobName, "0 0 " + hour + " * * ?", dto, TimingReBalanceJob.class);
    }

    public void cleanJob(ProductConfigDTO productConfigDTO) {
        final String pair = productConfigDTO.getDealPair();

        String jobName = String.format(JobEnum.ETF_NET_WORTH_TASK.getTaskName(), pair);
        quartzManager.removeJob(jobName);

        jobName = String.format(JobEnum.ETF_NET_WORTH_CHANGE_TASK.getTaskName(), pair);
        quartzManager.removeJob(jobName);

        jobName = String.format(JobEnum.ETF_NET_WORTH_DB_TASK.getTaskName(), pair);
        quartzManager.removeJob(jobName);


        jobName = String.format(JobEnum.ETF_TIMING_RE_BALANCE_TASK.getTaskName(), pair);
        quartzManager.removeJob(jobName);
    }


    enum JobEnum {
        ETF_NET_WORTH_CHANGE_TASK("ETF_NET_WORTH_CHANGE_TASK_NAME:%s", " ETF_NET_WORTH_CHANGE_TRIGGER_NAME"),
        ETF_NET_WORTH_TASK("ETF_NET_WORTH_TASK_NAME:%s", "ETF_NET_WORTH_TRIGGER_NAME"),
        ETF_NET_WORTH_DB_TASK("ETF_NET_WORTH_DB_TASK_NAME:%s", "ETF_NET_WORTH_DB_TRIGGER_NAME"),
        @Deprecated
        ETF_NET_WORTH_RECOVERY_TASK("ETF_NET_WORTH_RECOVERY_TASK_NAME:%s", "ETF_NET_WORTH_RECOVERY_TRIGGER_NAME"),
        @Deprecated
        ETF_THRESHOLD_CHECK_RE_BALANCE_TASK("ETF_THRESHOLD_CHECK_RE_BALANCE_TASK_NAME:%s", "ETF_THRESHOLD_CHECK_RE_BALANCE_TRIGGER_NAME"),
        ETF_TIMING_RE_BALANCE_TASK("ETF_TIMING_RE_BALANCE_TASK_NAME:%s", "ETF_TIMING_RE_BALANCE_TRIGGER_NAME");

        private final String taskName;
        private final String triggerName;

        JobEnum(String taskName, String triggerName) {
            this.taskName = taskName;
            this.triggerName = triggerName;
        }

        public String getTriggerName() {
            return triggerName;
        }

        public String getTaskName() {
            return taskName;
        }
    }
}
