package com.code.etf.job.server.service;

import com.alibaba.fastjson.JSONObject;
import com.code.etf.job.server.config.QuotationProcessProperties;
import com.code.etf.job.server.utils.HttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Connection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;

@Slf4j
@Service
public class QuotationManager {

    @Autowired
    private QuotationProcessProperties quotationProcessProperties;

    public BigDecimal getDealPairPrice(String dealPair) {
        Connection.Response response = null;
        try {
            response = HttpUtils.get(quotationProcessProperties.getUrl() + "/deal-pair-price/" + dealPair);
            return JSONObject.parseObject(String.valueOf(response.body()), BigDecimal.class);
        } catch (IOException e) {
            log.error("查询{}交易对的最新平均价失败：{}]", dealPair, response, e);
        }
        return null;
    }
}
