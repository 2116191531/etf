package com.code.etf.job.server.service;

import com.code.etf.admin.api.dto.ProductConfigDTO;
import com.code.etf.admin.api.dto.RebalanceRecordDTO;
import com.code.etf.admin.api.message.NetWorthMessage;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;


public interface NetWorthService {


    void execute(ProductConfigDTO dto, RebalanceRecordDTO reBalanceRecord, BigDecimal dealPairTrackPrice);

    /**
     * 发送净值到redis和kafka
     *
     * @param dealPair
     * @param result
     */
    void saveRedisAndPushEvent(String dealPair, BigDecimal result);

    /**
     * 净值计算
     *
     * @param dealPair                   交易对
     * @param stk1                       上一个周期净值
     * @param m                          目标刚刚倍数
     * @param ptk1                       上一个周期标的价格
     * @param dealPairTrackPrice         当前标的追踪价格
     * @param dealPairNetWorthLoadFactor 净值计算因子
     * @return 净值
     */
    BigDecimal getBigDecimal(String dealPair, BigDecimal stk1, BigDecimal m, BigDecimal ptk1, BigDecimal dealPairTrackPrice, BigDecimal dealPairNetWorthLoadFactor);

    NetWorthMessage productByDealPair(@RequestParam String symbol);
}
