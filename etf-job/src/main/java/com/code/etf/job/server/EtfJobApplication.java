package com.code.etf.job.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication(scanBasePackages = "com.code.etf.job.server")
@EnableDiscoveryClient
public class EtfJobApplication {

    public static void main(String[] args) {
        SpringApplication.run(EtfJobApplication.class, args);
    }
}