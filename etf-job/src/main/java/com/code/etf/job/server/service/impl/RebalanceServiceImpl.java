package com.code.etf.job.server.service.impl;

import cn.hutool.core.date.DateUnit;
import com.code.api.RespDict;
import com.code.commons.mojo.result.ResultData;
import com.code.commons.mojo.result.ResultStatus;
import com.code.etf.admin.api.dto.ProductConfigDTO;
import com.code.etf.admin.api.dto.RebalanceRecordDTO;
import com.code.etf.admin.api.dto.RedemptionOrderStatisticsDTO;
import com.code.etf.admin.api.feign.RebalanceRecordFeign;
import com.code.etf.admin.api.feign.RedemptionOrderFeign;
import com.code.etf.admin.api.menu.RebalanceTypeEnum;
import com.code.etf.admin.api.menu.RedisKeysEnum;
import com.code.etf.admin.api.message.NetWorthMessage;
import com.code.etf.job.server.service.NetWorthService;
import com.code.etf.job.server.service.RebalanceService;
import com.code.exception.ExceptionBuilder;
import com.code.message.ExternalMessage;
import com.code.message.InternalMessage;
import com.code.risk.notice.DingDingUtils;
import com.code.springboot.redis.server.RedisManager;
import com.code.util.env.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 篮子计算公式：
 * 当期篮子（交易货币） = 当期基础净值价格 * 杠杆倍数 / 当期基础标的价格
 * 当期篮子（计价货币） = 当期基础净值价格 - 当期篮子（交易货币） * 当期基础标的价格
 * 当期期末杠杠倍数(调仓前杠杠) = 当期篮子(计划货币) * 最新标的价格 / 最新净值
 * 调仓后篮子(交易货币) = 基础杠杠倍数 * 最新净值 / 最新标的价格
 * 调仓后篮子(交易货币) = 最新净值 - (调仓后篮子(交易货币) * 最新标的价格)
 */
@Slf4j
@Service
public class RebalanceServiceImpl implements RebalanceService {
    @Autowired
    private RedisManager redisManager;
    @Autowired
    private RedemptionOrderFeign redemptionOrderFeign;
    @Autowired
    private RebalanceRecordFeign rebalanceRecordFeign;
    @Autowired
    private NetWorthService netWorthService;

    public void execute(RebalanceTypeEnum type, ProductConfigDTO dto, RebalanceRecordDTO reBalanceRecord, BigDecimal dealPairTrackPrice) {
        final String pair = dto.getDealPair();
        log.info("==================开始执行任务: {} 开始执行{}==================", pair, type.getDesc());

        //获得最新净值
        final NetWorthMessage netWorthMessage = netWorthService.productByDealPair(pair);
        log.info("执行{}交易对{}时，最新净值:{}", pair, type.getDesc(), netWorthMessage);
        BigDecimal netWorthNew = netWorthMessage.getVal();
        if (netWorthNew.compareTo(BigDecimal.ZERO) <= 0) {
            log.warn("执行{}交易对{}时，获取净值数据异常:{}", pair, type.getDesc(), netWorthNew);
            ExceptionBuilder.service(InternalMessage.builder().template("净值数据异常").build()
                    , ExternalMessage.builder().template(RespDict.SERVICE_ERROR.getMsg()).code(RespDict.SERVICE_ERROR.getCode().toString()).build());
        }

        // 当期篮子 交易货币
        BigDecimal nowCoinTradePrice = reBalanceRecord.getCoinTradePrice();
        log.info("执行{}交易对{}时，获取当期篮子(交易货币)----------1、公式：获取上一次调仓后篮子(交易货币), 2、结果：{}", pair, type.getDesc(), nowCoinTradePrice);
        // 当期篮子 计价货币
        BigDecimal nowCoinValuationPrice = reBalanceRecord.getCoinValuationPrice();
        log.info("执行{}交易对{}时，获取当期篮子(计划货币)----------1、公式：获取上一次调仓后篮子(计划货币), 2、结果：{}", pair, type.getDesc(), nowCoinValuationPrice);
        // 当期期末杠杠倍数(调仓前杠杠)
        BigDecimal nowMultiple = nowCoinTradePrice.multiply(dealPairTrackPrice).divide(netWorthNew, 2, RoundingMode.DOWN);
        log.info("执行{}交易对{}时，当期期末杠杠倍数(调仓前杠杠)----------1、公式：当期期末杠杠倍数(调仓前杠杠) = 当期篮子(计划货币) * 最新标的价格 / 最新净值  2、结果：{} = {} * {} / {}", pair, type.getDesc(), nowMultiple, nowCoinTradePrice, dealPairTrackPrice, netWorthNew);
        // 调仓后篮子 交易货币
        BigDecimal coinTradePrice = BigDecimal.valueOf(dto.getMultiple()).multiply(netWorthNew).divide(dealPairTrackPrice, 18, RoundingMode.DOWN);
        log.info("执行{}交易对{}时，调仓后篮子(交易货币)----------1、公式：调仓后篮子(交易货币) = 基础杠杠倍数 * 最新净值 / 最新标的价格  2、结果：{} = {} * {} / {}", pair, type.getDesc(), coinTradePrice, dto.getMultiple(), netWorthNew, dealPairTrackPrice);
        // 调仓后 篮子 计价货币
        BigDecimal CoinValuationPrice = netWorthNew.subtract(coinTradePrice.multiply(dealPairTrackPrice));
        log.info("执行{}交易对{}时，调仓后篮子(交易货币)----------1、公式：调仓后篮子(交易货币) = 最新净值 - (调仓后篮子(交易货币) * 最新标的价格) 2、结果：{} = {} - ({} * {})", pair, type.getDesc(), CoinValuationPrice, netWorthNew, coinTradePrice, dealPairTrackPrice);

        RedemptionOrderStatisticsDTO redemptionOrderStatistics = null;
        BigDecimal total = BigDecimal.ZERO;
        try {
            // 由于这个接口老出问题，只能这样处理掉了。
            ResultData<RedemptionOrderStatisticsDTO> statisticsResult = redemptionOrderFeign.getRedemptionOrderStatistics(dto.getCoinTrade());
            if (statisticsResult == null || !ResultStatus.SUCCESS.getCode().equals(statisticsResult.getCode()) || statisticsResult.getData() == null) {
                log.warn("/etf-admin/redemption_order/statistics/{countUnit} response: {}", statisticsResult);
            } else {
                redemptionOrderStatistics = statisticsResult.getData();
                if (redemptionOrderStatistics != null) {
                    if (redemptionOrderStatistics.getMarketMakeTotalHoldAmount() != null) {
                        total = total.add(redemptionOrderStatistics.getMarketMakeTotalHoldAmount());
                    }
                    if (redemptionOrderStatistics.getUserTotalHoldAmount() != null) {
                        total = total.add(redemptionOrderStatistics.getUserTotalHoldAmount());
                    }
                }
            }
        } catch (Exception e) {
            log.warn("/etf-admin/redemption_order/statistics/{countUnit} 调用异常", e);
        }
        log.info("执行{}交易对{}时，申赎统计分析数据:{}", pair, type.getDesc(), redemptionOrderStatistics);


        // 执行再平衡
        RebalanceRecordDTO temp =
                RebalanceRecordDTO
                        .builder()
                        .dealPair(pair)
                        .type(type.getKey())
                        .coinTrade(dto.getCoinTrade())
                        .coinValuation(dto.getCoinValuation())
                        .netWorthOld(reBalanceRecord.getNetWorth())
                        .netWorth(netWorthNew)
                        .dealPairTrackPriceOld(reBalanceRecord.getDealPairTrackPrice())
                        .dealPairTrack(dto.getDealPairTrack())
                        //标的当前价格
                        .dealPairTrackPrice(dealPairTrackPrice)
                        // 当前交易货币
                        .coinTradePriceOld(nowCoinTradePrice)
                        .coinTradePrice(coinTradePrice)
                        // 当前计价货币
                        .coinValuationPriceOld(nowCoinValuationPrice)
                        .coinValuationPrice(CoinValuationPrice)
                        // 当前杠杠倍数
                        .leverageMultipleOld(nowMultiple)
                        .leverageMultiple(dto.getMultiple())
                        .multiple(dto.getMultiple())
                        // 用户总持仓
                        .userPositionToal(total)
                        .createTime(System.currentTimeMillis())
                        .createBy("SYSTEM")
                        .build();

        log.info("执行{}交易对{}时，执行再平衡数据：{}", pair, type.getDesc(), temp);
        rebalanceRecordFeign.create(temp);

        // 恢复净值系数
        redisManager.set(String.format(RedisKeysEnum.ETF_DEAL_PAIR_NET_WORTH_LOAD_FACTOR.getKey(), pair), BigDecimal.ONE);

        // TODO 发送钉钉，后面抽出配置
        String webhook = "https://oapi.dingtalk.com/robot/send?access_token=f94b391aa91242ef27266a5a29fb52615ba9e70f416a6ef5fdcec4bf2eb37962";
        String secret = "SECe1b87c4342c145f3a10493a39b3ac5d1c2684c81dfe7aac639c10ad2eac279b1";
        DingDingUtils.sendMsg(new StringBuilder()
                .append("环境：")
                .append(System.getenv("env")).append("\nn")
                .append("交易对:")
                .append(pair).append("\nn")
                .append("调仓类型：")
                .append(type.getDesc()).append("\nn")
                .append("计价货币单位")
                .append(temp.getCoinValuation()).append("\nn")
                .append("交易货币单位")
                .append(temp.getCoinTrade()).append("\nn")
                .append("上一次调仓：净值")
                .append(temp.getNetWorthOld()).append(temp.getCoinValuation()).append("\nn")
                .append("本次调仓：净值")
                .append(temp.getNetWorth()).append(temp.getCoinValuation()).append("\nn")
                .append("上一次调仓：标的价格")
                .append(temp.getDealPairTrackPriceOld()).append(temp.getCoinValuation()).append("\nn")
                .append("本次调仓：标的价格")
                .append(temp.getDealPairTrackPrice()).append(temp.getCoinValuation()).append("\nn")
                .append("标的交易对")
                .append(temp.getDealPairTrack()).append("\nn")
                .append("调仓前：篮子-交易货币值")
                .append(temp.getCoinTradePriceOld()).append(temp.getCoinTrade()).append("\nn")
                .append("调仓后：篮子-交易货币")
                .append(temp.getCoinTradePrice()).append(temp.getCoinTrade()).append("\nn")
                .append("调仓前：篮子-计价货币值")
                .append(temp.getCoinValuationPriceOld()).append(temp.getCoinValuation()).append("\nn")
                .append("调仓后：篮子-计价货币")
                .append(temp.getCoinValuationPrice()).append(temp.getCoinValuation()).append("\nn")
                .append("调仓前：杠杠倍数")
                .append(temp.getLeverageMultipleOld()).append("\nn")
                .append("调仓后：杠杠倍数")
                .append(temp.getLeverageMultiple()).append("\nn")
                .append("基础杠杠倍数")
                .append(temp.getMultiple()).append("\nn")
                .append("用户持仓总量")
                .append(temp.getUserPositionToal()).append(temp.getCoinTrade()).append("\nn")
                .append("创建时间:")
                .append(DateUtil.format(temp.getCreateTime())).append("\nn")
                .toString(), webhook, secret);
    }
}
