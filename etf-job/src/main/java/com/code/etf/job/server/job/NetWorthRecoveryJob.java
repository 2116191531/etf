package com.code.etf.job.server.job;

import com.code.etf.admin.api.dto.ProductConfigDTO;
import com.code.etf.admin.api.menu.RedisKeysEnum;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.PersistJobDataAfterExecution;

import java.math.BigDecimal;

/**
 * 净值计算因子恢复
 * 每次定时再平衡 5分钟执行。此举纯属补偿措施
 *
 * @author qilou
 * @date 2021-6-29
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
@Slf4j
@Deprecated
public class NetWorthRecoveryJob extends AbstractEtfJob {
    @Override
    public void postExecute(JobExecutionContext context, ProductConfigDTO productConfig) {
        log.info("==================开始执行任务: 净值计算因子恢复==================");
        log.info("执行任务线程ID{}", Thread.currentThread().getId());

        //  设置xx 交易对 净值计算因子  恢复为1
        try {

            redisManager.set(String.format(RedisKeysEnum.ETF_DEAL_PAIR_NET_WORTH_LOAD_FACTOR.getKey(), productConfig.getDealPair()), BigDecimal.ONE);
        } catch (Exception e) {
            log.error("管理费恢复失败(人工干预)：", e);
        }
    }
}