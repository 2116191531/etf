package com.code.etf.job.server.job;

import com.alibaba.fastjson.JSONObject;
import com.code.api.RespDict;
import com.code.commons.mojo.result.ResultData;
import com.code.commons.mojo.result.ResultStatus;
import com.code.etf.admin.api.dto.ProductConfigDTO;
import com.code.etf.admin.api.dto.RebalanceRecordDTO;
import com.code.etf.admin.api.dto.RedemptionOrderStatisticsDTO;
import com.code.etf.admin.api.menu.RebalanceTypeEnum;
import com.code.etf.admin.api.menu.RedisKeysEnum;
import com.code.etf.admin.api.message.NetWorthMessage;
import com.code.exception.ExceptionBuilder;
import com.code.message.ExternalMessage;
import com.code.message.InternalMessage;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.PersistJobDataAfterExecution;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 标的价格涨跌幅阈值触发：不定时再平衡
 *
 * @author qilou
 * @date 2021-6-29
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
@Slf4j
@Deprecated
public class ThresholdCheckReBalanceJob extends AbstractEtfJob {
    @Override
    public void postExecute(JobExecutionContext context, ProductConfigDTO dto) {
        log.info("==================开始执行任务: 阈值验证==================");
        log.info("执行任务线程ID{}", Thread.currentThread().getId());
        final String pair = dto.getDealPair();
        // 追踪交易对价格

        BigDecimal dealPairTrackPrice = quotationManager.getDealPairPrice(dto.getDealPairTrack());
        log.info("执行{}交易对调仓阈值验证时，追踪标的价格:{}", pair, dealPairTrackPrice);
        if (dealPairTrackPrice == null || dealPairTrackPrice.compareTo(BigDecimal.ZERO) <= 0) {
            log.error("执行{}交易对调仓阈值验证时，标的交易对数据不合法，请关闭当前交易对，{}最新标的价格:{}", pair, dto.getDealPairTrack(), dealPairTrackPrice);
            return;
        }

        //获取最新调仓记录
        ResultData<RebalanceRecordDTO> reBalanceRecordResult = rebalanceRecordFeign.findOneRebalanceRecordByNow(pair);
        log.info("执行{}交易对调仓阈值验证时，最新调仓记录:{}", pair, reBalanceRecordResult);
        if (reBalanceRecordResult == null || !ResultStatus.SUCCESS.getCode().equals(reBalanceRecordResult.getCode()) || reBalanceRecordResult.getData() == null) {
            log.error("执行{}交易对调仓阈值验证时，最新调仓记录失败:{}", pair, reBalanceRecordResult);
            ExceptionBuilder.service(InternalMessage.builder().template("交易对调仓阈值验证时，最新调仓记录失败!").build()
                    , ExternalMessage.builder().template(RespDict.SERVICE_ERROR.getMsg()).code(RespDict.SERVICE_ERROR.getCode().toString()).build());
        }
        RebalanceRecordDTO reBalanceRecord = reBalanceRecordResult.getData();

        //计算涨跌幅
        BigDecimal thresholdUpDown = dto.getThresholdUpDown();
        log.info("执行{}交易对调仓阈值验证时，涨跌幅阈值：{}", pair, thresholdUpDown);
        BigDecimal difference = dealPairTrackPrice.subtract(reBalanceRecord.getDealPairTrackPrice());
        if (difference.compareTo(BigDecimal.ZERO) == 0) {
            log.info("执行{}交易对调仓阈值验证时，最新标的和上一次调仓标的价格无变化----------1、公式： 最新标的价格 - 上一次调仓标的价格 , 2、结果：{} = {} - {}", pair, difference, dealPairTrackPrice, reBalanceRecord.getDealPairTrackPrice());
            return;
        }
        BigDecimal result = difference.divide(reBalanceRecord.getDealPairTrackPrice(), 3, RoundingMode.DOWN).abs();
        log.info("执行{}交易对调仓阈值验证时，计算涨跌幅值----------1、公式：(最新标的价格 - 上一次调仓标的价格) / 上一次调仓标的价格, 2、结果：{} = ({} - {}) / {}", pair, result, dealPairTrackPrice, reBalanceRecord.getDealPairTrackPrice(), reBalanceRecord.getDealPairTrackPrice());

        if (result.compareTo(BigDecimal.ZERO) == 0) {
            log.info("执行{}交易对调仓阈值验证时，计算涨跌幅值---------- 数据计算为{}，停止", pair, result);
            return;
        }
        if (result.compareTo(thresholdUpDown) < 0) {
            return;
        }

        log.info("==================开始执行任务: 不定时再平衡触发==================");

        log.info("执行任务线程ID{}", Thread.currentThread().getId());
        //最新净值
        Object val = redisManager.get(String.format(RedisKeysEnum.ETF_NET_WORTH_NEW.getKey(), pair));
        NetWorthMessage netWorth = JSONObject.parseObject(String.valueOf(val), NetWorthMessage.class);
        if (netWorth == null) {
            log.warn("执行{}交易对定时再平衡时，获取净值数据NetWorthMessage异常:{}", pair, netWorth);
            ExceptionBuilder.service(InternalMessage.builder().template("净值数据异常").build()
                    , ExternalMessage.builder().template(RespDict.SERVICE_ERROR.getMsg()).code(RespDict.SERVICE_ERROR.getCode().toString()).build());
        }
        BigDecimal netWorthNew = netWorth.getVal();
        log.info("执行{}交易对不定时再平衡时，最新净值:{}", pair, netWorthNew);
        if (netWorthNew.compareTo(BigDecimal.ZERO) <= 0) {
            log.warn("执行{}交易对不定时再平衡时，获取净值数据异常:{}", pair, netWorthNew);
            ExceptionBuilder.service(InternalMessage.builder().template("净值数据异常").build()
                    , ExternalMessage.builder().template(RespDict.SERVICE_ERROR.getMsg()).code(RespDict.SERVICE_ERROR.getCode().toString()).build());
        }

        /*
        篮子计算公式：
            公式需要重新核对一下。
                当期篮子（交易货币） = 当期基础净值价格 * 杠杆倍数 / 当期基础标的价格
                当期篮子（计价货币） = 当期基础净值价格 - 当期篮子（交易货币） * 当期基础标的价格
                当期期末杠杠倍数(调仓前杠杠) = 当期篮子(计划货币) * 最新标的价格 / 最新净值
                调仓后篮子(交易货币) = 基础杠杠倍数 * 最新净值 / 最新标的价格
                调仓后篮子(交易货币) = 最新净值 - (调仓后篮子(交易货币) * 最新标的价格)
         */
        // 当期篮子 交易货币
        BigDecimal nowCoinTradePrice = reBalanceRecord.getCoinTradePrice();
        log.info("执行{}交易对不定时再平衡时，获取当期篮子(交易货币)----------1、公式：获取上一次调仓后篮子(交易货币), 2、结果：{}", pair, nowCoinTradePrice);
        // 当期篮子 计价货币
        BigDecimal nowCoinValuationPrice = reBalanceRecord.getCoinValuationPrice();
        log.info("执行{}交易对不定时再平衡时，获取当期篮子(计划货币)----------1、公式：获取上一次调仓后篮子(计划货币), 2、结果：{}", pair, nowCoinValuationPrice);
        // 当期期末杠杠倍数(调仓前杠杠)
        BigDecimal nowMultiple = nowCoinTradePrice.multiply(dealPairTrackPrice).divide(netWorthNew, 3, RoundingMode.DOWN);
        log.info("执行{}交易对不定时再平衡时，当期期末杠杠倍数(调仓前杠杠)----------1、公式：当期期末杠杠倍数(调仓前杠杠) = 当期篮子(计划货币) * 最新标的价格 / 最新净值  2、结果：{} = {} * {} / {}", pair, nowMultiple, nowCoinTradePrice, dealPairTrackPrice, netWorthNew);
        // 调仓后篮子 交易货币
        BigDecimal coinTradePrice = BigDecimal.valueOf(dto.getMultiple()).multiply(netWorthNew).divide(dealPairTrackPrice, 18, RoundingMode.DOWN);
        log.info("执行{}交易对不定时再平衡时，调仓后篮子(交易货币)----------1、公式：调仓后篮子(交易货币) = 基础杠杠倍数 * 最新净值 / 最新标的价格  2、结果：{} = {} * {} / {}", pair, coinTradePrice, dto.getMultiple(), netWorthNew, dealPairTrackPrice);
        // 调仓后 篮子 计价货币
        BigDecimal CoinValuationPrice = netWorthNew.subtract(coinTradePrice.multiply(dealPairTrackPrice));
        log.info("执行{}交易对不定时再平衡时，调仓后篮子(交易货币)----------1、公式：调仓后篮子(交易货币) = 最新净值 - (调仓后篮子(交易货币) * 最新标的价格) 2、结果：{} = {} - ({} * {})", pair, CoinValuationPrice, netWorthNew, coinTradePrice, dealPairTrackPrice);

        RedemptionOrderStatisticsDTO redemptionOrderStatistics = null;
        BigDecimal total = BigDecimal.ZERO;
        try {
            // 由于这个接口老出问题，只能这样处理掉了。
            ResultData<RedemptionOrderStatisticsDTO> statisticsResult = redemptionOrderFeign.getRedemptionOrderStatistics(dto.getCoinTrade());
            if (statisticsResult == null || !ResultStatus.SUCCESS.getCode().equals(statisticsResult.getCode()) || statisticsResult.getData() == null) {
                log.warn("/etf-admin/redemption_order/statistics/{countUnit} response: {}", statisticsResult);
            } else {
                redemptionOrderStatistics = statisticsResult.getData();
                if (redemptionOrderStatistics != null) {
                    if (redemptionOrderStatistics.getMarketMakeTotalHoldAmount() != null) {
                        total = total.add(redemptionOrderStatistics.getMarketMakeTotalHoldAmount());
                    }
                    if (redemptionOrderStatistics.getUserTotalHoldAmount() != null) {
                        total = total.add(redemptionOrderStatistics.getUserTotalHoldAmount());
                    }
                }
            }
        } catch (Exception e) {
            log.warn("/etf-admin/redemption_order/statistics/{countUnit} 调用异常", e);
        }
        log.info("执行{}交易对不定时再平衡时，申赎统计分析数据:{}", pair, redemptionOrderStatistics);


        // 执行再平衡
        RebalanceRecordDTO temp =
                RebalanceRecordDTO
                        .builder()
                        .dealPair(pair)
                        .type(RebalanceTypeEnum.THRESHOLD_CHECK_RE_BALANCE.getKey())
                        .coinTrade(dto.getCoinTrade())
                        .coinValuation(dto.getCoinValuation())
                        .netWorthOld(reBalanceRecord.getNetWorth())
                        .netWorth(netWorthNew)
                        .dealPairTrackPriceOld(reBalanceRecord.getDealPairTrackPrice())
                        .dealPairTrack(dto.getDealPairTrack())
                        //标的当前价格
                        .dealPairTrackPrice(dealPairTrackPrice)

                        // 当前交易货币
                        .coinTradePriceOld(nowCoinTradePrice)
                        .coinTradePrice(coinTradePrice)
                        // 当前计价货币
                        .coinValuationPriceOld(nowCoinValuationPrice)
                        .coinValuationPrice(CoinValuationPrice)
                        // 当前杠杠倍数
                        .leverageMultipleOld(nowMultiple)
                        .leverageMultiple(dto.getMultiple())
                        .multiple(dto.getMultiple())
                        // 用户总持仓
                        .userPositionToal(total)
                        .createTime(System.currentTimeMillis())
                        .createBy("SYSTEM")
                        .build();

        log.info("执行{}交易对不定时再平衡时，执行再平衡数据：{}", pair, temp);
        rebalanceRecordFeign.create(temp);

        // 恢复净值系数
        BigDecimal value = BigDecimal.ONE.subtract(dto.getFeeMangeActual());
        redisManager.set(String.format(RedisKeysEnum.ETF_DEAL_PAIR_NET_WORTH_LOAD_FACTOR.getKey(), pair), value);
    }
}
