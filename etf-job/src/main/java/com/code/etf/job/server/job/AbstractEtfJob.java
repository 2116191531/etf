package com.code.etf.job.server.job;

import com.alibaba.fastjson.JSONObject;
import com.code.etf.admin.api.dto.ProductConfigDTO;
import com.code.etf.admin.api.feign.NetWorthFeign;
import com.code.etf.admin.api.feign.RebalanceRecordFeign;
import com.code.etf.admin.api.feign.RedemptionOrderFeign;
import com.code.etf.admin.api.menu.EtfMainSwitcher;
import com.code.etf.admin.api.menu.RedisKeysEnum;
import com.code.etf.job.server.service.NetWorthService;
import com.code.etf.job.server.service.QuotationManager;
import com.code.etf.job.server.service.RebalanceService;
import com.code.springboot.redis.server.RedisManager;
import com.code.util.web.SpringContextUtils;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.MDC;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.quartz.QuartzJobBean;

@Slf4j
public abstract class AbstractEtfJob extends QuartzJobBean {
    protected final static RedisManager redisManager = SpringContextUtils.getBean(RedisManager.class);
    protected final static NetWorthFeign netWorthFeign = SpringContextUtils.getBean(NetWorthFeign.class);
    protected final static RebalanceRecordFeign rebalanceRecordFeign = SpringContextUtils.getBean(RebalanceRecordFeign.class);
    protected final static RedemptionOrderFeign redemptionOrderFeign = SpringContextUtils.getBean(RedemptionOrderFeign.class);
    protected final static KafkaTemplate<String, String> kafkaTemplate = SpringContextUtils.getBean(KafkaTemplate.class);
    protected final static QuotationManager quotationManager = SpringContextUtils.getBean(QuotationManager.class);
    protected final static NetWorthService netWorthService = SpringContextUtils.getBean(NetWorthService.class);
    protected final static RebalanceService rebalanceService = SpringContextUtils.getBean(RebalanceService.class);
    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        MDC.put("tradeId", java.util.UUID.randomUUID().toString().replaceAll("-", "").toUpperCase());

        JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
        ProductConfigDTO dto = JSONObject.parseObject(JSONObject.toJSONString(jobDataMap), ProductConfigDTO.class);

        try {
            this.postExecute(context, dto);
        } catch (Exception e) {
            log.error("job任务执行失败，productConfig:{},exception:{}", dto, e);
            compensate(context, dto);
        }
    }

    protected void compensate(JobExecutionContext context, ProductConfigDTO dto) throws JobExecutionException {
    }

    public abstract void postExecute(JobExecutionContext context, ProductConfigDTO productConfig);
}
