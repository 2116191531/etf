package com.code.etf.job.server.job;

import com.alibaba.fastjson.JSONObject;
import com.code.api.RespDict;
import com.code.commons.mojo.result.ResultData;
import com.code.commons.mojo.result.ResultStatus;
import com.code.etf.admin.api.dto.ProductConfigDTO;
import com.code.etf.admin.api.dto.RebalanceRecordDTO;
import com.code.etf.admin.api.menu.KafkaTopicKeys;
import com.code.etf.admin.api.menu.RedisKeysEnum;
import com.code.etf.admin.api.message.NetWorthMessage;
import com.code.exception.ExceptionBuilder;
import com.code.message.ExternalMessage;
import com.code.message.InternalMessage;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 净值计算
 * 每天再平衡前10分钟变更净值【净值 = 上个周期的基准净值 * （1 - 管理费率*M）】
 * 直到下个再平衡点计算完净值后恢复正常公式
 *
 * @author qilou
 * @date 2021-6-29
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
@Slf4j
public class NetWorthJob extends AbstractEtfJob {

    @Override
    public void postExecute(JobExecutionContext context, ProductConfigDTO dto) {
        log.info("==================开始执行任务: 净值计算==================");
        log.info("执行任务线程ID{}", Thread.currentThread().getId());
        final String pair = dto.getDealPair();

        //调仓记录
        ResultData<RebalanceRecordDTO> reBalanceRecordResult = rebalanceRecordFeign.findOneRebalanceRecordByNow(pair);
        log.info("执行{}交易对净值计算时，最新调仓记录:{}", pair, reBalanceRecordResult);
        if (reBalanceRecordResult == null || !ResultStatus.SUCCESS.getCode().equals(reBalanceRecordResult.getCode()) || reBalanceRecordResult.getData() == null) {
            log.error("执行{}交易对净值计算时，最新调仓记录失败:{}", pair, reBalanceRecordResult);
            ExceptionBuilder.service(InternalMessage.builder().template("交易对净值计算时，最新调仓记录失败!").build()
                    , ExternalMessage.builder().template(RespDict.SERVICE_ERROR.getMsg()).code(RespDict.SERVICE_ERROR.getCode().toString()).build());
        }
        RebalanceRecordDTO reBalanceRecord = reBalanceRecordResult.getData();


        // 追踪交易对价格 ,如果标的数据为null，或小于等于 0 ，则净值计算失败
        BigDecimal dealPairTrackPrice = quotationManager.getDealPairPrice(dto.getDealPairTrack());
        log.info("执行{}交易对净值计算时，追踪标的价格:{}", pair, dealPairTrackPrice);
        if (dealPairTrackPrice == null || dealPairTrackPrice.compareTo(BigDecimal.ZERO) <= 0) {
            // 当出现标的价格不合法时，直接开启维护了
            log.error("执行{}交易对净值计算时, 标的交易对数据不合法，请关闭当前交易对，{}最新标的价格:{}", pair, dto.getDealPairTrack(), dealPairTrackPrice);
            ExceptionBuilder.service(InternalMessage.builder().template("交易对净值计算时, 标的交易对数据不合法，请关闭当前交易对!").build()
                    , ExternalMessage.builder().template(RespDict.SERVICE_ERROR.getMsg()).code(RespDict.SERVICE_ERROR.getCode().toString()).build());
        }

        // 当前产品的计算因子
        String dealPairNetWorthLoadFactorStr = String.valueOf(redisManager.get(String.format(RedisKeysEnum.ETF_DEAL_PAIR_NET_WORTH_LOAD_FACTOR.getKey(), pair)));
        BigDecimal dealPairNetWorthLoadFactor = new BigDecimal(dealPairNetWorthLoadFactorStr);
        log.info("执行{}交易对净值计算时，当前产品的计算因子:{}", pair, dealPairNetWorthLoadFactorStr);


        BigDecimal result = getBigDecimal(pair, reBalanceRecord.getNetWorth(), BigDecimal.valueOf(reBalanceRecord.getMultiple()), reBalanceRecord.getDealPairTrackPrice(), dealPairTrackPrice, dealPairNetWorthLoadFactor);

        if (result.compareTo(BigDecimal.ZERO) <= 0) {
            log.error("执行{}交易对净值计算时, 净值出现负数:{},净值计算因子:{}", pair, result, dealPairNetWorthLoadFactor);
            ExceptionBuilder.service(InternalMessage.builder().template("交易对净值计算时, 净值出现负数!").build()
                    , ExternalMessage.builder().template(RespDict.SERVICE_ERROR.getMsg()).code(RespDict.SERVICE_ERROR.getCode().toString()).build());
        }

        saveRedisAndPushEvent(pair, result);
    }

//    @Override
//    protected void compensate(JobExecutionContext context, ProductConfigDTO dto) throws JobExecutionException {
//        // 补偿，如果出现异常，直接净值计算为0，后续逻辑需要考虑净值为0时的情况
//        saveRedisAndPushEvent(dto.getDealPair(), BigDecimal.ZERO);
//    }

    private void saveRedisAndPushEvent(String dealPair, BigDecimal result) {
        NetWorthMessage netWorth = NetWorthMessage.builder()
                .dealPair(dealPair)
                .time(System.currentTimeMillis())
                .val(result)
                .build();
        // 存储redis
        redisManager.set(String.format(RedisKeysEnum.ETF_NET_WORTH_NEW.getKey(), dealPair), JSONObject.toJSONString(netWorth));

        //发送kafka


        kafkaTemplate.send(KafkaTopicKeys.ETF_DEAL_PAIR_NET_WORTH, JSONObject.toJSONString(netWorth));
    }

    /**
     * @param dealPair                   交易对
     * @param stk1                       上一个周期净值
     * @param m                          目标刚刚倍数
     * @param ptk1                       上一个周期标的价格
     * @param dealPairTrackPrice         当前标的追踪价格
     * @param dealPairNetWorthLoadFactor 净值计算因子
     * @return 净值
     */
    private BigDecimal getBigDecimal(String dealPair, BigDecimal stk1, BigDecimal m, BigDecimal ptk1, BigDecimal dealPairTrackPrice, BigDecimal dealPairNetWorthLoadFactor) {
        BigDecimal result;
        /*
        S(0)：基金初值;
        S(t)：基金在 t 时刻的净值，t ≥ 0；
        P(0)：标的资产在初始时刻的价格；
        P(t)：标的资产在 t 时刻的价格；
        M：目标杠杆倍数，可取 2、3、-1、-2、-3。
        K：当日标的价格反方向涨跌幅不超过15%（后台可配置），k为一天；当日标的价格第一次反方向涨跌幅超过15%，从0点到此刻记为一个周期k。
        上一个周期净值 * (1 + 目标杠杠倍数 * (当前标的价格 - 上个周期标的价格) / 上一个周期标的价格)
         净值  S(v) = S(tk - 1) * ( 1 + M * (P(v) - P(tk - 1)) / P(tk - 1) )
        */
        result = stk1.multiply(BigDecimal.ONE.add(m.multiply(dealPairTrackPrice.subtract(ptk1).divide(ptk1, 18, RoundingMode.DOWN))));
        result = result.multiply(dealPairNetWorthLoadFactor).setScale(4, RoundingMode.DOWN);
        log.info("执行{}交易对净值计算时保留4位, : S(v) = [S(tk-1) * ( 1 + M * (P(v) - P(tk-1)) / P(tk-1) )] * factor, 净值计算：{} = [{} * (1  + {} * ({} - {}) / {})] * {}", dealPair, result, stk1, m, dealPairTrackPrice, ptk1, ptk1, dealPairNetWorthLoadFactor);
        return result;
    }
}
