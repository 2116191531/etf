package com.code.etf.job.server.licenter;

import com.code.etf.admin.api.menu.KafkaTopicKeys;
import com.code.etf.job.server.service.QuartzManager;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

// todo  优化，不用kafka 采用feign调用 , 并且默认是禁用的。所以只有开启和禁用发送定时任务操作，
@Component
public class ETFDealPairConsumer {
    @Autowired
    private QuartzManager quartzManager;
//
//    /**
//     * 监听etf配置加入事件
//     */
//    @KafkaListener(topics = KafkaTopicKeys.ETF_DEAL_PAIR_CONFIG_ADD)
//    public void insertETFDealPairListen(ConsumerRecord<String, String> record, Acknowledgment acknowledgment) {
//        System.out.println(record.timestamp());
//        System.out.printf("添加deal: topic = %s, offset = %d, value = %s \n", record.topic(), record.offset(), record.value());
//        ProductConfigDTO productConfigDTO = JSONObject.parseObject(record.value(), ProductConfigDTO.class);
//        SchedulerListener.addJob(productConfigDTO);
//        acknowledgment.acknowledge();
//    }
//
//    /**
//     * 监听etf配置更新事件
//     */
//    @KafkaListener(topics = KafkaTopicKeys.ETF_DEAL_PAIR_CONFIG_UPDATE)
//    public void UpdateETFDealPairListen(ConsumerRecord<String, String> record, Acknowledgment acknowledgment) {
//        System.out.println(record.timestamp());
//        System.out.printf("添加deal: topic = %s, offset = %d, value = %s \n", record.topic(), record.offset(), record.value());
//        ProductConfigDTO productConfigDTO = JSONObject.parseObject(record.value(), ProductConfigDTO.class);
//        SchedulerListener.cleanJob(productConfigDTO);
//        SchedulerListener.addJob(productConfigDTO);
//        acknowledgment.acknowledge();
//    }
//
//    /**
//     * 监听etf配置删除事件
//     */
//    @KafkaListener(topics = KafkaTopicKeys.ETF_DEAL_PAIR_CONFIG_DELETE)
//    public void DeleteETFDealPairListen(ConsumerRecord<String, String> record, Acknowledgment acknowledgment) {
//        System.out.println(record.timestamp());
//        System.out.printf("添加deal: topic = %s, offset = %d, value = %s \n", record.topic(), record.offset(), record.value());
//        ProductConfigDTO productConfigDTO = JSONObject.parseObject(record.value(), ProductConfigDTO.class);
//        SchedulerListener.cleanJob(productConfigDTO);
//        acknowledgment.acknowledge();
//    }

    /**
     * 监听etf开关，TODO 优化
     */
    @KafkaListener(topics = KafkaTopicKeys.ETF_DEAL_PAIR_SWITCH)
    public void etfSwitchListen(ConsumerRecord<String, String> record, Acknowledgment acknowledgment) {
        System.out.println(record.timestamp());
        System.out.printf("添加deal: topic = %s, offset = %d, value = %s \n", record.topic(), record.offset(), record.value());
        Object val = record.value();
        String res = String.valueOf(val);
        if ("1".equals(res)) {
            // 暂停所有任务，包装当前quartz没有其他任务，避免关闭多余额任务
            quartzManager.shutdownJobs();
        } else {
            // 开启所有任务
            quartzManager.startJobs();
        }
        acknowledgment.acknowledge();
    }
}
