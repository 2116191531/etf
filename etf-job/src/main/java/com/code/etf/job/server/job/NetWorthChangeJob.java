package com.code.etf.job.server.job;

import com.code.api.RespDict;
import com.code.etf.admin.api.dto.ProductConfigDTO;
import com.code.etf.admin.api.menu.RedisKeysEnum;
import com.code.exception.ExceptionBuilder;
import com.code.message.ExternalMessage;
import com.code.message.InternalMessage;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;

import java.math.BigDecimal;

/**
 * 定时修改净值计算因子（收取管理费）
 *
 * @author qilou
 * @date 2021-6-29
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
@Slf4j
public class NetWorthChangeJob extends AbstractEtfJob {

    @Override
    public void postExecute(JobExecutionContext context, ProductConfigDTO productConfig) {
        log.info("================== 开始执行任务: 定时修改净值计算因子（收取管理费）==================");
        log.info("执行任务线程ID{}", Thread.currentThread().getId());


        // 设置xx 交易对 净值计算因子  = 1 - 管理费
        BigDecimal value = BigDecimal.ONE.subtract(productConfig.getFeeMangeActual());

        try {
            redisManager.set(String.format(RedisKeysEnum.ETF_DEAL_PAIR_NET_WORTH_LOAD_FACTOR.getKey(), productConfig.getDealPair()), value);
        } catch (Exception e) {
            log.error("管理费收取失败(人工干预)：", e);
            ExceptionBuilder.service(InternalMessage.builder().template("管理费收取失败(人工干预)失败!").build()
                    , ExternalMessage.builder().template(RespDict.SERVICE_ERROR.getMsg()).code(RespDict.SERVICE_ERROR.getCode().toString()).build());
        }
    }

    @Override
    public void compensate(JobExecutionContext context, ProductConfigDTO dto) throws JobExecutionException {
        //TODO  管理费收取失败，怎么补偿？
    }
}