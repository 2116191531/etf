package com.code.etf.job.server.config;

import com.code.util.web.SpringContextUtils;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.context.annotation.Configuration;

@Configuration
@ImportAutoConfiguration({SpringContextUtils.class})
public class SpringBeanConfig {
}
