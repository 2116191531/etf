package com.code.etf.job.server.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "com.code.quotation-process")
public class QuotationProcessProperties {
    private String url;
}
