package com.code.etf.job.server.job;

import com.alibaba.fastjson.JSONObject;
import com.code.api.RespDict;
import com.code.commons.mojo.result.ResultData;
import com.code.commons.mojo.result.ResultStatus;
import com.code.etf.admin.api.dto.ProductConfigDTO;
import com.code.etf.admin.api.dto.RebalanceRecordDTO;
import com.code.etf.admin.api.menu.RebalanceTypeEnum;
import com.code.etf.admin.api.menu.RedisKeysEnum;
import com.code.etf.admin.api.message.NetWorthMessage;
import com.code.exception.ExceptionBuilder;
import com.code.message.ExternalMessage;
import com.code.message.InternalMessage;
import com.code.util.env.EnvUtil;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;

import java.math.BigDecimal;

/**
 * 定时再平衡
 *
 * @author qilou
 * @date 2021-6-29
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
@Slf4j
public class TimingReBalanceJob extends AbstractEtfJob {
    @Override
    public void postExecute(JobExecutionContext context, ProductConfigDTO dto) {
        final String pair = dto.getDealPair();
        log.info("==================开始执行任务: {} 定时再平衡==================", pair);
        log.info("执行任务线程ID{}", Thread.currentThread().getId());

        if (!EnvUtil.isTestOrQa()) {
            // 查询今天是否有定时调仓记录，如果有，则不执行此次调仓记录
            ResultData<RebalanceRecordDTO> resultData = rebalanceRecordFeign.findOneByTypeTiming(pair);
            log.info("执行{}交易对定时再平衡时，查询当前交易对今天的定时调仓记录:{}", pair, resultData);

            if (!ResultStatus.SUCCESS.getCode().equals(resultData.getCode())) {
                log.error("/etf-admin/rebalance-record/find-one-by-now response: {}", resultData);
                return;
            }
            if (resultData.getData() != null) {
                log.warn("执行{}交易对定时再平衡时，今日已进行定时调仓，不再重复进行:{}", pair, resultData.getData());
                return;
            }
        }


        // 追踪交易对价格
        BigDecimal dealPairTrackPrice = quotationManager.getDealPairPrice(dto.getDealPairTrack());
        log.info("执行{}交易对定时再平衡时，追踪标的价格:{}", pair, dealPairTrackPrice);
        if (dealPairTrackPrice == null || dealPairTrackPrice.compareTo(BigDecimal.ZERO) <= 0) {
            log.error("执行{}交易对定时再平衡时，标的交易对数据不合法，请关闭当前交易对，{}最新标的价格:{}", pair, dto.getDealPairTrack(), dealPairTrackPrice);
            ExceptionBuilder.service(InternalMessage.builder().template("执行" + pair + "交易对定时再平衡时，标的交易对数据不合法，请关闭当前交易对").build()
                    , ExternalMessage.builder().template(RespDict.SERVICE_ERROR.getMsg()).code(RespDict.SERVICE_ERROR.getCode().toString()).build());
        }

        //获取最新调仓记录

        ResultData<RebalanceRecordDTO> reBalanceRecordResult = rebalanceRecordFeign.findOneRebalanceRecordByNow(pair);
        log.info("执行{}交易对定时再平衡时，最新调仓记录:{}", pair, reBalanceRecordResult);
        if (reBalanceRecordResult == null || !ResultStatus.SUCCESS.getCode().equals(reBalanceRecordResult.getCode()) || reBalanceRecordResult.getData() == null) {
            log.error("执行{}交易对定时再平衡时，最新调仓记录失败:{}", pair, reBalanceRecordResult);
            ExceptionBuilder.service(InternalMessage.builder().template("交易对定时再平衡时，最新调仓记录失败!").build()
                    , ExternalMessage.builder().template(RespDict.SERVICE_ERROR.getMsg()).code(RespDict.SERVICE_ERROR.getCode().toString()).build());
        }
        RebalanceRecordDTO reBalanceRecord = reBalanceRecordResult.getData();

        rebalanceService.execute(RebalanceTypeEnum.TIMING_RE_BALANCE, dto, reBalanceRecord, dealPairTrackPrice);
    }

    @Override
    protected void compensate(JobExecutionContext context, ProductConfigDTO dto) throws JobExecutionException {
        // todo 定时再平衡失败后，怎么去处理？？？李雷
    }
}
