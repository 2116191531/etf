CREATE TABLE `etf_product_config`
(
    `id`                        bigint(65) NOT NULL AUTO_INCREMENT,
    `sort`                      int(65) DEFAULT NULL COMMENT '排序',
    `deal_pair`                 varchar(255) NOT NULL COMMENT '交易对',
    `multiple`                  int(11) NOT NULL DEFAULT '0' COMMENT '倍数',
    `market_maker_Ids`          varchar(1024)   DEFAULT NULL COMMENT '做市用户ids 逗号分隔',
    `fee_mange_expect`          decimal(36, 18) DEFAULT NULL COMMENT '管理费(展示)',
    `fee_mange_actual`          decimal(36, 18) DEFAULT NULL COMMENT '管理费(实际)',
    `fee_rate_buy`              decimal(36, 18) DEFAULT NULL COMMENT '申购费率',
    `fee_rate_redeem`           decimal(36, 18) DEFAULT NULL COMMENT '赎回费率',
    `buy_min_price`             decimal(36, 18) DEFAULT NULL COMMENT '最小申购',
    `redeem_min_price`          decimal(36, 18) DEFAULT NULL COMMENT '最小赎回',
    `day_buy_limit_global`      decimal(36, 18) DEFAULT NULL COMMENT '产品维度每日购买限额',
    `day_redeem_limit_global`   decimal(36, 18) DEFAULT NULL COMMENT '产品维度每日赎回限额',
    `day_buy_limit_personal`    decimal(36, 18) DEFAULT NULL COMMENT '个人每日购买限额',
    `day_redeem_limit_personal` decimal(36, 18) DEFAULT NULL COMMENT '个人每日赎回限额',
    `threshold_up_down`         decimal(36, 18) DEFAULT NULL COMMENT '再平衡阈值（反方向涨跌幅）',
    `time_equilibrium`          int(11) DEFAULT NULL COMMENT '定时平衡时间(0-23（默认0点）)',
    `deal_pair_track`           varchar(50)     DEFAULT NULL COMMENT '标的交易对',
    `price_net_init`            decimal(36, 18) DEFAULT NULL COMMENT '初始净值价格',
    `threshold_trade_coin`      decimal(36, 18) DEFAULT NULL COMMENT '交易币种余额阀值',
    `threshold_valuation_coin`  decimal(36, 18) DEFAULT NULL COMMENT '计价币种余额阀值',
    `threshold_net_merge`       decimal(36, 18) DEFAULT NULL COMMENT '净值合并阀值',
    `icon`                      varchar(500)    DEFAULT NULL COMMENT '产品图标',
    `is_release`                tinyint(1) DEFAULT '0' COMMENT '发布状态 1 开启中 0 已隐藏',
    `coin_valuation`            varchar(50)     DEFAULT NULL COMMENT '估值币种',
    `coin_trade`                varchar(50)     DEFAULT NULL COMMENT '交易币种',
    `switch_buy_redemption`     tinyint(1) DEFAULT '0' COMMENT '申赎开关 1 开启中 0 已关闭',
    `create_user`               varchar(255)    DEFAULT NULL COMMENT '创建人',
    `operator`                  varchar(255)    DEFAULT NULL,
    `issue_time`                bigint(30) DEFAULT NULL COMMENT '发行时间',
    `international`             varchar(5000)   DEFAULT NULL COMMENT '国际化描述',
    `create_time`               bigint(30) DEFAULT NULL COMMENT '创建时间',
    `update_time`               bigint(30) DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (`id`),
    UNIQUE KEY `idx_deal_pair` (`deal_pair`) USING BTREE COMMENT '交易对唯一索引'
) ENGINE = InnoDB
  AUTO_INCREMENT = 157
  DEFAULT CHARSET = utf8mb4 COMMENT ='etf产品配置表';


CREATE TABLE `etf_rebalance_record`
(
    `id`                        bigint(20) NOT NULL AUTO_INCREMENT,
    `deal_pair`                 varchar(255)    NOT NULL COMMENT '交易对',
    `type`                      char(1)         NOT NULL COMMENT '调仓类型：1 定时 2临时',
    `coin_trade`                varchar(255)    NOT NULL COMMENT '计价货币单位',
    `coin_valuation`            varchar(255)    NOT NULL COMMENT '交易货币单位',
    `net_worth_old`             decimal(36, 18) NOT NULL COMMENT '上一次调仓：净值',
    `net_worth`                 decimal(36, 18) NOT NULL COMMENT '本次调仓：净值',
    `deal_pair_track_price_old` decimal(36, 18) NOT NULL COMMENT '上一次调仓：标的价格',
    `deal_pair_track_price`     decimal(36, 18) NOT NULL COMMENT '本次调仓：标的价格',
    `deal_pair_track`           varchar(50)     NOT NULL COMMENT '标的交易对',
    `coin_trade_price_old`      decimal(36, 18) NOT NULL COMMENT '上一次调仓：篮子-计价货币值',
    `coin_trade_price`          decimal(36, 18) NOT NULL COMMENT '本次调仓：篮子-交易货币',
    `coin_valuation_price_old`  decimal(36, 18) NOT NULL COMMENT '上一次调仓：篮子-交易货币值',
    `coin_valuation_price`      decimal(36, 18) NOT NULL COMMENT '本次调仓：篮子-计价货币',
    `leverage_multiple_old`     decimal(36, 18) NOT NULL COMMENT '上一次调仓：杠杠倍数',
    `leverage_multiple`         int(3) NOT NULL COMMENT '本次调仓：杠杠倍数',
    `multiple`                  int(2) NOT NULL COMMENT '基本杠杠倍数',
    `user_position_toal`        decimal(36, 18) NOT NULL COMMENT '用户持仓总量',
    `create_time`               bigint(20) DEFAULT NULL,
    `create_by`                 varchar(255) DEFAULT NULL,
    `update_time`               bigint(20) DEFAULT NULL,
    `update_by`                 varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
) COMMENT='调仓记录';


CREATE TABLE `etf_net_worth`
(
    `deal_pair` varchar(20)    DEFAULT NULL,
    `time`      bigint(13) DEFAULT NULL,
    `val`       decimal(32, 4) DEFAULT NULL
) COMMENT='净值记录表';